delamo package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   delamo.cli
   delamo.models
   delamo.text
   delamo.utils

Submodules
----------

delamo.callbacks module
-----------------------

.. automodule:: delamo.callbacks
   :members:
   :undoc-members:
   :show-inheritance:

delamo.losses module
--------------------

.. automodule:: delamo.losses
   :members:
   :undoc-members:
   :show-inheritance:

delamo.metrics module
---------------------

.. automodule:: delamo.metrics
   :members:
   :undoc-members:
   :show-inheritance:

delamo.optimizers module
------------------------

.. automodule:: delamo.optimizers
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: delamo
   :members:
   :undoc-members:
   :show-inheritance:
