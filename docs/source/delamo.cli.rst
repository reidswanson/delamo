delamo.cli package
==================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   delamo.cli.options
   delamo.cli.utils

Submodules
----------

delamo.cli.generate module
--------------------------

.. automodule:: delamo.cli.generate
   :members:
   :undoc-members:
   :show-inheritance:

delamo.cli.preprocess\_wiki module
----------------------------------

.. automodule:: delamo.cli.preprocess_wiki
   :members:
   :undoc-members:
   :show-inheritance:

delamo.cli.test module
----------------------

.. automodule:: delamo.cli.test
   :members:
   :undoc-members:
   :show-inheritance:

delamo.cli.train module
-----------------------

.. automodule:: delamo.cli.train
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: delamo.cli
   :members:
   :undoc-members:
   :show-inheritance:
