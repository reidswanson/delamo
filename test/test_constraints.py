# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import unittest

# 3rd Party Modules
import numpy as np
import tensorflow as tf

# Project Modules
from delamo.constraints import RangeConstraint
from delamo.utils.gpu import disable_gpus


# Always use the CPU for tests
disable_gpus()

# Always run eagerly when possible
tf.config.run_functions_eagerly(True)


class TestConstraints(unittest.TestCase):
    def test_range_constraint(self):
        t1 = tf.range(0, 5, 0.25, dtype=tf.float32)
        t2 = tf.range(-3.0, 3.0, 0.25, dtype=tf.float32)
        t3 = tf.range(1.1, 1.9, 0.1, dtype=tf.float32)

        # Test when the range is completely above 0
        c1 = RangeConstraint(1.33, 3.33)
        act_1 = c1(t1)
        exp_1 = [1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.50, 1.75, 2.00, 2.25, 2.50, 2.75, 3.00,
                 3.25, 3.33, 3.33, 3.33, 3.33, 3.33, 3.33]

        act_2 = c1(t2)
        exp_2 = [1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.33,
                 1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.33, 1.50, 1.75, 2.00, 2.25,
                 2.50, 2.75]

        act_3 = c1(t3)
        exp_3 = [1.33, 1.33, 1.33, 1.40, 1.50, 1.60, 1.70, 1.80]

        np.testing.assert_array_almost_equal(exp_1, act_1)
        np.testing.assert_array_almost_equal(exp_2, act_2)
        np.testing.assert_array_almost_equal(exp_3, act_3)

        # Test when the range includes 0
        c2 = RangeConstraint(-2.1, 2.1)

        act_1 = c2(t1)
        exp_1 = [0.00, 0.25, 0.50, 0.75, 1.00, 1.25, 1.50, 1.75, 2.0, 2.10, 2.10, 2.10, 2.10, 2.10,
                 2.10, 2.10, 2.10, 2.10, 2.10, 2.10]

        act_2 = c2(t2)
        exp_2 = [-2.10, -2.10, -2.10, -2.10, -2.00, -1.75, -1.50, -1.25, -1.00, -0.75, -0.50,
                 -0.25,  0.00,  0.25,  0.50,  0.75,  1.00,  1.25,  1.50,  1.75,  2.00,  2.10,
                  2.10,  2.10]

        act_3 = c2(t3)
        exp_3 = [1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8]

        np.testing.assert_array_almost_equal(exp_1, act_1)
        np.testing.assert_array_almost_equal(exp_2, act_2)
        np.testing.assert_array_almost_equal(exp_3, act_3)
