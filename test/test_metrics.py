# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import unittest

# 3rd Party Modules
import tensorflow as tf

# Project Modules
from delamo.metrics import ShingledSparseCategoricalCrossentropy


class TestMetrics(unittest.TestCase):
    def test_shingled_crossentropy(self):
        y_true = tf.constant([[1, 2, 3],
                              [2, 3, 4],
                              [3, 2, 4]])

        y_pred = tf.random.uniform((3, 3, 5), minval=-1.0, maxval=1.0, seed=0)
        y_pred_softmax = tf.nn.softmax(y_pred)

        xent = ShingledSparseCategoricalCrossentropy()

        xent.update_state(y_true, y_pred)

        exp_xent = tf.math.log([
            y_pred_softmax[0, 2, 3],
            y_pred_softmax[1, 2, 4],
            y_pred_softmax[2, 2, 4],
        ])

        exp_xent = -tf.math.reduce_sum(exp_xent) / tf.cast(tf.shape(exp_xent)[0], exp_xent.dtype)
        act_xent = xent.result()

        self.assertAlmostEqual(act_xent, exp_xent)

    def test_masked_shingled_crossentropy(self):
        y_true = tf.constant([[1, 2, 3, 0],
                              [2, 3, 4, 2],
                              [3, 2, 4, 1]])

        y_pred = tf.random.uniform((3, 4, 5), minval=-1.0, maxval=1.0, seed=0)
        y_pred_softmax = tf.nn.softmax(y_pred)

        print(f"y_pred_softmax:\n{y_pred_softmax}")

        xent = ShingledSparseCategoricalCrossentropy()

        xent.update_state(y_true, y_pred)

        exp_xent = tf.math.log([
            y_pred_softmax[1, 3, 2],
            y_pred_softmax[2, 3, 1],
        ])

        exp_xent = -tf.math.reduce_sum(exp_xent) / tf.cast(tf.shape(exp_xent)[0], exp_xent.dtype)
        act_xent = xent.result()

        self.assertAlmostEqual(act_xent, exp_xent)
