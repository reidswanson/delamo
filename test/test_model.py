# Python Modules
import logging
import unittest

# 3rd Party Modules
import numpy as np
import tensorflow as tf

# Project Modules
from delamo.losses import MaskedSparseCategoricalCrossentropy
from delamo.models.transformer import DecoderOnlyLanguageModel, Decoder, MultiHeadAttention, \
    PositionEncoding

tf.config.experimental_run_functions_eagerly(True)
tf.random.set_seed(0)


logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
)
log = logging.getLogger('test_model')


class TestModel(unittest.TestCase):
    def test_positional_encoding(self):
        act_enc = Decoder.make_positional_encoding(8, 16).numpy()[0]
        exp_enc = np.array([[ 0.0000000e+00,  1.0000000e+00,  0.0000000e+00,  1.0000000e+00,
                              0.0000000e+00,  1.0000000e+00,  0.0000000e+00,  1.0000000e+00,
                              0.0000000e+00,  1.0000000e+00,  0.0000000e+00,  1.0000000e+00,
                              0.0000000e+00,  1.0000000e+00,  0.0000000e+00,  1.0000000e+00],
                            [ 8.4147096e-01,  5.4030228e-01,  3.1098360e-01,  9.5041525e-01,
                              9.9833414e-02,  9.9500418e-01,  3.1617507e-02,  9.9950004e-01,
                              9.9998331e-03,  9.9994999e-01,  3.1622723e-03,  9.9999499e-01,
                              9.9999981e-04,  9.9999952e-01,  3.1622776e-04,  9.9999994e-01],
                            [ 9.0929741e-01, -4.1614684e-01,  5.9112710e-01,  8.0657840e-01,
                              1.9866933e-01,  9.8006660e-01,  6.3203394e-02,  9.9800068e-01,
                              1.9998666e-02,  9.9980003e-01,  6.3245133e-03,  9.9997997e-01,
                              1.9999987e-03,  9.9999797e-01,  6.3245551e-04,  9.9999982e-01],
                            [ 1.4112000e-01, -9.8999250e-01,  8.1264889e-01,  5.8275360e-01,
                              2.9552022e-01,  9.5533651e-01,  9.4726093e-02,  9.9550337e-01,
                              2.9995501e-02,  9.9955004e-01,  9.4866911e-03,  9.9995500e-01,
                              2.9999956e-03,  9.9999553e-01,  9.4868318e-04,  9.9999952e-01],
                            [-7.5680250e-01, -6.5364361e-01,  9.5358074e-01,  3.0113748e-01,
                             3.8941833e-01,   9.2106098e-01,  1.2615407e-01,  9.9201065e-01,
                             3.9989334e-02,   9.9920011e-01,  1.2648773e-02,  9.9992001e-01,
                             3.9999895e-03,   9.9999201e-01,  1.2649107e-03,  9.9999923e-01],
                            [-9.5892429e-01,  2.8366220e-01,  9.9994653e-01, -1.0342319e-02,
                             4.7942555e-01,   8.7758255e-01,  1.5745589e-01,  9.8752600e-01,
                             4.9979169e-02,   9.9875027e-01,  1.5810730e-02,  9.9987501e-01,
                             4.9999794e-03,   9.9998748e-01,  1.5811381e-03,  9.9999875e-01],
                            [-2.7941549e-01,  9.6017027e-01,  9.4714814e-01, -3.2079646e-01,
                             5.6464249e-01,   8.2533562e-01,  1.8860029e-01,  9.8205394e-01,
                             5.9964005e-02,   9.9820054e-01,  1.8972527e-02,  9.9981999e-01,
                             5.9999642e-03,   9.9998200e-01,  1.8973654e-03,  9.9999821e-01],
                            [ 6.5698659e-01,  7.5390226e-01,  8.0042166e-01, -5.9943742e-01,
                              6.4421767e-01,  7.6484221e-01,  2.1955609e-01,  9.7559988e-01,
                              6.9942847e-02,  9.9755102e-01,  2.2134136e-02,  9.9975502e-01,
                              6.9999429e-03,  9.9997550e-01,  2.2135926e-03,  9.9999756e-01]])

        np.testing.assert_array_almost_equal(act_enc, exp_enc)

        # Test the positional encoding as a layer
        layer = PositionEncoding(8, 16)

        log.debug(f"layer.pos_encoding:\n{layer.pos_encoding[0]}")

        np.testing.assert_array_almost_equal(layer.pos_encoding[0], exp_enc)

    def test_padding_mask(self):
        # The example from https://www.tensorflow.org/tutorials/text/transformer#masking
        x = tf.constant([[7, 6, 0, 0, 1],
                         [1, 2, 3, 0, 0],
                         [0, 0, 0, 4, 5]])
        act_mask = DecoderOnlyLanguageModel.make_padding_mask(x)
        exp_mask = [[[[0., 0., 1., 1., 0.]]],
                    [[[0., 0., 0., 1., 1.]]],
                    [[[1., 1., 1., 0., 0.]]]]

        np.testing.assert_array_equal(act_mask, exp_mask)

    def test_lookahead_mask(self):
        x = tf.random.uniform((1, 3), seed=0)
        act_mask = DecoderOnlyLanguageModel.make_lookahead_mask(x.shape[1])
        exp_mask = [[0., 1., 1.],
                    [0., 0., 1.],
                    [0., 0., 0.]]

        np.testing.assert_array_equal(act_mask, exp_mask)

    def test_sdp_attention(self):
        # The example from:
        # https://www.tensorflow.org/tutorials/text/transformer#scaled_dot_product_attention
        k1 = tf.constant([[10,  0,  0],
                          [ 0, 10,  0],
                          [ 0 , 0, 10],
                          [ 0,  0, 10]], dtype=tf.float32)  # (4, 3)

        v1 = tf.constant([[   1, 0],
                          [  10, 0],
                          [ 100, 5],
                          [1000, 6]], dtype=tf.float32)  # (4, 2)

        # This `query` aligns with the second `key`,
        # so the second `value` is returned.
        q1 = tf.constant([[0, 10, 0]], dtype=tf.float32)  # (1, 3)

        act_out1, act_attn1 = MultiHeadAttention.sdp_attention(q1, k1, v1, None)
        exp_out1, exp_attn1 = [[10.0, 0.0]], [[0, 1, 0, 0]]

        np.testing.assert_array_almost_equal(act_out1, exp_out1)
        np.testing.assert_array_almost_equal(act_attn1, exp_attn1)

        q2 = tf.constant([[0, 0, 10], [0, 10, 0], [10, 10, 0]], dtype=tf.float32)  # (3, 3)

        act_out2, act_attn2 = MultiHeadAttention.sdp_attention(q2, k1, v1, None)
        exp_out2 = [[550., 5.5],
                    [ 10., 0. ],
                    [ 5.5, 0. ]]
        exp_attn2 = [[0.,  0. ,  0.5, 0.5],
                     [0.,  1. ,  0.,  0. ],
                     [0.5, 0.5,  0.,  0. ]]

        np.testing.assert_array_almost_equal(act_out2, exp_out2)
        np.testing.assert_array_almost_equal(act_attn2, exp_attn2)

    @unittest.skip('not a real test')
    def test_language_model(self):
        train_data = np.array([[1, 2, 3, 2, 4, 5, 0, 0],
                               [1, 4, 2, 3, 5, 0, 0, 0],
                               [1, 3, 3, 2, 4, 2, 5, 0]])

        x, y = train_data[:, :-1], train_data[:, 1:]

        word2id = {str(k): i for i, k in enumerate(range(6))}

        optimizer = tf.keras.optimizers.Adam()
        loss = MaskedSparseCategoricalCrossentropy()
        model = DecoderOnlyLanguageModel(2, 64, 2, 32, word2id, 8)
        model.compile(optimizer=optimizer, loss=loss)
        model.fit(x=x, y=y, epochs=1, batch_size=3)
