# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import math
import unittest

# 3rd Party Modules
import numpy as np
import tensorflow as tf

# Project Modules
from delamo.models.xfmr import MultiHeadAttention, XfmrConfig
from delamo.utils.gpu import disable_gpus


# Always use the CPU for tests
disable_gpus()

# Always run eagerly when possible
tf.config.run_functions_eagerly(True)


class TestXfmr(unittest.TestCase):
    def test_split_heads(self):
        batch_size = 2
        seq_len = 5
        n_features = 8
        n_heads = 3
        sample_size = 4

        cfg = XfmrConfig(
            sequence_length=seq_len,
            n_attention_state_units=n_features,
            n_heads=n_heads,
            head_split_method='sample',
            head_sample_size=sample_size,
            head_sample_seed=1
        )
        mha = MultiHeadAttention(cfg)

        x = tf.reshape(
            tf.tile(tf.range(n_features, dtype=tf.float32), [batch_size * seq_len]),
            (batch_size, seq_len, n_features)
        )

        act = mha.split_heads(x)
        exp = [[[[7., 4., 5., 2.]] * 5,
                [[3., 0., 6., 1.]] * 5,
                [[3., 6., 4., 7.]] * 5]] * 2

        np.testing.assert_array_equal(exp, act)
