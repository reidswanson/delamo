# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import logging
import os
import unittest

# 3rd Party Modules
from typing import List

import numpy as np
import tensorflow as tf

# Project Modules
from delamo.cli.utils.dataset import make_dataset, find_max_document_length
from delamo.utils.vocab import py_vocab_from_file, tf_vocab_from_dict
from test.expected_datasets import *


tf.config.run_functions_eagerly(True)
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
)
log = logging.getLogger('test_dataset')


class TestDataset(unittest.TestCase):
    dataset_orig_file = os.path.join(os.path.dirname(__file__), 'dataset.tokens')
    dataset_1doc_file = os.path.join(os.path.dirname(__file__), 'dataset.1doc.tokens')
    dataset_1line_file = os.path.join(os.path.dirname(__file__), 'dataset.1line.tokens')

    dataset_orig_tokens = None
    dataset_1doc_tokens = None

    py_orig_vocab = py_vocab_from_file(dataset_orig_file)
    py_1doc_vocab = py_vocab_from_file(dataset_1doc_file)

    tf_orig_vocab = tf_vocab_from_dict(py_orig_vocab)
    tf_1doc_vocab = tf_vocab_from_dict(py_1doc_vocab)

    py_vocab = py_orig_vocab
    tf_vocab = tf_orig_vocab

    @classmethod
    def read_flat_tokens(cls, filename: str) -> List[str]:
        tokens = []
        with open(filename, 'rt') as fh:
            for line in fh:
                line = line.strip()
                tokens += line.split() + ['<br>']

        return tokens

    @classmethod
    def apply_vocab(cls, array):
        return np.apply_along_axis(
            lambda a: np.array([cls.py_vocab[w] for w in a]),
            -1,
            array
        )

    @classmethod
    def setUpClass(cls) -> None:
        cls.dataset_orig_tokens = cls.read_flat_tokens(cls.dataset_orig_file)

    def test_vocab(self):
        # All the unique words plus an empty token and <unk>
        self.assertEqual(len(self.py_orig_vocab), 52)
        self.assertEqual(len(self.py_1doc_vocab), len(self.py_orig_vocab))

    def test_max_document_length(self):
        max_doc_len = find_max_document_length(self.dataset_1doc_file)
        self.assertEqual(max_doc_len, 36)

    def test_fixed_sequences_method(self):
        params = {
            'filename': self.dataset_orig_file,
            'max_len': find_max_document_length(self.dataset_orig_file),
            'word2id': self.tf_vocab,
            'method': 'fixed_sequences',
            'batch_size': 4,
            'seq_len': 5
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_fixed_sequences_x_1)
        exp_y_batches = self.apply_vocab(exp_fixed_sequences_y_1)

        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

    def test_shingle_no_shuffle_method(self):
        params = {
            'filename': self.dataset_orig_file,
            'max_len': find_max_document_length(self.dataset_orig_file),
            'word2id': self.tf_vocab,
            'method': 'shingle',
            'batch_size': 4,
            'seq_len': 5
        }
        dataset = make_dataset(**params)

        n_tokens = len(self.dataset_orig_tokens)

        # Normally, shingling would create ``n_tokens - seq_len + 1`` complete
        # sequences. However, the window function adds incomplete padded
        # sequences at the end, so there are a total of exactly ``n_tokens + 1``
        # shingles. However, the ``x`` and ``y`` data are each ``n_tokens -``
        # in length. So the expected number of sequences/shingles should be
        # exactly ``n_tokens``.
        exp_n_sequences = n_tokens
        act_n_sequences = 0
        exp_x_batches = self.apply_vocab(exp_shingle_no_shuffle_x_1)
        exp_y_batches = self.apply_vocab(exp_shingle_no_shuffle_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            act_n_sequences += n_x_rows

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

        self.assertEqual(exp_n_sequences, act_n_sequences)

    def test_shingle_shuffle_method(self):
        params = {
            'filename': self.dataset_orig_file,
            'max_len': find_max_document_length(self.dataset_orig_file),
            'word2id': self.tf_vocab,
            'method': 'shingle',
            'shuffle': True,
            'batch_size': 4,
            'seq_len': 5,
            'random_seed': 1
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_shingle_shuffle_x_1)
        exp_y_batches = self.apply_vocab(exp_shingle_shuffle_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

    def test_slice_method(self):
        params = {
            'filename': self.dataset_1line_file,
            'max_len': find_max_document_length(self.dataset_1line_file),
            'word2id': self.tf_vocab,
            'method': 'slice',
            'batch_size': 4,
            'seq_len': 5,
            'random_seed': 1
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_slice_x_1)
        exp_y_batches = self.apply_vocab(exp_slice_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

    def test_fixed_sequences_per_line_method(self):
        params = {
            'filename': self.dataset_1doc_file,
            'max_len': find_max_document_length(self.dataset_1doc_file),
            'word2id': self.tf_vocab,
            'method': 'fixed_sequences_per_line',
            'batch_size': 4,
            'seq_len': 5
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_fixed_sequences_per_line_x_1)
        exp_y_batches = self.apply_vocab(exp_fixed_sequences_per_line_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

    def test_shingle_per_line_method(self):
        params = {
            'filename': self.dataset_1doc_file,
            'max_len': find_max_document_length(self.dataset_1doc_file),
            'word2id': self.tf_vocab,
            'method': 'shingle_per_line',
            'batch_size': 4,
            'seq_len': 5
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_shingle_per_line_x_1)
        exp_y_batches = self.apply_vocab(exp_shingle_per_line_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

    def test_slice_per_line_method(self):
        params = {
            'filename': self.dataset_1doc_file,
            'max_len': find_max_document_length(self.dataset_1doc_file),
            'word2id': self.tf_vocab,
            'method': 'slice_per_line',
            'batch_size': 4,
            'seq_len': 5,
            'random_seed': 3
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_slice_per_line_x_1)
        exp_y_batches = self.apply_vocab(exp_slice_per_line_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])

    def test_fixed_sequences_per_line_stateful_method(self):
        params = {
            'filename': self.dataset_1doc_file,
            'max_len': find_max_document_length(self.dataset_1doc_file),
            'word2id': self.tf_vocab,
            'method': 'fixed_sequences_per_line_stateful',
            'batch_size': 4,
            'seq_len': 5
        }
        dataset = make_dataset(**params)

        exp_x_batches = self.apply_vocab(exp_fixed_sequences_per_line_stateful_x_1)
        exp_y_batches = self.apply_vocab(exp_fixed_sequences_per_line_stateful_y_1)
        for i, (x, y) in enumerate(dataset):
            n_x_rows = tf.shape(x)[0].numpy()
            n_y_rows = tf.shape(y)[0].numpy()

            np.testing.assert_array_equal(x, exp_x_batches[i, 0:n_x_rows, :])
            np.testing.assert_array_equal(y, exp_y_batches[i, 0:n_y_rows, :])
