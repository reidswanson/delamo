# Python Modules
import unittest

# 3rd Party Modules
import tensorflow as tf

# Project Modules
from delamo.losses import MaskedSparseCategoricalCrossentropy


tf.config.run_functions_eagerly(True)

ln = tf.math.log


class TestCrossentropy(unittest.TestCase):
    def test_no_padding(self):
        y_true = tf.constant([[1, 2, 3],
                              [2, 3, 4],
                              [3, 2, 4]])

        y_pred = tf.random.uniform((3, 3, 5), minval=-1.0, maxval=1.0, seed=0)
        y_pred_softmax = tf.nn.softmax(y_pred)
        # print(y_pred)
        # print(y_pred_softmax)

        # [[[0.07720141 0.4392313  0.34454367 0.06949502 0.06952864]
        #   [0.25009677 0.28595504 0.12163788 0.14646924 0.19584109]
        #   [0.31739563 0.09163621 0.09365739 0.17525148 0.32205927]]
        #
        #  [[0.10393007 0.29699078 0.16499685 0.19239965 0.24168271]
        #   [0.05413055 0.3525192  0.07942444 0.26803896 0.24588682]
        #   [0.25691175 0.16896214 0.2632222  0.06926838 0.24163553]]
        #
        #  [[0.14605056 0.37748972 0.273582   0.10578957 0.09708813]
        #   [0.18076421 0.12942828 0.27681354 0.24016517 0.1728288 ]
        #   [0.11737691 0.28027532 0.06923025 0.40507597 0.12804158]]]

        xent = MaskedSparseCategoricalCrossentropy()

        exp_xent = tf.math.log([
            y_pred_softmax[0, 0, 1], y_pred_softmax[0, 1, 2], y_pred_softmax[0, 2, 3],
            y_pred_softmax[1, 0, 2], y_pred_softmax[1, 1, 3], y_pred_softmax[1, 2, 4],
            y_pred_softmax[2, 0, 3], y_pred_softmax[2, 1, 2], y_pred_softmax[2, 2, 4],
        ])
        exp_xent = -tf.math.reduce_sum(exp_xent) / tf.cast(tf.shape(exp_xent)[0], exp_xent.dtype)
        act_xent = xent(y_true, y_pred)

        self.assertAlmostEqual(act_xent.numpy(), exp_xent.numpy())