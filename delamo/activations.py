# ##############################################################################
#  Copyright 2018 The Google AI Language Team Authors and The HuggingFace Inc. team.
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import math

# 3rd Party Modules
import tensorflow as tf
import tensorflow.keras.layers as tfl

# Project Modules
from delamo.constraints import RangeConstraint


def gelu(x):
    """Gaussian Error Linear Unit.
    Original Implementation of the gelu activation function in Google Bert repo when initially created.
        For information: OpenAI GPT's gelu is slightly different (and gives slightly different results):
        0.5 * x * (1 + torch.tanh(math.sqrt(2 / math.pi) * (x + 0.044715 * torch.pow(x, 3))))
        Also see https://arxiv.org/abs/1606.08415
    """
    x = tf.convert_to_tensor(x)
    cdf = 0.5 * (1.0 + tf.math.erf(x / tf.math.sqrt(2.0)))

    return x * cdf


def gelu_new(x):
    """Gaussian Error Linear Unit.
    This is a smoother version of the GELU.
    Original paper: https://arxiv.org/abs/1606.08415
    Args:
        x: float Tensor to perform activation.
    Returns:
        `x` with the GELU activation applied.
    """
    x = tf.convert_to_tensor(x)
    pi = tf.cast(math.pi, x.dtype)
    coeff = tf.cast(0.044715, x.dtype)
    cdf = 0.5 * (1.0 + tf.tanh(tf.sqrt(2.0 / pi) * (x + coeff * tf.pow(x, 3))))

    return x * cdf


def mish(x):
    x = tf.convert_to_tensor(x)

    return x * tf.tanh(tf.math.softplus(x))


def gelu_fast(x):
    x = tf.convert_to_tensor(x)
    coeff1 = tf.cast(7978845608, x.dtype)
    coeff2 = tf.cast(0.044715, x.dtype)

    return 0.5 * x * (1.0 + tf.tanh(x * coeff2 * (1.0 + coeff1 * x * x)))


class PGeLU(tfl.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        # 0.1 <= gamma <= 10
        self.gamma = None

        # 0.001 <= beta <= 0.5
        self.beta = None

        # 0.0 <= alpha <= 0.05
        self.alpha = None

    def build(self, input_shape):
        self.gamma = self.add_weight(
            name='gamma',
            shape=[1],
            initializer=tf.keras.initializers.Constant(0.5),
            constraint=RangeConstraint(0.1, 10)
        )

        self.beta = self.add_weight(
            name='beta',
            shape=[1],
            initializer=tf.keras.initializers.Constant(tf.sqrt(2.0 / math.pi)),
            constraint=RangeConstraint(0.001, 0.5)
        )

        self.alpha = self.add_weight(
            name='alpha',
            shape=[1],
            initializer=tf.keras.initializers.Constant(0.044715),
            constraint=RangeConstraint(0, 0.05)
        )

    def call(self, x: tf.Tensor, **kwargs):
        cdf = self.gamma * (1.0 + tf.tanh(self.beta * (x + self.alpha * tf.pow(x, 3))))

        return x * tf.cast(cdf, dtype=x.dtype)

    def get_config(self):
        return super().get_config()


ACT2FN = {
    "gelu": tf.keras.layers.Activation(gelu),
    "relu": tf.keras.activations.relu,
    "swish": tf.keras.activations.swish,
    "gelu_new": tf.keras.layers.Activation(gelu_new),
    "mish": tf.keras.layers.Activation(mish),
    "tanh": tf.keras.activations.tanh,
    "gelu_fast": tf.keras.layers.Activation(gelu_fast),
}


def get_activation(activation_string):
    if activation_string in ACT2FN:
        return ACT2FN[activation_string]
    else:
        raise KeyError("function {} not found in ACT2FN mapping {}".format(activation_string, list(ACT2FN.keys())))
