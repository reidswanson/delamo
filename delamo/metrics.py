# ##############################################################################
#  Copyright 2020 Google Developers, Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# These metrics are derived from the transformer tutorial on the TensorFlow
# website:
# https://www.tensorflow.org/tutorials/text/transformer#loss_and_metrics

# Python Modules
from typing import Union

# 3rd Party Modules
import tensorflow as tf

# Project Modules


class MaskedSparseCategoricalCrossentropy(tf.keras.metrics.Metric):
    def __init__(
            self,
            mask_value: Union[int, float] = 0,
            name: str = 'masked_sparse_categorical_crossentropy',
            aggregation: str = 'mean',
            **kwargs
    ):
        super().__init__(name=name, **kwargs)

        self.mask_value = mask_value
        self.aggregation = aggregation.lower()

        if self.aggregation == 'mean':
            self.crossentropy = tf.keras.metrics.Mean()
        elif self.aggregation == 'sum':
            self.crossentropy = tf.keras.metrics.Sum()
        else:
            raise ValueError(
                f"Unknown aggregation type: '{aggregation}. "
                "Must be one of {mean, sum}."
            )

    def update_state(self, y_true: tf.Tensor, y_pred: tf.Tensor, *args, **kwargs):
        loss_ = tf.nn.sparse_softmax_cross_entropy_with_logits(y_true, y_pred)
        mask = tf.cast(tf.math.logical_not(tf.math.equal(y_true, self.mask_value)), loss_.dtype)

        self.crossentropy.update_state(loss_, sample_weight=mask)

    def result(self):
        return self.crossentropy.result()

    def reset_states(self):
        self.crossentropy.reset_states()


class MaskedSparseCategoricalAccuracy(tf.keras.metrics.Metric):
    def __init__(
            self,
            mask_value: Union[int, float] = 0,
            name: str = 'masked_sparse_categorical_accuracy',
            aggregation: str = 'mean',
            **kwargs
    ):
        super().__init__(name=name, **kwargs)

        self.mask_value = mask_value
        self.aggregation = aggregation.lower()

        if self.aggregation == 'mean':
            self.accuracy = tf.keras.metrics.Mean()
        elif self.aggregation == 'sum':
            self.accuracy = tf.keras.metrics.Sum()
        else:
            raise ValueError(
                f"Unknown aggregation type: '{aggregation}. "
                "Must be one of {mean, sum}."
            )

    def update_state(self, y_true: tf.Tensor, y_pred: tf.Tensor, *args, **kwargs):
        argmax = tf.cast(tf.math.argmax(y_pred, axis=-1), tf.int32)
        loss_ = tf.cast(tf.math.equal(y_true, argmax), y_pred.dtype)

        mask = tf.cast(tf.math.logical_not(tf.math.equal(y_true, self.mask_value)), loss_.dtype)

        self.accuracy.update_state(loss_, sample_weight=mask)

    def result(self):
        return self.accuracy.result()

    def reset_states(self):
        self.accuracy.reset_states()


class ShingledSparseCategoricalCrossentropy(tf.keras.metrics.Metric):
    def __init__(
            self,
            mask_value: Union[int, float] = 0,
            name: str = 'shingled_sparse_categorical_crossentropy',
            aggregation: str = 'mean',
            **kwargs
    ):
        super().__init__(name=name, **kwargs)

        self.mask_value = mask_value
        self.aggregation = aggregation.lower()

        if self.aggregation == 'mean':
            self.crossentropy = tf.keras.metrics.Mean()
        elif self.aggregation == 'sum':
            self.crossentropy = tf.keras.metrics.Sum()
        else:
            raise ValueError(
                f"Unknown aggregation type: '{aggregation}. "
                "Must be one of {mean, sum}."
            )

    def update_state(self, y_true: tf.Tensor, y_pred: tf.Tensor, *args, **kwargs):
        loss_ = tf.nn.sparse_softmax_cross_entropy_with_logits(y_true, y_pred)
        mask = tf.cast(
            tf.math.logical_not(tf.math.equal(y_true[:, -1], self.mask_value)),
            loss_.dtype
        )
        # Only consider the last token
        loss_ = loss_[:, -1]

        self.crossentropy.update_state(loss_, sample_weight=mask)

    def result(self):
        return self.crossentropy.result()

    def reset_states(self):
        self.crossentropy.reset_states()
