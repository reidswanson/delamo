# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import abc
import logging

from typing import Type, Dict, Any, List, Tuple

# 3rd Party Modules
import optuna
import tensorflow as tf

from tensorflow.keras.callbacks import EarlyStopping, ReduceLROnPlateau

# Project Modules
from delamo.cli.utils.dataset import make_dataset, find_max_document_length
from delamo.models.core import LanguageModel
from delamo.utils.vocab import py_vocab_from_file, tf_vocab_from_dict


log = logging.getLogger(__name__)


class Objective(abc.ABC):
    def __init__(
            self,
            train_data_file: str,
            valid_data_file: str,
            test_data_file: str,
            dataset_method: str,
            max_epochs: int,
            early_stopping_parameters: Dict[str, Any],
            plateau_parameters: Dict[str, Any],
            sequence_length_range: Tuple[int, int, int]
    ):
        self.train_data_file = train_data_file
        self.valid_data_file = valid_data_file
        self.test_data_file = test_data_file
        self.dataset_method = dataset_method

        self.early_stopping_parameters = early_stopping_parameters
        self.plateau_parameters = plateau_parameters

        self.max_epochs = max_epochs
        self.monitor = 'val_xent'
        self.mode = 'min'

        self.py_vocab = py_vocab_from_file(train_data_file)
        self.tf_vocab = tf_vocab_from_dict(self.py_vocab)

        self.max_doc_len = max(
            find_max_document_length(train_data_file),
            find_max_document_length(valid_data_file)
        )

        self.sequence_length_range = sequence_length_range

    def __call__(self, trial: optuna.Trial, **kwargs):
        params = self.make_parameters(trial)

        # These need to be before model.fit otherwise tensorflow will
        # wrap the nested dictionaries in a wrapper class that is not
        # JSON serializable.
        trial.set_user_attr('train_data_file', self.train_data_file)
        trial.set_user_attr('valid_data_file', self.valid_data_file)
        trial.set_user_attr('dataset_method', self.dataset_method)
        trial.set_user_attr('early_stopping_params', self.early_stopping_parameters)
        trial.set_user_attr('plateau_params', self.plateau_parameters)
        trial.set_user_attr('model_params', params)

        model = self.model_cls(
            vocabulary=self.py_vocab,
            dataset_method=self.dataset_method,
            **params
        )
        model = model.compile_model()
        train_data, valid_data, test_data = self._make_datasets(params)
        callbacks = self._make_callbacks(params)

        log.debug(f"Fitting model with parameters:\n{params}")

        fit_results = model.fit(
            train_data,
            epochs=self.max_epochs,
            validation_data=valid_data,
            callbacks=callbacks
        )

        evaluation_results = model.evaluate(test_data)
        evaluation_results = dict(zip(model.metrics_names, evaluation_results))

        trial.set_user_attr('n_epochs', len(fit_results.history['loss']))

        return evaluation_results['xent']

    def make_parameters(self, trial: optuna.Trial):
        model_params = self._make_model_params(trial)
        optimizer_params = self._make_optimizer_params(trial)

        return {**model_params, **optimizer_params}

    @property
    @abc.abstractmethod
    def model_cls(self) -> Type[LanguageModel]:
        raise NotImplementedError()

    # region Utility Methods
    def _make_model_params(self, trial: optuna.Trial) -> Dict[str, Any]:
        return {
            'sequence_length': trial.suggest_int('sequence_length', *self.sequence_length_range)
        }

    @classmethod
    def _make_optimizer_params(cls, trial: optuna.Trial) -> Dict[str, Any]:
        # optimizer_name = trial.suggest_categorical('optimizer_name', ['adam', 'sgd', 'transformer'])
        optimizer_name = trial.suggest_categorical('optimizer_name', ['adam', 'sgd'])

        if optimizer_name == 'sgd':
            optimizer_params = {
                'learning_rate': trial.suggest_loguniform('sgd_learning_rate', 1e-7, 1e-3),
                'momentum': trial.suggest_loguniform('sgd_momentum', 0.7, 0.99),
                'nesterov': trial.suggest_categorical('sgd_nesterov', [True, False]),
                'clipnorm': trial.suggest_uniform('sgd_clipnorm', 0.05, 1)
            }
        elif optimizer_name == 'transformer':
            optimizer_params = {
                'schedule_size': trial.suggest_int('xfmr_schedule_size', 64, 1024, 16),
                'schedule_warmup': trial.suggest_int('xfmr_schedule_warmup', 20000, 100000, 1000),
                'beta_1': trial.suggest_loguniform('xfmr_beta_1', 0.8, 0.99),
                'beta_2': trial.suggest_loguniform('xfmr_beta_2', 0.9, 0.9999),
                'epsilon': trial.suggest_loguniform('xfmr_epsilon', 1e-10, 1e-7),
                'clipnorm': trial.suggest_uniform('xfmr_clipnorm', 0.05, 1)
            }
        else:
            optimizer_params = {
                'learning_rate': trial.suggest_loguniform('adam_learning_rate', 1e-7, 1e-3),
                'beta_1': trial.suggest_loguniform('adam_beta_1', 0.8, 0.99),
                'beta_2': trial.suggest_loguniform('adam_beta_2', 0.9, 0.9999),
                'epsilon': trial.suggest_loguniform('adam_epsilon', 1e-10, 1e-7),
                'amsgrad': trial.suggest_categorical('adam_amsgrad', [True, False]),
                'clipnorm': trial.suggest_uniform('adam_clipnorm', 0.05, 1)
            }

        return {
            'optimizer_params': {
                'name': optimizer_name,
                'args': optimizer_params
            }
        }

    def _make_callbacks(self, params: Dict[str, Any]) -> List[tf.keras.callbacks.Callback]:
        callbacks = [
            EarlyStopping(
                monitor=self.monitor,
                mode=self.mode,
                restore_best_weights=True,
                **self.early_stopping_parameters
            )
        ]

        if params['optimizer_params']['name'] != 'transformer':
            callbacks += [
                ReduceLROnPlateau(
                    monitor=self.monitor,
                    mode=self.mode,
                    **self.plateau_parameters
                )
            ]

        return callbacks

    def _make_datasets(self, params: Dict[str, Any]):
        test_method = (
            self.dataset_method if 'fixed_sequences' in self.dataset_method else
            'fixed_sequences'
        )

        train_data = make_dataset(
            self.train_data_file,
            self.max_doc_len,
            self.tf_vocab,
            params['batch_size'],
            params['sequence_length'],
            method=self.dataset_method
        )

        valid_data = make_dataset(
            self.valid_data_file,
            self.max_doc_len,
            self.tf_vocab,
            params['batch_size'],
            params['sequence_length'],
            method=test_method
        )

        test_data = make_dataset(
            self.test_data_file,
            self.max_doc_len,
            self.tf_vocab,
            params['batch_size'],
            params['sequence_length'],
            method=test_method
        )

        return train_data, valid_data, test_data
    # endregion Utility Methods
