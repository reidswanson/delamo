# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import copy
import logging
import math

from typing import Dict, Any, Type, Tuple

# 3rd Party Modules
import optuna

# Project Modules
import delamo.models.transformer as transformer

from delamo.hyopt.core import Objective
from delamo.models.core import LanguageModel
from delamo.utils.gpu import get_max_available_memory


log = logging.getLogger(__name__)


class DecoderOnlyObjective(Objective):
    def __init__(
            self,
            train_data_file: str,
            valid_data_file: str,
            test_data_file: str,
            dataset_method: str,
            max_epochs: int,
            early_stopping_parameters: Dict[str, Any],
            plateau_parameters: Dict[str, Any],
            sequence_length_range: Tuple[int, int, int] = (30, 1000, 10)
    ):
        super().__init__(
            train_data_file,
            valid_data_file,
            test_data_file,
            dataset_method,
            max_epochs,
            early_stopping_parameters,
            plateau_parameters,
            sequence_length_range=sequence_length_range
        )

        self.n_layers_range = (1, 8, 1)
        self.n_model_dim_range = (32, 512, 16)
        self.n_heads_range = (2, 24, 1)
        self.n_ff_dim_range = (256, 1024, 16)
        self.dropout_range = (0.0, 0.9)

    def make_parameters(self, trial: optuna.Trial):
        # This should be a class field
        parameters = super().make_parameters(trial)
        parameters.update({
            'n_layers': trial.suggest_int('n_layers', *self.n_layers_range),
            'n_model_dim': trial.suggest_int('n_model_dim', *self.n_model_dim_range),
            'n_heads': trial.suggest_int('n_heads', *self.n_heads_range),
            'n_ff_dim': trial.suggest_int('n_ff_dim', *self.n_ff_dim_range),
            'dropout': trial.suggest_uniform('dropout', *self.dropout_range),
        })
        parameters['batch_size'] = self.estimate_batch_size(parameters)

        return parameters

    @property
    def model_cls(self) -> Type[LanguageModel]:
        return transformer.DecoderOnlyLanguageModel

    # region Utility Methods
    def estimate_batch_size(self, parameters: Dict[str, Any]) -> int:
        # Get the amount of memory available on the GPU with the largest pool of
        # free memory. Hold out some additional memory (the value is just a guess).
        available_memory = get_max_available_memory(0.0)

        # This is a total hack, but for some reason the memory usage estimate
        # does not work well when the batch size gets too large. This tends
        # to happen with short sequence lengths. To compensate I add a
        # sequence length penalty to the set of parameters the memory usage
        # estimator will use. This is a total hack, but seems to work
        # moderately well.
        temp_model = transformer.DecoderOnlyLanguageModel(
            vocabulary=self.py_vocab,
            dataset_method='fixed_sequences',
            **copy.deepcopy(parameters)
        )
        memory_usage = temp_model.estimate_memory_usage(parameters, len(self.py_vocab))
        # memory_usage = transformer.DecoderOnlyLanguageModel.estimate_memory_usage(
        #     parameters,
        #     len(self.py_vocab)
        # )

        # Memory usage appears to be some function of batch_size, seq_len, n_units (model and/or
        # feed forward). The most natural (to me) function is something like:
        # a * (batch_size * seq_len^2 * n_units)^c + b
        # However, when fiddling with batch sizes and observing the memory used it appears
        # to be something closer to:
        # a * (batch_size^c * seq_len^2 * n_units) + b
        # Although both are hacks and neither seem to work all that well.
        # The following parameter seem to work moderately well in that it doesn't
        # appear to ever underestimate the memory used and is much better than
        # picking a small fixed batch size that is guaranteed to work across all search
        # parameters.
        # Also note, these numbers are tuned to my current video card (gtx 1080), which has
        # 8 GB of memory. These values may not be as good with more or less memory available.
        a, b, c = 0.175, 0.0, 1.8
        batch_size = int(math.floor(((available_memory - b) / (a * memory_usage))**(1/c)))

        log.debug(f"available_memory: {available_memory}")
        log.debug(f"initial batch_size: {batch_size}")

        if batch_size == 0:
            log.warning(f"The estimated memory usage is greater than the available memory. "
                        f"It is likely there will be an OOM error.")
            batch_size = 1

        # But it should also be divisible by 8 to enable TPU processing when available.
        # So, find the largest int that is smaller than the current value that is divisible by
        # 8. If it's less than 8, then there's no point in making it smaller.
        while batch_size % 8 != 0 and batch_size > 8:
            batch_size -= 1

        log.debug(f"final batch_size: {batch_size}")
        parameters['batch_size'] = batch_size

        if batch_size * memory_usage > available_memory:
            log.warning(f"The estimated memory usage is greater than the available memory. "
                        f"It is likely there will be an OOM error.")

        return batch_size
    # endregion Utility Methods
