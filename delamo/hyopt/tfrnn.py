# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import copy
import logging
from typing import Type, Dict, Any, Tuple

# 3rd Party Modules
import optuna

# Project Modules
import delamo.models.tfrnn as tfrnn

from delamo.hyopt.core import Objective
from delamo.models.core import LanguageModel
from delamo.utils.gpu import get_max_available_memory


log = logging.getLogger(__name__)


class RnnObjective(Objective):
    def __init__(
            self,
            cell_type: str,
            train_data_file: str,
            valid_data_file: str,
            test_data_file: str,
            dataset_method: str,
            max_epochs: int,
            early_stopping_parameters: Dict[str, Any],
            plateau_parameters: Dict[str, Any],
            sequence_length_range: Tuple[int, int, int] = (10, 100, 10)
    ):
        super().__init__(
            train_data_file,
            valid_data_file,
            test_data_file,
            dataset_method,
            max_epochs,
            early_stopping_parameters,
            plateau_parameters,
            sequence_length_range
        )

        self.cell_type = cell_type
        self.n_layers_range = (1, 3, 1)
        self.n_embedding_units_range = (32, 512, 16)
        self.n_units_range = (64, 1024, 16)
        self.dropout_range = (0.0, 0.9)
        self.recurrent_dropout_range = (0.0, 0.9)

    def make_parameters(self, trial: optuna.Trial):
        parameters = super().make_parameters(trial)
        parameters.update({
            'cell_type': self.cell_type,
            'n_layers': trial.suggest_int('n_layers', *self.n_layers_range),
            'n_embedding_units': trial.suggest_int('n_embedding_units', *self.n_embedding_units_range),
            'n_units': trial.suggest_int('n_units', *self.n_units_range),
            'dropout_rate': trial.suggest_uniform('dropout_rate', *self.dropout_range),
            'recurrent_dropout_rate': trial.suggest_uniform('recurrent_dropout_rate', *self.recurrent_dropout_range),
        })
        parameters['batch_size'] = self.estimate_batch_size(parameters)

        return parameters

    @property
    def model_cls(self) -> Type[LanguageModel]:
        return tfrnn.RnnLanguageModel

    def estimate_batch_size(self, parameters: Dict[str, Any]) -> int:
        available_memory = get_max_available_memory(0.0)

        temp_model = tfrnn.RnnLanguageModel(
            batch_size=1,
            vocabulary=self.py_vocab,
            dataset_method='fixed_sequences',
            **copy.deepcopy(parameters)
        )
        temp_model.build((parameters['sequence_length'], len(self.py_vocab)))
        memory_usage = temp_model.estimate_memory_usage(parameters, len(self.py_vocab))
        n_layers = parameters['n_layers']
        n_embedding_units = parameters['n_embedding_units']
        n_units = parameters['n_units']

        log.debug(f"Estimated memory usage per example: {memory_usage}")

        a, b, c = 1.0, 0.0, 0.66
        max_units = 1.25 * (self.n_embedding_units_range[1] + n_layers**1.33 * self.n_units_range[1])
        total_units = n_embedding_units + (1.0 / n_layers) * n_units
        memory_usage = (max_units / total_units)**c * (a * memory_usage)
        batch_size = int(available_memory / memory_usage)

        log.debug(f"Max units: {self.n_embedding_units_range[1] + self.n_units_range[1]}")
        log.debug(f"Adjusted max units: {max_units:.2f}")
        log.debug(f"Total units: {n_embedding_units + n_units}")
        log.debug(f"Adjusted total units: {total_units:.2f}")
        log.debug(f"Unit weight: {(max_units / total_units)**c:.2f}")
        log.debug(f"Adjusted memory usage: {memory_usage:.2f}")
        log.debug(f"Initial batch_size: {batch_size}")

        if batch_size == 0:
            log.warning(f"The estimated memory usage is greater than the available memory. "
                        f"It is likely there will be an OOM error.")
            batch_size = 1

        # But it should also be divisible by 8 to enable TPU processing when available.
        # So, find the largest int that is smaller than the current value that is divisible by
        # 8. If it's less than 8, then there's no point in making it smaller.
        while batch_size % 8 != 0 and batch_size > 8:
            batch_size -= 1

        log.debug(f"final batch_size: {batch_size}")
        parameters['batch_size'] = batch_size

        if batch_size * memory_usage > available_memory:
            log.warning(f"The estimated memory usage is greater than the available memory. "
                        f"It is likely there will be an OOM error.")

        return batch_size
