# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import logging
import math

from typing import Dict, Any, Type

# 3rd Party Modules
import optuna
import tensorflow as tf

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
import delamo.models.xfmr as xfmr

from delamo.hyopt.core import Objective
from delamo.models.core import LanguageModel
from delamo.utils.gpu import get_max_available_memory
from delamo.utils.math import factors

log = logging.getLogger(__name__)


class XfmrObjective(Objective):
    def __init__(
            self,
            train_data_file: str,
            valid_data_file: str,
            test_data_file: str,
            dataset_method: str,
            max_epochs: int,
            early_stopping_parameters: Dict[str, Any],
            plateau_parameters: Dict[str, Any],
            **kwargs
    ):
        super().__init__(
            train_data_file,
            valid_data_file,
            test_data_file,
            dataset_method,
            max_epochs,
            early_stopping_parameters,
            plateau_parameters,
            sequence_length_range=kwargs.get('sequence_length_range', (100, 500, 20))
        )
        
        self.n_decoder_layers_range = kwargs.get('n_decoder_layers_range', (1, 16, 1))
        self.n_feed_forward_layers_range = kwargs.get('n_feed_forward_layers_range', (1, 8, 1))
        self.n_embedding_units_range = kwargs.get('n_embedding_units_range', (64, 1024, 32))
        self.n_attention_state_units_range = kwargs.get('n_attention_state_units_range', (64, 1024, 32))
        self.n_feed_forward_units_range = kwargs.get('n_feed_forward_units_range', (64, 1024, 32))
        self.embedding_dropout_rate_range = kwargs.get('embedding_dropout_rate_range', (0, 0.9))
        self.attention_dropout_rate_range = kwargs.get('attention_dropout_rate_range', (0, 0.9))
        self.residual_dropout_rate_range = kwargs.get('residual_dropout_rate_range', (0, 0.9))
        self.decoder_dropout_rate_range = kwargs.get('decoder_dropout_rate_range', (0.0, 0.9))
        self.layer_norm_epsilon_range = kwargs.get('layer_norm_epsilon_range', (1e-9, 1e-6))
        self.n_heads_range = kwargs.get('n_heads_range', (1, 32, 1))
        self.attention_activation_function_range = kwargs.get('attention_activation_function_range', ('softmax', 'relu', 'sigmoid', 'tanh'))
        self.head_split_method_range = kwargs.get('head_split_method_range', ('reshape', 'convolution', 'sample'))
        self.head_sample_size_range = kwargs.get('head_sample_size', (8, 256, 8))
        self.normalization_method_range = kwargs.get('head_normalization_method_range', ('none', 'layer', 'batch'))
        self.decoder_update_method_range = kwargs.get('decoder_update_method_range', ('simple', 'gated', 'additive'))
        self.scale_attention_range = kwargs.get('scale_attention_range', (True, False))
        self.activation_function_range = kwargs.get('activation_function_range', ('gelu', 'relu', 'gelu_new', 'pgelu'))
        self.use_gpt_convolution_range = kwargs.get('use_gpt_activation_range', (True, False))
        # self.auxiliary_loss_method_range = kwargs.get('auxiliary_loss_method_range', ('none', 'depth_weighted'))
        self.auxiliary_loss_method_range = kwargs.get('auxiliary_loss_method_range', ('none',))
        self.loss_weight_n_range = kwargs.get('loss_weight_n_range', (1.0, 5.0))
        self.loss_weight_lambda_range = kwargs.get('loss_weight_lambda_range', (1e-7, 1e-3))
        self.loss_weight_normalized_range = kwargs.get('loss_weight_normalized_range', (True, False))

    def __call__(self, trial: optuna.Trial, **kwargs):
        # I've got to override this because the model takes a config object and not a dictionary.

        params = self.make_parameters(trial)

        # These need to be before model.fit otherwise tensorflow will
        # wrap the nested dictionaries in a wrapper class that is not
        # JSON serializable.
        trial.set_user_attr('train_data_file', self.train_data_file)
        trial.set_user_attr('valid_data_file', self.valid_data_file)
        trial.set_user_attr('dataset_method', self.dataset_method)
        trial.set_user_attr('early_stopping_params', self.early_stopping_parameters)
        trial.set_user_attr('plateau_params', self.plateau_parameters)
        trial.set_user_attr('model_params', params)

        if params['batch_size'] <= 1:
            return float('nan')

        # noinspection PyArgumentList
        model = self.model_cls(
            vocabulary=self.py_vocab,
            config=params['config']
        )
        model = model.compile_model()
        train_data, valid_data, test_data = self._make_datasets(params)
        callbacks = self._make_callbacks(params)

        fit_results = model.fit(
            train_data,
            epochs=self.max_epochs,
            validation_data=valid_data,
            callbacks=callbacks
        )

        evaluation_results = model.evaluate(test_data)
        evaluation_results = dict(zip(model.metrics_names, evaluation_results))

        trial.set_user_attr('n_epochs', len(fit_results.history['loss']))

        tf.keras.backend.clear_session()

        return evaluation_results['xent']

    @property
    def model_cls(self) -> Type[LanguageModel]:
        return xfmr.Xfmr

    def make_parameters(self, trial: optuna.Trial):
        # Some useful function aliases
        rint = trial.suggest_int
        runi = trial.suggest_uniform
        rlog = trial.suggest_loguniform
        rcat = trial.suggest_categorical

        parameters = super().make_parameters(trial)

        auxiliary_loss_method = rcat('auxiliary_loss_method', self.auxiliary_loss_method_range)
        if auxiliary_loss_method == 'depth_weighted':
            output_predictions_per_layer = True
            loss_weight_n = runi('loss_weight_n', *self.loss_weight_n_range)
            loss_weight_lambda = rlog('loss_weight_lambda', *self.loss_weight_lambda_range)
            loss_weight_normalized = rcat('loss_weight_normalized', self.loss_weight_normalized_range)
        else:
            output_predictions_per_layer = False
            loss_weight_n = None
            loss_weight_lambda = None
            loss_weight_normalized = None

        n_attention_state_units = rint('n_attention_state_units', *self.n_attention_state_units_range)
        n_embedding_units = rint('n_embedding_units', *self.n_embedding_units_range)
        head_split_method = rcat('head_split_method', self.head_split_method_range)

        n_heads = rint('n_heads', *self.n_heads_range)

        if head_split_method == 'reshape':
            attention_state_factors = factors(n_attention_state_units, *self.n_heads_range[:2])

            tmp_heads = 1
            for factor in attention_state_factors:
                if factor <= n_heads and factor <= self.n_heads_range[1]:
                    tmp_heads = factor
                    log.debug(f"tmp_heads: {tmp_heads} - n_heads: {n_heads}")
            n_heads = tmp_heads

            log.debug(f"n_attention_state_units: {n_attention_state_units}")
            log.debug(f"attention_state_factors: {attention_state_factors}")
            log.debug(f"n_heads: {n_heads}")

        if head_split_method == 'sample':
            head_sample_size = rint('head_sample_size', *self.head_sample_size_range)
        else:
            head_sample_size = 1

        parameters['config'] = xfmr.XfmrConfig(
            dataset_method=self.dataset_method,
            sequence_length=parameters['sequence_length'],
            n_decoder_layers=rint('n_decoder_layers', *self.n_decoder_layers_range),
            n_feed_forward_layers=rint('n_feed_forward_layers', *self.n_feed_forward_layers_range),
            n_embedding_units=n_embedding_units,
            n_attention_state_units=n_attention_state_units,
            n_feed_forward_units=rint('n_feed_forward_units', *self.n_feed_forward_units_range),
            embedding_dropout_rate=runi('embedding_dropout_rate', *self.embedding_dropout_rate_range),
            attention_dropout_rate=runi('attention_dropout_rate', *self.attention_dropout_rate_range),
            residual_dropout_rate=runi('residual_dropout_rate', *self.residual_dropout_rate_range),
            decoder_dropout_rate=runi('decoder_dropout_rate', *self.decoder_dropout_rate_range),
            layer_norm_epsilon=rlog('layer_norm_epsilon', *self.layer_norm_epsilon_range),
            n_heads=n_heads,
            attention_activation_function=rcat('attention_activation_function', self.attention_activation_function_range),
            head_split_method=head_split_method,
            head_sample_size=head_sample_size,
            normalization_method=rcat('normalization_method', self.normalization_method_range),
            output_predictions_per_layer=output_predictions_per_layer,
            loss_weight_n=loss_weight_n,
            loss_weight_lambda=loss_weight_lambda,
            loss_weight_normalized=loss_weight_normalized
        )

        log.debug(f"Estimating batch size from parameters:\n{parameters}")
        batch_size = self.estimate_batch_size(parameters)

        parameters['batch_size'] = batch_size

        return parameters

    # noinspection PyMethodMayBeStatic
    def estimate_batch_size(self, parameters: Dict[str, Any]) -> int:
        # Get the amount of memory available on the GPU with the largest pool of
        # free memory. Hold out some additional memory (the value is just a guess).
        # available_memory = get_max_available_memory(0.0)
        policy_name = mixed_precision.global_policy().name
        dtype_size = 2 if '16' in policy_name else 4 if '32' in policy_name else 8

        cfg = parameters['config']
        seq_len = cfg['sequence_length']
        n_embedding_units = cfg['n_embedding_units']
        n_attention_state_units = cfg['n_attention_state_units']
        n_ff_units = cfg['n_feed_forward_units']
        n_heads = cfg['n_heads']
        sample_size = cfg['head_sample_size']
        n_feed_forward_layers = cfg['n_feed_forward_layers']

        model = xfmr.Xfmr(
            vocabulary=self.py_vocab,
            config=parameters['config']
        )
        model = model.compile_model()
        dummy_data = tf.random.uniform((1, seq_len), maxval=len(self.py_vocab), dtype=tf.int32)
        model(dummy_data, training=True)

        available_mem = get_max_available_memory()

        # At a minimum, I assume TensorFlow will try to keep all the parameters
        # of the model in memory. In addition it will need some overhead for
        # CUDA and some for TensorFlow. There will also be memory needed to
        # throughout the forward and backward passes of the network and temporary
        # memory needed to perform operations.

        # Assume the model variables are always stored using 4 bytes.
        # (Computations maybe be done using 2 bytes, e.g., using a mixed_float16
        # policy).
        model_mem = model.count_params() * 4

        # Estimate that Tensorflow will need double the number of parameters for
        # persistent storage of various calculations during the forward and backward
        # pass. I have no actual knowledge of what this should be so it is just
        # a complete shot in the dark.
        model_mem *= 2

        # Assume about 100MB overhead for both CUDA and TensorFlow
        cuda_overhead = 100_000_000
        tf_overhead = 100_000_000
        overhead = cuda_overhead + tf_overhead

        # The storage for computations should be temporary, so we should need
        # to sum up the requirements for each layer. To estimate the maximum
        # amount of memory we should only need to know the computation that
        # requires the maximum amount of storage).
        # This Nvidia documentation might provide a hint on how much memory is
        # needed for a matrix calculation.
        # https://docs.nvidia.com/deeplearning/performance/dl-performance-matrix-multiplication/index.html
        # However, the formulas related to memory are about how many times memory
        # needs to be accessed, not necessarily how much memory is need to store
        # the computations.
        # If A is an MxK matrix and B is a KxN matrix, then 2 * (M*K + K*N + M*N)
        # byte accesses are required.
        a, d = available_mem, dtype_size
        o, m = overhead, model_mem
        z = sample_size
        s, e, t, f = seq_len, n_embedding_units, n_attention_state_units, n_ff_units
        h = n_heads * math.log(n_heads)

        # This often significantly overestimates the amount of memory needed
        # (so the batch size is smaller than optimal), but even so it still seems
        # to sometimes underestimates the memory needed and will cause an OOM
        # error.
        if cfg['head_split_method'] == 'sample':
            x1 = o + 2 * m + 2 * d * s**1.5 * t
            x2 = d * s * e + d * s * h * z + d * s * f + d * s * t
            sqrt = math.sqrt(x1**2 + 8 * a * x2)
        elif cfg['head_split_method'] == 'convolution':
            x1 = o + 2 * m + 2 * d * s**1.5 * t
            x2 = d * s * t + d * s * h * t * f + d * s * f
            sqrt = math.sqrt(x1**2 + 8 * a * x2)
        else:
            x1 = o + 2 * m + 2 * d * s**1.5 * t
            x2 = d * s * e + d * s * t * f
            sqrt = math.sqrt(x1**2 + 8 * a * x2)

        numerator = (sqrt - x1)
        denominator = 4 * x2
        batch_size = int(numerator / denominator)

        log.debug(f"Initial batch_size: {batch_size}")

        if batch_size <= 1:
            log.warning(f"The estimated memory usage is greater than the available memory. "
                        f"It is likely there will be an OOM error.")
            return 0

        # But it should also be divisible by 8 to enable TPU processing when available.
        # So, find the largest int that is smaller than the current value that is divisible by
        # 8. If it's less than 8, then there's no point in making it smaller.
        while batch_size % 8 != 0 and batch_size > 8:
            batch_size -= 1

        log.debug(f"final batch_size: {batch_size}")
        parameters['batch_size'] = batch_size

        # if batch_size * mem_usage > available_memory:
        #     log.warning(f"The estimated memory usage is greater than the available memory. "
        #                 f"It is likely there will be an OOM error.")

        return batch_size
