# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import gzip
import logging
import random

from typing import Set

# 3rd Party Modules
import numpy as np

# Project Modules
import delamo.cli.utils.commandline as commandline

from delamo.utils.word2vec import WordVectors


log = logging.getLogger('word2vec_subset')


def read_vocab(filename: str) -> Set[str]:
    fopen = gzip.open if filename.endswith('.gz') else open

    vocab = {'', '<s>', '</s>', '<unk>', '<br>'}

    with fopen(filename, 'rt') as fh:
        for line in fh:
            vocab.update(line.split())

    return vocab


def fill_missing(word: str, w2v: WordVectors) -> WordVectors:
    if word == '<s>':
        # Approximate the start token with the average of all uppercase words.
        vectors = [v for k, v in w2v.items() if k and k[0].isupper()]
    elif word == '</s>' or '<br>':
        # Approximate end of document and end of line tokens with punctuation-like
        # words.
        vectors = [v for k, v in w2v.items() if k and k[-1] in ('.', '?', '!')]
    else:
        # Approximate all other unknown words with a random sample of vectors.
        sampled_words = set(random.sample(w2v.keys(), 50))
        vectors = [v for k, v in w2v.items() if k in sampled_words]

    vector = np.mean(vectors, axis=0)

    assert vector.shape == vectors[0].shape

    w2v[word] = vector

    return w2v


def main(args: argparse.Namespace):
    vocab = read_vocab(args.train_file)
    w2v = WordVectors.load(args.word2vec_file, vocab)
    missing = vocab - set(w2v.keys())

    log.info(f"Vocabulary size: {len(vocab)}")
    log.info(f"Word vectors size: {len(w2v)}")
    log.info(f"Missing words: {len(missing)}")

    for word in missing:
        fill_missing(word, w2v)

    log.info(f"Saving to file: {args.output_file}")
    w2v.save(args.output_file)


def make_options():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--log-level',
        type=commandline.log_level,
        default=logging.INFO,
        help="Specify the log level."
    )

    parser.add_argument(
        '--word2vec-file',
        required=True,
        help="The path to the word2vec vectors."
    )
    parser.add_argument(
        '--train-file',
        required=True,
        help="The path to a training data file that can be used to infer the vocabulary."
    )

    parser.add_argument(
        '--output-file',
        required=True,
        help="The path where the output vectors will be written."
    )

    parser.set_defaults(func=main)

    return parser


if __name__ == '__main__':
    cli = make_options()

    cli_args = cli.parse_args()

    logging.basicConfig(
        level=cli_args.log_level,
        format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
    )

    cli_args.func(cli_args)
