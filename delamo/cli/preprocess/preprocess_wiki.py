# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################
"""
Preprocess WikiText data files so that each document/article is on
its own line. It will also replace newline characters with a special token
(``<br>``) and optionally add ``<s>`` and ``</s>`` tags before and after
each document respectively.
"""

# Python Modules
import argparse
import gzip
import os
import re

from typing import List

# 3rd Party Modules

# Project Modules


def write_document(buffer: List[str], include_boundaries: bool, is_single_line: bool, fh):
    output = ' '.join([(s + ' <br>').strip() for s in buffer[:-3]])
    if include_boundaries:
        output = '<s> ' + output + ' </s>'

    end = ' ' if is_single_line else os.linesep

    fh.write(output + end)


def is_flushable(buffer: List[str]) -> bool:
    # If the buffer is empty there is nothing to flush.
    if len(buffer) < 4:
        return False

    if buffer[-3] or buffer[-1]:
        return False

    # I don't think this is needed anymore, but I also don't think it hurts
    return re.match(r'^=[^;=]+=$', buffer[-2]) is not None


def main(args: argparse.Namespace):
    input_compression = 'gzip' if args.input_file.endswith('.gz') else None
    output_compression = 'gzip' if args.output_file.endswith('.gz') else None

    in_open = gzip.open if input_compression else open
    out_open = gzip.open if output_compression else open

    buffer = []
    with in_open(args.input_file, 'rt') as in_fh, out_open(args.output_file, 'wt') as out_fh:
        for line in in_fh:
            buffer.append(line.strip())

            if is_flushable(buffer):
                write_document(buffer, args.include_boundaries, args.single_line, out_fh)
                buffer = buffer[-3:]

        if buffer:
            write_document(buffer, args.include_boundaries, args.single_line, out_fh)


def make_options():
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-file', required=True)
    parser.add_argument('--output-file', required=True)
    parser.add_argument('--include-boundaries', action='store_true')
    parser.add_argument('--single-line', action='store_true')

    return parser


if __name__ == '__main__':
    cli = make_options()

    cli.set_defaults(func=main)

    cli_args = cli.parse_args()
    cli_args.func(cli_args)
