# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

"""
A program that reduces the vocabulary of a file by replacing more words
by special ``<unk>`` tokens. First, it will keep all words that are in a given
vocabulary list. Second, if the word contains any non-ascii characters it will
will replace the entire word with a special ``<fw>`` token (foreign word).
Third, it will replace any numbers in the word with ``0``. This will keep
some information about the number (e.g., if it is likely a date, year,
phone number, etc), but prevent an explosion of numeric tokens. Finally,
it will replace words whose first letter is uppercase by ``<Unk>``, words
that are entirely uppercase by ``<UNK>`` and all other unknown words by
``<unk>``.
"""

# Python Modules
import argparse
import gzip
import os
import re

from typing import Set

# 3rd Party Modules

# Project Modules


def read_vocab(filename: str) -> Set[str]:
    with open(filename, 'rt') as fh:
        return {t.strip() for t in fh}


def map_token(token: str, vocab: Set[str]) -> str:
    if token in vocab:
        return token

    # If the word is not entirely made of ascii characters consider it
    # a foreign word and return the special token.
    if not token.isascii():
        return '<fw>'

    # Replace all individual numbers with a single value.
    # This still keeps some information about the number like whether
    # it is (probably) a date, year, etc.
    new_token = re.sub(r'[0-9]', '0', token)

    # If we replaced any characters then return the modified string
    if new_token != token:
        return new_token

    # Check the case of the token and return a special unknown token reflecting
    # the case of the original word.
    if token.isupper():
        return '<UNK>'

    if token[0].isupper():
        return '<Unk>'

    return '<unk>'


def main(args: argparse.Namespace):
    vocab = read_vocab(args.vocabulary_file)
    input_compression = 'gzip' if args.input_file.endswith('.gz') else None
    output_compression = 'gzip' if args.output_file.endswith('.gz') else None

    in_open = gzip.open if input_compression else open
    out_open = gzip.open if output_compression else open

    with in_open(args.input_file, 'rt') as in_fh, out_open(args.output_file, 'wt') as out_fh:
        for line in in_fh:
            tokens = line.split()

            # Map each token according to the rules laid out above.
            tokens = [map_token(t, vocab) for t in tokens]

            line = ' '.join(tokens)

            out_fh.write(line + os.linesep)


def make_options():
    parser = argparse.ArgumentParser(description=__doc__)

    parser.add_argument('--vocabulary-file', required=True)
    parser.add_argument('--input-file', required=True)
    parser.add_argument('--output-file', required=True)

    return parser


if __name__ == '__main__':
    cli = make_options()

    cli.set_defaults(func=main)

    cli_args = cli.parse_args()
    cli_args.func(cli_args)
