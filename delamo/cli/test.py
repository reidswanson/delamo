# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import logging

# 3rd Party Modules
import tensorflow as tf

# Project Modules
import delamo.cli.utils.commandline as commandline

from delamo.cli.utils.dataset import make_dataset, find_max_document_length
from delamo.metrics import ShingledSparseCategoricalCrossentropy as ShingledCrossentropy, \
    MaskedSparseCategoricalCrossentropy as Crossentropy, \
    MaskedSparseCategoricalAccuracy as Accuracy
from delamo.models.core import LanguageModel
from delamo.utils.vocab import tf_vocab_from_dict


log = logging.getLogger('delamo_test')


for device in tf.config.list_physical_devices('GPU'):
    tf.config.experimental.set_memory_growth(device, True)


def test(args: argparse.Namespace):
    tf.config.run_functions_eagerly(args.run_eagerly)
    model = LanguageModel.load_from_files(args.model_config_file, args.model_weights_file)

    tf_vocab = tf_vocab_from_dict(model.vocabulary)
    seq_len = model.sequence_length
    dataset_method = model.dataset_method

    # Do a "regular" evaluation first
    test_dataset = make_dataset(
        args.test_file,
        max_len=find_max_document_length(args.test_file),
        word2id=tf_vocab,
        batch_size=args.batch_size,
        seq_len=seq_len,
        include_boundaries=not args.exclude_boundaries,
        method=dataset_method,
    )
    model.compile(metrics=[
        Accuracy(name='mean_accuracy'),
        Crossentropy(name='mean_xent1', aggregation='mean'),
        Crossentropy(name='total_xent1', aggregation='sum')
    ])

    log.info(
        "Evaluating on the test data using the same dataset method and metrics as training. "
        "On the one hand this will underestimate the performance of the model because the full "
        "context is not used to predict every model. Namely, each sequence is independent so "
        "the words earlier in the sequence have less context than words later in the sequence. "
        "On the other hand it may include many start of document tokens which are easy to predict "
        "and are not indicative of the performance of the other words."
    )
    results = model.evaluate(test_dataset, verbose=args.keras_verbosity)
    results = {name: result for name, result in zip(model.metrics_names, results)}

    for metric, value in results.items():
        log.info(f"{metric:20s}: {value}")

    # Now do a "shingled" evaluation
    test_dataset = make_dataset(
        args.test_file,
        max_len=find_max_document_length(args.test_file),
        word2id=tf_vocab,
        batch_size=args.batch_size,
        seq_len=seq_len,
        include_boundaries=True,
        method='shingle_per_line' if 'per_line' in dataset_method else 'shingle',
    )
    model.compile(metrics=[
        ShingledCrossentropy(name='mean_xent2', aggregation='mean'),
        ShingledCrossentropy(name='total_xent2', aggregation='sum')
    ])

    log.info(
        "Evaluating on the test data using a 'shingled' dataset that provides the maximum amount "
        "of context for each predicted word. This will take much more time than the first "
        "evaluation."
    )
    results = model.evaluate(test_dataset, verbose=args.keras_verbosity)
    results = {name: result for name, result in zip(model.metrics_names, results)}
    for metric, value in results.items():
        log.info(f"{metric:20s}: {value}")


def make_options():
    cli = argparse.ArgumentParser(fromfile_prefix_chars='@')

    cli.add_argument(
        '--log-level',
        required=False,
        type=commandline.log_level,
        default='info',
        help="Sets the log level."
    )

    tf_args = cli.add_argument_group(
        'TensorFlow',
        "Options to customize core TensorFlow operation."
    )

    tf_args.add_argument(
        '--run-eagerly',
        action='store_true',
        help="Set this flag to run tensorflow functions eagerly."
    )

    tf_args.add_argument(
        '--keras-verbosity',
        type=int,
        default=0,
        help="The verbosity for Keras/TensorFlow."
    )

    io_args = cli.add_argument_group(
        "IO Files",
        "Options that specify the input files needed for training and the output files "
        "where results will be saved."
    )
    io_args.add_argument(
        '--model-config-file',
        required=True,
        type=str,
    )

    io_args.add_argument(
        '--model-weights-file',
        required=True,
        type=str,
    )

    io_args.add_argument(
        '--test-file',
        required=True,
        type=str
    )

    dataset_args = cli.add_argument_group(
        "Dataset",
        "Options for customizing the dataset pipeline."
    )

    dataset_args.add_argument(
        '--batch-size',
        required=False,
        type=int,
        default=128
    )

    dataset_args.add_argument(
        '--exclude-boundaries',
        action='store_true',
        help="Exclude document boundaries (they should usually be set)."
    )

    cli.set_defaults(func=test)

    return cli


if __name__ == '__main__':
    parser = make_options()

    cli_args = parser.parse_args()

    logging.basicConfig(
        level=cli_args.log_level,
        format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
    )

    cli_args.func(cli_args)
