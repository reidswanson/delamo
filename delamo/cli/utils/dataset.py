# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import gzip
import logging

from typing import Tuple, Optional

# 3rd Party Modules
import tensorflow as tf

# Project Modules


log = logging.getLogger(__name__)

AUTOTUNE = tf.data.experimental.AUTOTUNE


def find_max_document_length(filename: str):
    """
    Finds the maximum document length in the given file.
    This just counts the number of space separated tokens per line and
    returns the maximum value.

    :param filename:
    :return:
    """
    open_cmd = gzip.open if filename.endswith('.gz') else open
    max_len = 0
    with open_cmd(filename, 'rt') as fh:
        for line in fh:
            line_len = len(line.split())
            if line_len > max_len:
                max_len = line_len

    return max_len


@tf.function
def random_slices(tensor: tf.Tensor, seq_len: int, random_seed=None):
    t_size = tf.size(tensor)
    seq_len = tf.math.minimum(t_size, seq_len)
    max_idx = tf.math.maximum(t_size - seq_len, 1)

    n_samples = tf.cast(tf.math.ceil(t_size / seq_len), tf.int32)
    tensor = tf.expand_dims(tensor, 0)
    indices = tf.random.uniform((n_samples,), 0, max_idx, tf.int32, seed=random_seed)

    x = tf.zeros([n_samples * seq_len], dtype=tf.int32)
    y = tf.tile(tf.expand_dims(tf.range(seq_len), 0), [n_samples, 1])
    y = tf.reshape(y + tf.expand_dims(indices, 1), [-1])

    idx = tf.transpose(tf.stack([x, y]))

    return tf.reshape(tf.gather_nd(tensor, idx), (-1, seq_len))


def make_dataset(
        filename: str,
        max_len: int,
        word2id: tf.lookup.StaticHashTable,
        batch_size: int,
        seq_len: int,
        include_boundaries: bool = True,
        method: str = 'fixed_sequences',
        shuffle: bool = False,
        buffer_size: int = 50000,
        drop_remainder: bool = False,
        start: str = '<s>',
        end: str = '</s>',
        newline: str = '<br>',
        random_seed: Optional[int] = None
) -> tf.data.Dataset:
    """
    Valid methods:

        **fixed_sequences**
            The entire file is treated as a single document. The document is
            split into equal sized sequences of length ``seq_len`` with the
            possible exception of the last sequence which may be zero padded
            so it has exactly ``seq_len`` items.
        **shingle**
            The entire file is treated as a single document. Every possible
            sequence of length ``seq_len`` is generated. If ``shuffle`` is
            ``True`` then a random subset of :math:`\frac{|D|}{|S|}` sequences
            are returned (where ``|D|`` is the number of tokens in the
            dataset and ``|S|`` is the sequence length).
        **slice**
            The entire file is treated as a single document.
            :math:`\frac{|D|}{|S|}` slices of  length ``seq_len``
            are randomly selected from the document. ``shuffle`` has no
            meaning for this method since the slices are randomly selected
            each time.
        **fixed_sequences_per_line**
            The same as **fixed_sequences** except each line is treated as a
            separate document and sequences never cross line boundaries.
        **shingle_per_line**
            The same as **shingle** except each line is treated as a separate
            document.
            **Currently not implemented**
        **sliced_per_line**
            The same as **slice** except each line is treated as a separate
            document.
        **fixed_sequences_per_line_stateful**
            The same as **fixed_sequences_per_line** except every line is
            padded so that it is split into a fixed integral number of batches,
            which allows stateful RNNs to be trained more easily.

    :param filename:
    :param max_len:
    :param word2id:
    :param batch_size:
    :param seq_len:
    :param include_boundaries:
    :param method:
    :param shuffle:
    :param buffer_size:
    :param drop_remainder:
    :param start:
    :param end:
    :param newline:
    :param random_seed:
    :return:
    """

    # Assume the dataset is compressed if it ends with .gz
    compression = 'GZIP' if filename.endswith('.gz') else ''

    # Add seq_len - 1 start tokens to the beginning of the dataset.
    # ``per_line`` and the ``slice`` method have their own way of adding
    # boundaries to the dataset.
    if 'per_line' not in method and 'slice' not in method and include_boundaries:
        dataset = tf.data.Dataset.from_tensor_slices(
            [' '.join(start for _ in range(seq_len - 1))]
        )
        # Load the dataset from the given file
        file_dataset = tf.data.TextLineDataset(filename, compression_type=compression)
        dataset = dataset.concatenate(file_dataset)
    else:
        # Load the dataset from the given file
        dataset = tf.data.TextLineDataset(filename, compression_type=compression)

    # Strip white spaces from the front and back of each line.
    dataset = dataset.map(tf.strings.strip, num_parallel_calls=AUTOTUNE)

    # The 'per line' methods assume that the data has been preprocessed so that
    # each document is on a single line and all functional tokens (e.g., <s>,
    # </s>, <br>) have been added already.
    # already.
    if 'per_line' not in method:
        dataset = dataset.map(lambda line: line + ' ' + newline, num_parallel_calls=AUTOTUNE)

    # Optionally add start and end tokens to each line.
    if ('per_line' in method or 'slice' in method) and include_boundaries:
        prefix = ' '.join(['<s>' for _ in range(seq_len - 1)])
        dataset = dataset.map(lambda t: prefix + ' ' + t + ' ' + end, num_parallel_calls=AUTOTUNE)

    # Split the lines by whitespace
    dataset = dataset.map(tf.strings.split, num_parallel_calls=AUTOTUNE)

    # Use the vocabulary to look up the numeric id of each word.
    dataset = dataset.map(lambda t: word2id.lookup(t), num_parallel_calls=AUTOTUNE)

    if method == 'fixed_sequences':
        dataset = _fixed_sequences(
            dataset,
            batch_size,
            seq_len,
            drop_remainder,
            shuffle,
            buffer_size,
            random_seed
        )
    elif method == 'shingle':
        dataset = _shingle(dataset, batch_size, seq_len, shuffle, buffer_size, random_seed)
    elif method == 'slice':
        dataset = _slice(dataset, batch_size, seq_len, random_seed)
    elif method == 'fixed_sequences_per_line':
        dataset = _fixed_sequences_per_line(
            dataset,
            batch_size,
            seq_len,
            shuffle,
            buffer_size,
            random_seed
        )
    elif method == 'fixed_sequences_per_line_stateful':
        dataset = _fixed_sequences_per_line_stateful(dataset, batch_size, seq_len, max_len)
    elif method == 'shingle_per_line':
        dataset = _shingle_per_line(
            dataset,
            batch_size,
            seq_len,
            shuffle,
            buffer_size,
            random_seed
        )
    elif method == 'slice_per_line':
        dataset = _slice_per_line(dataset, batch_size, seq_len, random_seed)

    dataset = dataset.prefetch(tf.data.experimental.AUTOTUNE)

    return dataset


def make_train_datasets(
        args: argparse.Namespace,
        word2id: tf.lookup.StaticHashTable
) -> Tuple[tf.data.Dataset, tf.data.Dataset]:
    train_file, valid_file = args.train_file, args.valid_file
    train_method = args.dataset_method
    valid_method = train_method if 'fixed_sequences' in train_method else 'fixed_sequences'

    max_len = max(find_max_document_length(train_file), find_max_document_length(valid_file))

    shared_kw = {
        'max_len': max_len,
        'word2id': word2id,
        'batch_size': args.batch_size,
        'seq_len': args.sequence_length,
        'include_boundaries': not args.exclude_boundaries
    }

    train_data = make_dataset(
        train_file,
        method=train_method,
        shuffle=args.shuffle,
        drop_remainder='rnn' in args.command and args.stateful,
        **shared_kw
    )
    valid_data = make_dataset(
        valid_file,
        method=valid_method,
        shuffle=False,
        drop_remainder='rnn' in args.command and args.stateful,
        **shared_kw
    )

    return train_data, valid_data


# region Utility Methods
# region Dataset Methods
def _fixed_sequences(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        drop_remainder: bool,
        shuffle: bool,
        buffer_size: int,
        random_seed: Optional[int]
) -> tf.data.Dataset:
    # Flatten the dataset
    dataset = dataset.unbatch()

    # Create windows that are 1 longer than the sequence length to account
    # for slicing off the front and back for the y and x values. We also
    # need to shift the window over by 1 less than the window size. This
    # make the last element of the window the first element of the next
    # window.
    # If we did not do this then the last element of the window would be
    # sliced off to make the y data, but would not be included in the
    # next x window.
    dataset = dataset.window(seq_len + 1, seq_len)

    # We also need to apply flat_map to turn the windows into actual
    # sequences.
    dataset = dataset.flat_map(lambda w: w.batch(seq_len + 1))

    # Cache the dataset in memory
    dataset = dataset.cache()

    # Shuffle the sequences if the option is set.
    if shuffle:
        dataset = dataset.shuffle(buffer_size, reshuffle_each_iteration=True, seed=random_seed)

    # Make the x, y pairs
    dataset = dataset.map(lambda t: (t[:-1], t[1:]))

    # Make the batches
    dataset = dataset.padded_batch(
        batch_size,
        (seq_len, seq_len),
        drop_remainder=drop_remainder
    )
    return dataset


def _shingle(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        shuffle: bool,
        buffer_size: int,
        random_seed: Optional[int]
) -> tf.data.Dataset:
    # Flatten the dataset
    dataset = dataset.unbatch()

    # Create a window that is (seq_len + 1) in length starting at every
    # token by shifting only one token over each time.
    dataset = dataset.window(seq_len + 1, 1)

    # Apply the flat_map
    dataset = dataset.flat_map(lambda w: w.batch(seq_len + 1))

    # Shuffle all the sequences and select a subset of them that is the same
    # size as if the original dataset had been split into normal sequences.
    if shuffle:
        # If we are shuffling then we should cache the dataset here
        dataset = dataset.cache()
        dataset = dataset.shuffle(buffer_size, reshuffle_each_iteration=True, seed=random_seed)
        dataset = dataset.shard(seq_len, 0)

    # Make the x, y pairs
    dataset = dataset.map(lambda t: (t[:-1], t[1:]))

    # Make the batches
    dataset = dataset.padded_batch(batch_size, (seq_len, seq_len))

    if not shuffle:
        # If we aren't shuffling we can cache after all processing.
        dataset = dataset.cache()

    return dataset


def _slice(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        random_seed: Optional[int]
) -> tf.data.Dataset:
    # This expects the data to be preprocessed so that all the tokens are on a single line.
    dataset = dataset.cache()

    # Take random slices from the dataset to use as sequences.
    # The number of sequences (n_samples) is (|t| / (seq_len + 1)).
    # This will return a dataset whose elements are of shape: (n_samples, seq_len+1)
    dataset = dataset.map(lambda t: random_slices(t, seq_len + 1, random_seed=random_seed))

    # Unbatch the outer dimension so each element is of shape: (seq_len+1,)
    dataset = dataset.unbatch()

    # Make the x, y pairs
    dataset = dataset.map(lambda t: (t[:-1], t[1:]))

    # Make the batches
    dataset = dataset.batch(batch_size)

    return dataset


def _fixed_sequences_per_line(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        shuffle: bool,
        buffer_size: int,
        random_seed: Optional[int]
) -> tf.data.Dataset:
    # This is a bit trickier
    # Get the x, y pairs
    dataset = dataset.map(lambda t: (t[:-1], t[1:]))

    def resize(t):
        t_size = tf.size(t)

        if tf.math.mod(t_size, seq_len) == 0:
            return t

        n = tf.cast(tf.math.ceil(t_size / seq_len), tf.int32)
        pad_len = seq_len * n - t_size
        return tf.pad(t, [[0, pad_len]])

    dataset = dataset.map(lambda x, y: (resize(x), resize(y)))

    # Reshape to create the sequences
    dataset = dataset.map(lambda x, y: (tf.reshape(x, (-1, seq_len)), tf.reshape(y, (-1, seq_len))))

    # Unbatch to remove the outer dimension
    dataset = dataset.unbatch()

    # If we don't want batches suitable for stateful training, then
    # we can remove any empty rows.
    dataset = dataset.filter(lambda x, y: tf.math.reduce_sum(x) > 0)
    dataset = dataset.cache()

    if shuffle:
        # Only allow shuffling if stateful is not enabled
        dataset = dataset.shuffle(
            buffer_size,
            reshuffle_each_iteration=True,
            seed=random_seed
        )

    # We don't need padded_batch here because everything is already padded
    dataset = dataset.batch(batch_size)

    return dataset


def _fixed_sequences_per_line_stateful(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        max_len: int
) -> tf.data.Dataset:
    # This is a bit trickier
    # Get the x, y pairs
    n_docs = 0
    for _ in dataset:
        n_docs += 1

    # There needs to be the exact same number of sequences in each batch.
    # If there aren't, append dummy/padded sequences to the end of the
    # dataset until the number of documents is evenly divisible by the
    # batch size.
    if n_docs < batch_size:
        pad_docs = batch_size - n_docs
    elif n_docs % batch_size == 0:
        pad_docs = 0
    else:
        pad_docs = batch_size - (n_docs % batch_size)

    if pad_docs > 0:
        pad_dataset = tf.data.Dataset.from_tensor_slices([[0] for _ in range(pad_docs)])
        dataset = dataset.concatenate(pad_dataset)

    dataset = dataset.map(lambda t: (t[:-1], t[1:]))

    # The batch size can't be bigger than the number of documents for
    # stateful training.
    # bsz = tf.cast(tf.math.minimum(batch_size, n_docs), tf.int64)
    bsz = batch_size

    # The length of each document must be at least (max_len - 1) but also
    # must be evenly divisible by the sequence length.
    # How many elements we need to add to make the maximum document length
    # (minus one) to make it evenly divisible by the sequence length.
    pad_len = seq_len - ((max_len - 1) % seq_len)

    # The length every document in the batch should have to make it evenly
    # divisible by the sequence length times the batch_size.
    doc_len = (max_len - 1) + pad_len

    # Group documents in batches
    dataset = dataset.padded_batch(bsz, (doc_len, doc_len))

    # Reshape each batch so that it is one long sequence.
    # In the next steps we will interleave them so they have the
    # appropriate composition for stateful training.
    dataset = dataset.map(lambda x, y: (tf.reshape(x, [-1]), tf.reshape(y, [-1])))

    # The following uses dynamic_stitch which allows you to rearrange a
    # tensor by specifying a new set of indices for each element.
    # The ultimate goal is to have a sequence arranged so that the first
    # seq_len elements are from sequence 1, the second seq_len elements are
    # from sequence 2, and so on until all sequences are in this order.
    # Then, if we reshape this tensor to have the shape (bsz, seq_len)
    # the sequences from each document will be interleaved properly for
    # use with stateful RNN training.

    # First create a base template for creating the indices. Each consecutive
    # sequence of identical values represents a sequence from one of the
    # documents. These will eventually be the final indices of each value
    # in the new tensor. Given a batch size (bsz) of 3 and a seq_len of 4
    # the initial template should look something like:
    # [0, 0, 0, 0, 12, 12, 12, 12, 24, 24, 24, 24, ...]
    tmp1 = tf.repeat(tf.range(0, doc_len * bsz, bsz * seq_len, dtype=tf.int32), seq_len)

    # If we think about tmp1 as the indices of only the first document
    # then its clear we need each consecutive sequence to be incremented
    # by 1. tmp2 creates a tensor that we can add to tmp1 to accomplish this.
    # It should look something like:
    # [0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, ...]
    tmp2 = tf.tile(tf.range(seq_len, dtype=tf.int32), [doc_len // seq_len])
    tmp3 = tmp1 + tmp2

    # tmp3 works for the first sequence, but we need to arrange all the
    # documents in the batch. We can do this with some addition and
    # broadcasting.
    # [0,
    #  4, + [0, 1, 2, 3, 12, 13, 14, 15, 24, 25, 26, 27, ...]
    #  8]
    # =
    # [[ 0,  1,  2,  3, 12, 13, 14, 15, 24, 25, 26, 27, ...],
    #  [ 4,  5,  6,  7, 16, 17, 18, 19, 28, 29, 30, 31, ...],
    #  [ 8,  9, 10, 11, 20, 21, 22, 23, 32, 33, 34, 35, ...]]
    tmp4 = tf.range(0, bsz * seq_len, seq_len, dtype=tf.int32)

    # Now, if we flatten tmp4 it will contain the indices of each value
    # in the new array.
    indices = tf.reshape(tmp3 + tf.reshape(tmp4, (-1, 1)), [-1])

    # Apply dynamic_stitch to both the x and y values using the indices.
    dataset = dataset.map(
        lambda x, y: (tf.dynamic_stitch([indices], [x]), tf.dynamic_stitch([indices], [y]))
    )

    # Reshape so that each row has seq_len columns (and ``max_len * bsz / seq_len`` columns).
    dataset = dataset.map(
        lambda x, y: (tf.reshape(x, [-1, seq_len]), tf.reshape(y, [-1, seq_len]))
    )

    # Unbatch so that each sequence is an element in the dataset.
    dataset = dataset.unbatch()

    # Finally rebatch them to the appropriate size. (Note, there seems to
    # be an off by 1 error that produces an empty batch at the end).
    dataset = dataset.batch(bsz)

    # Compensate for the off by 1 error.
    # Actually not sure if this is ok to do, since all batches must have
    # the same number of sequences in them.
    # dataset = dataset.filter(lambda x, y: tf.math.reduce_sum(x) > 0)

    dataset = dataset.cache()

    return dataset


def _shingle_per_line(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        shuffle: bool,
        buffer_size: int,
        random_seed: Optional[int]
) -> tf.data.Dataset:
    def shingle(t, shingle_len):
        n_shingles = tf.size(t) - shingle_len + 1
        indices = tf.reshape(tf.tile(tf.range(shingle_len), [n_shingles]), (-1, shingle_len))
        indices = indices + tf.reshape(tf.range(n_shingles), (-1, 1))
        indices = tf.reshape(indices, (-1, 1))

        return tf.reshape(tf.gather_nd(t, indices), (-1, shingle_len))

    dataset = dataset.map(lambda t: shingle(t, seq_len + 1))
    dataset = dataset.unbatch()

    if shuffle:
        # If we are shuffling then we should cache the dataset here
        dataset = dataset.cache()
        dataset = dataset.shuffle(buffer_size, reshuffle_each_iteration=True, seed=random_seed)
        dataset = dataset.shard(seq_len, 0)

    dataset = dataset.map(lambda t: (t[:-1], t[1:]))
    dataset = dataset.batch(batch_size)

    if not shuffle:
        dataset = dataset.cache()

    return dataset


def _slice_per_line(
        dataset: tf.data.Dataset,
        batch_size: int,
        seq_len: int,
        random_seed: Optional[int]
) -> tf.data.Dataset:
    def resize(t):
        return tf.pad(t, [[0, seq_len - tf.size(t)]])

    dataset = dataset.cache()
    dataset = dataset.map(lambda t: random_slices(t, seq_len + 1, random_seed=random_seed))
    dataset = dataset.unbatch()
    dataset = dataset.map(lambda t: (t[:-1], t[1:]))
    dataset = dataset.filter(lambda t, _: tf.size(t) > 0)
    dataset = dataset.map(lambda x, y: (resize(x), resize(y)))

    dataset = dataset.batch(batch_size)

    return dataset
# endregion Dataset Methods
# endregion Utility Methods
