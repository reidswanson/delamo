# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import logging

# 3rd Party Modules

# Project Modules


LOG_LEVELS = {
    'debug': logging.DEBUG,
    'info': logging.INFO,
    'warning': logging.WARNING,
    'error': logging.ERROR,
    'critical': logging.CRITICAL
}


def log_level(name: str):
    """
    Convert the name of the log level to its value.

    :param name: The name of the log level.
    :return: The value of the log level.
    :raises: ArgumentTypeError if the name is not known.
    """
    level = LOG_LEVELS.get(name.lower())

    if level:
        return level

    raise argparse.ArgumentTypeError(f"Unknown logging level: {name}")
