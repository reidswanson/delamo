# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse

# 3rd Party Modules
import tensorflow as tf

# Project Modules
from delamo.models.core import LanguageModel


def main(args: argparse.Namespace):
    model = LanguageModel.load_from_files(args.model_config, args.model_weights)

    for _ in range(args.n):
        print(model.generate(args.text_seed, 40, random_seed=args.random_seed))


def make_options():
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')

    parser.add_argument('--model-config', required=True)
    parser.add_argument('--model-weights', required=True)
    parser.add_argument('--n', required=False, type=int, default=1)
    parser.add_argument('--text-seed', required=False, type=str, default='<s>')
    parser.add_argument('--random-seed', required=False, type=int)

    return parser


if __name__ == '__main__':
    cli = make_options()
    cli.set_defaults(func=main)

    cli_args = cli.parse_args()

    cli_args.func(cli_args)
