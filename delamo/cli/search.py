# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import logging
import os
import pathlib
import pickle

# 3rd Party Modules
from typing import Callable

import optuna
import tensorflow as tf

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
import delamo.cli.options.callbacks as opts_callbacks
import delamo.cli.options.hyopt as opts_hyopt
import delamo.cli.utils.commandline as commandline

from delamo.hyopt.tfrnn import RnnObjective
from delamo.hyopt.transformer import DecoderOnlyObjective
from delamo.hyopt.xfmr import XfmrObjective

log = logging.getLogger('delamo_search')
logging.getLogger('urllib3').setLevel(logging.WARNING)


for device in tf.config.list_physical_devices('GPU'):
    tf.config.experimental.set_memory_growth(device, True)


def main(args: argparse.Namespace):
    logging.basicConfig(
        level=args.log_level,
        format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
    )

    policy = mixed_precision.Policy(args.dtype_policy)
    mixed_precision.set_policy(policy)

    # By default the program assumes you are starting a new study from scratch.
    # However, you can't simply overwrite the existing sqlite db, so delete
    # it here if necessary.
    if not args.load_if_exists and os.path.isfile(args.study_file):
        log.info(f"Removing existing study file and starting from scratch.")
        os.remove(args.study_file)

    sampler = optuna.samplers.TPESampler(
        n_startup_trials=args.n_startup_trials,
        seed=args.optuna_random_seed
    )

    study = optuna.create_study(
        study_name=args.study_file,
        sampler=sampler,
        direction='minimize',
        storage=f'sqlite:///{args.study_file}',
        load_if_exists=args.load_if_exists
    )

    if args.model_name in ('rnn', 'gru', 'lstm'):
        objective = RnnObjective(
            cell_type=args.model_name,
            train_data_file=args.train_file,
            valid_data_file=args.valid_file,
            test_data_file=args.test_file,
            dataset_method=args.dataset_method,
            max_epochs=args.max_epochs,
            early_stopping_parameters=opts_callbacks.early_stopping_kwargs(args),
            plateau_parameters=opts_callbacks.reduce_lr_on_plateau_kwargs(args)
        )
    elif args.model_name == 'transformer':
        objective = DecoderOnlyObjective(
            train_data_file=args.train_file,
            valid_data_file=args.valid_file,
            test_data_file=args.test_file,
            dataset_method=args.dataset_method,
            max_epochs=args.max_epochs,
            early_stopping_parameters=opts_callbacks.early_stopping_kwargs(args),
            plateau_parameters=opts_callbacks.reduce_lr_on_plateau_kwargs(args)
        )
    elif args.model_name == 'xfmr':
        objective = XfmrObjective(
            train_data_file=args.train_file,
            valid_data_file=args.valid_file,
            test_data_file=args.test_file,
            dataset_method=args.dataset_method,
            max_epochs=args.max_epochs,
            early_stopping_parameters=opts_callbacks.early_stopping_kwargs(args),
            plateau_parameters=opts_callbacks.reduce_lr_on_plateau_kwargs(args)
        )
    else:
        raise ValueError(f"Unsupported model: {args.model_name}")

    study.optimize(objective, n_trials=args.n_trials)

    if args.trials_file:
        with open(args.trials_file, 'wb') as fh:
            pickle.dump(study.trials, fh)

    df = study.trials_dataframe(attrs=('number', 'value', 'params'))
    df.sort_values(['value'], ascending=True, inplace=True)

    # Log the results of the search as a human readable table.
    log.info(f"Results:\n{df.to_string()}")

    # Plot some reports
    plot_dir = args.plot_directory
    if plot_dir:
        # Make the directory if it does not exist already
        log.debug(f"Creating plot directory at: {plot_dir}")
        pathlib.Path(plot_dir).mkdir(parents=True, exist_ok=True)
        plot(optuna.visualization.plot_optimization_history, study, plot_dir, 'history.pdf')
        plot(optuna.visualization.plot_slice, study, plot_dir, 'slice.pdf')
        plot(optuna.visualization.plot_edf, study, plot_dir, 'edf.pdf')


def plot(plot_fn: Callable, study: optuna.Study, out_dir: str, filename: str):
    path = os.path.join(out_dir, filename)
    try:
        plt = plot_fn(study)
        plt.write_image(path)
    except ArithmeticError as e:
        log.warning(f"Unable to plot file '{filename}': {e}")
    except ValueError as e:
        log.error(f"Something else went wrong: {e}")


def make_options():
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')

    parser.add_argument(
        '--log-level',
        type=commandline.log_level,
        default='debug'
    )

    tf_args = parser.add_argument_group(
        'TensorFlow',
        "Options to customize core TensorFlow operation."
    )

    tf_args.add_argument(
        '--run-eagerly',
        action='store_true',
        help="Set this flag to run tensorflow functions eagerly."
    )

    tf_args.add_argument(
        '--dtype-policy',
        type=str,
        required=False,
        default='mixed_float16',
        choices=['float16', 'float32', 'float64', 'mixed_float16', 'mixed_bfloat16'],
        help=(
            "Use this option to set the dtype policy (e.g., mixed precision for better "
            "performance on newer GPUs)."
        )
    )

    io_args = parser.add_argument_group(
        "IO Files",
        "Options that specify the input files needed for training and the output files "
        "where results will be saved."
    )
    
    io_args.add_argument(
        '--train-file',
        required=True,
        type=str,
        help=(
            "The training data file. The document should be preprocessed such that: 1) words are "
            "tokenized. 2) all OOV words are replaced by the OOV marker ``<unk>``. 3) each "
            "sentence (or other unit of discourse) should be on a separate line."
        )
    )

    io_args.add_argument(
        '--valid-file',
        required=True,
        type=str,
        help=(
            "The validation data file. The document should be preprocessed such that: 1) words are "
            "tokenized. 2) all OOV words are replaced by the OOV marker ``<unk>``. 3) each "
            "sentence (or other unit of discourse) should be on a separate line."
        )
    )

    io_args.add_argument(
        '--test-file',
        required=True,
        type=str,
        help=(
            "The test data file. The document should be preprocessed such that: 1) words are "
            "tokenized. 2) all OOV words are replaced by the OOV marker ``<unk>``. 3) each "
            "sentence (or other unit of discourse) should be on a separate line."
        )
    )

    dataset_args = parser.add_argument_group(
        "Dataset",
        "Options for customizing the dataset pipeline."
    )

    dataset_args.add_argument(
        '--dataset-method',
        required=False,
        type=str,
        default='fixed_sequences',
        choices=[
            'fixed_sequences', 'shingle', 'slice',
            'fixed_sequences_per_line', 'shingle_per_line', 'slice_per_line',
            'fixed_sequences_per_line_stateful'
        ],
        help="The method used to format the dataset."
    )

    dataset_args.add_argument(
        '--exclude-boundaries',
        action='store_true'
    )

    dataset_args.add_argument(
        '--max-epochs',
        required=False,
        type=int,
        default=100
    )

    search_args = parser.add_argument_group(
        "Searching",
        "General options for customizing the search."
    )

    search_args.add_argument(
        '--model-name',
        required=True,
        type=str,
        choices=['rnn', 'lstm', 'gru', 'transformer', 'xfmr']
    )

    # Callback options
    parser = opts_callbacks.make_options(parser)

    # Optuna options
    parser = opts_hyopt.make_options(parser)

    parser.set_defaults(func=main)

    return parser


if __name__ == '__main__':
    cli = make_options()

    cli_args = cli.parse_args()

    cli_args.func(cli_args)
