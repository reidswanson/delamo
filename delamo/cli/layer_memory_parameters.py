# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import json

# 3rd Party Modules
import pandas as pd
import plotnine as p9

from sklearn.linear_model import LinearRegression

# Project Modules


def read_data(filename: str) -> pd.DataFrame:
    with open(filename, 'rt') as fh:
        data = [json.loads(line) for line in fh if line]

    # Create the dataframe and fill missing entries (i.e. NaN) with zeros
    df = pd.DataFrame(data).fillna(0)

    # Drop columns that only contain zeros
    # noinspection PyUnresolvedReferences
    df = df.loc[:, (df != 0).any(axis=0)]

    return df


# noinspection PyTypeChecker
def plot_memory_usage(data: pd.DataFrame):
    df = data[['tf_peak_memory', 'nv_peak_memory']].copy() / 1e6

    p = p9.ggplot(df)
    p += p9.geom_point(p9.aes(x='tf_peak_memory', y='nv_peak_memory'))

    print(p)


def fit_regression(df: pd.DataFrame):
    dtype_size = df['dtype_size'][0]

    x = df.copy()
    x = x.drop(df.filter(regex=r"peak_memory|dtype|Layer|Batch", axis=1).columns, axis=1) * dtype_size
    y = df['nv_peak_memory'].copy()


    df_batch = df[df['BatchNormalization'] > 0].copy()
    df_layer = df[df['LayerNormalization'] > 0].copy()
    y_batch = df_batch['nv_peak_memory']
    y_layer = df_layer['nv_peak_memory']

    x_batch = df_batch.drop(df.filter(regex=r"peak_memory|dtype|Layer", axis=1).columns, axis=1) * dtype_size
    x_layer = df_layer.drop(df.filter(regex=r"peak_memory|dtype|Batchr", axis=1).columns, axis=1) * dtype_size

    print(x_batch.to_string(max_rows=10))
    print(y_batch.to_string(max_rows=10))

    reg = LinearRegression(positive=True).fit(x, y)
    reg_batch = LinearRegression(positive=True).fit(x_batch, y_batch)
    reg_layer = LinearRegression(positive=True).fit(x_layer, y_layer)

    print(reg.score(x, y))
    print(reg_batch.score(x_batch, y_batch))
    print(reg_layer.score(x_layer, y_layer))

    print(reg.coef_)
    print(reg.intercept_)

    print(reg_batch.coef_)
    print(reg_batch.intercept_)

    print(reg_layer.coef_)
    print(reg_layer.intercept_)


def main(args: argparse.Namespace):
    data = read_data(args.input_file)

    # plot_memory_usage(data)

    layer_parameters = fit_regression(data)




def make_options():
    parser = argparse.ArgumentParser()

    parser.add_argument('--input-file', required=True)

    return parser


if __name__ == '__main__':
    cli = make_options()

    cli_args = cli.parse_args()

    main(cli_args)