# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import logging
import math
import os
import pathlib
import pickle

# 3rd Party Modules
from typing import List

import tensorflow as tf

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
import delamo.cli.options.callbacks as opts_callbacks
import delamo.cli.options.dernn as opts_dernn
import delamo.cli.options.gpt2 as opts_gpt2
import delamo.cli.options.optimizer as opts_optimizer
import delamo.cli.options.transformer as opts_transformer
import delamo.cli.options.tfrnn as opts_tfrnn
import delamo.cli.options.xfmr as opts_xfmr

import delamo.cli.utils.commandline as commandline
import delamo.models.dernn as dernn
import delamo.models.tfrnn as tfrnn

from delamo.callbacks import ResetStatesCallback
from delamo.cli.utils.dataset import make_train_datasets, find_max_document_length, make_dataset
from delamo.metrics import ShingledSparseCategoricalCrossentropy as ShingledCrossentropy
from delamo.models.gpt2 import Gpt2
from delamo.models.transformer import DecoderOnlyLanguageModel
from delamo.models.xfmr import Xfmr
from delamo.utils.vocab import py_vocab_from_file, tf_vocab_from_dict
from delamo.utils.word2vec import WordVectors

log = logging.getLogger('delamo_train')

for device in tf.config.list_physical_devices('GPU'):
    tf.config.experimental.set_memory_growth(device, True)


def make_default_callbacks(args: argparse.Namespace):
    callbacks = [
        opts_callbacks.make_model_checkpoint_from_args(args),
        opts_callbacks.make_early_stopping_from_args(args),
        opts_callbacks.make_reduce_lr_on_plateau_from_args(args)
    ]

    if args.tensorboard_directory:
        opts_callbacks.make_tensorboard_dir_from_args(args)

        callbacks += [opts_callbacks.make_tensorboard_from_args(args)]

    return callbacks


def transformer(args: argparse.Namespace):
    py_vocab = py_vocab_from_file(args.train_file)
    model = DecoderOnlyLanguageModel.prepare_model(args, py_vocab)

    callbacks = make_default_callbacks(args)

    train(args, model, callbacks)


def gpt2(args: argparse.Namespace):
    py_vocab = py_vocab_from_file(args.train_file)
    model = Gpt2.prepare_model(args, py_vocab)

    callbacks = make_default_callbacks(args)

    train(args, model, callbacks)


def xfmr(args: argparse.Namespace):
    py_vocab = py_vocab_from_file(args.train_file)
    model = Xfmr.prepare_model(args, py_vocab)

    callbacks = make_default_callbacks(args)

    train(args, model, callbacks)


def tf_rnn(args: argparse.Namespace):
    if args.stateful and (args.shuffle or 'fixed_sequences' not in args.dataset_method):
        raise ValueError(
            f"You cannot have shuffle or shingle set if you are using a stateful RNN."
        )

    py_vocab = py_vocab_from_file(args.train_file)

    if args.word_vectors_file:
        word_vectors = WordVectors.load(args.word_vectors_file, set(py_vocab.keys()))
        embedding_matrix = word_vectors.get_embedding_matrix(py_vocab)
    else:
        embedding_matrix = None

    model = tfrnn.RnnLanguageModel.prepare_model(args, py_vocab, embedding_matrix=embedding_matrix)
    max_len = max(
        find_max_document_length(args.train_file),
        find_max_document_length(args.valid_file)
    )

    callbacks = make_default_callbacks(args)

    if args.stateful:
        n_sequences = math.ceil(max_len / args.sequence_length)
        callbacks += [ResetStatesCallback(n_sequences)]

    train(args, model, callbacks)


def de_rnn(args: argparse.Namespace):
    py_vocab = py_vocab_from_file(args.train_file)
    model = dernn.RnnLanguageModel.prepare_model(args, py_vocab)

    callbacks = make_default_callbacks(args)

    train(args, model, callbacks)


def train(
        args: argparse.Namespace,
        model: tf.keras.models.Model,
        callbacks: List[tf.keras.callbacks.Callback]
):
    tf.config.run_functions_eagerly(args.run_eagerly)

    policy = mixed_precision.Policy(args.dtype_policy)
    mixed_precision.set_policy(policy)

    log.debug(
        f"dtype policy: {policy} "
        f"with {policy.compute_dtype} compute dtype "
        f"and {policy.variable_dtype} variable dtype"
    )

    tf_vocab = tf_vocab_from_dict(model.vocabulary)
    train_data, valid_data = make_train_datasets(args, tf_vocab)

    if args.tensorboard_directory:
        # Save the labels so that the tensorboard embedding projector can
        # use them.
        with open(os.path.join(args.tensorboard_directory, 'metadata.tsv'), 'w') as fh:
            fh.write("Word\tId\n")
            for word, ident in sorted(model.vocabulary.items(), key=lambda i: i[1]):
                fh.write(f"{word or '<empty>'}\t{ident}\n")

    model.fit(
        train_data,
        epochs=args.max_epochs,
        validation_data=valid_data,
        callbacks=callbacks
    )

    log.info(f"model parameters: {model.count_params()}")

    # Evaluate the model using the same metrics used to train.
    # Make sure the best weights are loaded
    model.load_weights(args.checkpoint_file)
    model.evaluate(valid_data)

    # Evaluate the model by making sure that every predicted word is evaluated
    # with a full seq_len (minus 1) worth of context (using a shingled dataset).
    train_file, valid_file = args.train_file, args.valid_file
    max_len = max(find_max_document_length(train_file), find_max_document_length(valid_file))
    valid_data = make_dataset(
        args.valid_file,
        max_len=max_len,
        word2id=tf_vocab,
        batch_size=args.batch_size,
        seq_len=args.sequence_length,
        include_boundaries=True,
        method='shingle_per_line' if 'per_line' in args.dataset_method else 'shingle',
    )
    model.compile(metrics=[ShingledCrossentropy(name='xent2')])
    model.evaluate(valid_data)

    # It seems (practically) impossible to save the model config and the weights
    # together. So, save them separately.
    output_dir = pathlib.Path(os.path.dirname(args.model_config_file))
    output_dir.mkdir(parents=True, exist_ok=True)

    with open(args.model_config_file, 'wb') as fh:
        pickle.dump(model.get_config(), fh)

    model.save_weights(args.model_weights_file)


def make_options():
    cli = argparse.ArgumentParser(fromfile_prefix_chars='@')
    subcli = cli.add_subparsers(title='commands', dest='command', help="command help")

    # region Global Options
    cli.add_argument(
        '--log-level',
        required=False,
        type=commandline.log_level,
        default='info',
        help="Sets the log level."
    )

    # region TensorFlow Options
    tf_args = cli.add_argument_group(
        'TensorFlow',
        "Options to customize core TensorFlow operation."
    )
    tf_args.add_argument(
        '--run-eagerly',
        action='store_true',
        help="Set this flag to run tensorflow functions eagerly."
    )

    tf_args.add_argument(
        '--dtype-policy',
        type=str,
        required=False,
        default='mixed_float16',
        choices=['float16', 'float32', 'float64', 'mixed_float16', 'mixed_bfloat16'],
        help=(
            "Use this option to set the dtype policy (e.g., mixed precision for better "
            "performance on newer GPUs)."
        )
    )
    # endregion TensorFlow Options

    # region Input/Output File Options
    io_args = cli.add_argument_group(
        "IO Files",
        "Options that specify the input files needed for training and the output files "
        "where results will be saved."
    )
    io_args.add_argument(
        '--train-file',
        required=True,
        type=str,
        help=(
            "The training data file. The document should be preprocessed such that: 1) words are "
            "tokenized. 2) all OOV words are replaced by the OOV marker ``<unk>``. 3) each "
            "sentence (or other unit of discourse) should be on a separate line."
        )
    )

    io_args.add_argument(
        '--valid-file',
        required=True,
        type=str,
        help=(
            "The validation data file. The document should be preprocessed such that: 1) words are "
            "tokenized. 2) all OOV words are replaced by the OOV marker ``<unk>``. 3) each "
            "sentence (or other unit of discourse) should be on a separate line."
        )
    )

    io_args.add_argument(
        '--word-vectors-file',
        required=False,
        type=str,
        help=(
            "If the path to the specified file already exists the program will assume it is a set "
            "of pretrained word vectors in the Google format and use them instead of training "
            "a custom embedding. If a path is given and it does not already exist, the trained "
            "embedding will be saved here in the Google format. If the option is not specified " 
            "(the default), a custom embedding layer will be used, but it will not be saved "
            "separately from the model."
        )
    )

    io_args.add_argument(
        '--model-config-file',
        required=True,
        type=str,
        help="The file where the model configuration will be saved."
    )

    io_args.add_argument(
        '--model-weights-file',
        required=True,
        type=str,
        help="The file where the model weights will be saved."
    )
    # endregion Input/Output File Options

    # region Dataset Options
    dataset_args = cli.add_argument_group(
        "Dataset",
        "Options for customizing the dataset pipeline."
    )
    
    dataset_args.add_argument(
        '--dataset-method',
        required=False,
        type=str,
        default='fixed_sequences',
        choices=[
            'fixed_sequences', 'shingle', 'slice',
            'fixed_sequences_per_line', 'shingle_per_line', 'slice_per_line',
            'fixed_sequences_per_line_stateful'
        ],
        help="The method used to format the dataset."
    )

    dataset_args.add_argument(
        '--batch-size',
        required=False,
        type=int,
        default=64
    )

    dataset_args.add_argument(
        '--sequence-length',
        required=False,
        type=int,
        default=50
    )

    dataset_args.add_argument(
        '--shuffle',
        required=False,
        action='store_true',
        help="When set, the samples will be shuffled after every epoch."
    )

    dataset_args.add_argument(
        '--exclude-boundaries',
        action='store_true',
        help=(
            "By default special token boundaries (<s> and </s>) are added to the beginning and "
            "and end of every line of a file. Set this flag to disable this behavior."
        )
    )
    # endregion Dataset Options

    # region Training Options
    train_args = cli.add_argument_group(
        "Training",
        "Options for customizing the general training processing"
    )
    
    train_args.add_argument(
        '--max-epochs',
        required=False,
        type=int,
        default=200
    )

    train_args.add_argument(
        '--n-softmax-samples',
        required=False,
        type=int,
        default=0,
        help=(
            "If greater than 0 approximate the crossentropy using the sampled softmax loss. "
            "The larger the number of samples the more accurate the approximation will be, "
            "but will take longer to train."
        )
    )
    # endregion Training Options
    # endregion

    # Optimzer Options
    cli = opts_optimizer.make_options(cli)

    # Callback Options
    cli = opts_callbacks.make_options(cli)

    # Transformer
    transformer_cli = opts_transformer.make_options(subcli)
    transformer_cli.set_defaults(func=transformer)

    # GPT2
    gpt2_cli = opts_gpt2.make_options(subcli)
    gpt2_cli.set_defaults(func=gpt2)

    # TF RNN
    tfrnn_cli = opts_tfrnn.make_options(subcli)
    tfrnn_cli.set_defaults(func=tf_rnn)

    # DE RNN
    dernn_cli = opts_dernn.make_options(subcli)
    dernn_cli.set_defaults(func=de_rnn)

    # XFMR
    xfmr_cli = opts_xfmr.make_options(subcli)
    xfmr_cli.set_defaults(func=xfmr)

    return cli


if __name__ == '__main__':
    parser = make_options()

    cli_args = parser.parse_args()

    logging.basicConfig(
        level=cli_args.log_level,
        format='%(asctime)-15s [%(name)s]:%(lineno)d %(levelname)s %(message)s'
    )

    cli_args.func(cli_args)
