# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import logging
import argparse
import threading

# 3rd Party Modules
import numpy as np
import tensorflow as tf

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
from delamo.models.xfmr import Xfmr, XfmrConfig
from delamo.utils.math import factors
from delamo.utils.modeling import count_parameters


for device in tf.config.list_physical_devices('GPU'):
    tf.config.experimental.set_memory_growth(device, True)


logging.basicConfig(format='%(asctime)-15s %(message)s', level=logging.DEBUG)

log = logging.getLogger('memory_usage')


class NvidiaMemoryMonitor(threading.Thread):
    def __init__(self):
        super().__init__()

        self.tf_peak_memory = 0
        self.nv_peak_memory = 0
        self.stop = False

    def run(self):
        import nvidia_smi
        import time

        nvidia_smi.nvmlInit()

        while True:
            tf_mem_usage = tf.config.experimental.get_memory_usage('GPU:0')
            if tf_mem_usage > self.tf_peak_memory:
                self.tf_peak_memory = tf_mem_usage

            handle = nvidia_smi.nvmlDeviceGetHandleByIndex(0)
            info = nvidia_smi.nvmlDeviceGetMemoryInfo(handle)
            nv_mem_usage = info.used

            if nv_mem_usage > self.nv_peak_memory:
                self.nv_peak_memory = nv_mem_usage

            time.sleep(0.0001)

            if self.stop:
                break


def main(args):
    policy_name = mixed_precision.global_policy().name
    log.info(f"Policy name: {policy_name}")
    dtype_size = 2 if '16' in policy_name else 4 if '32' in policy_name else 8
    vocab_size = 20000
    unit_choices = list(range(32, 512, 8))

    rng = np.random.RandomState(seed=args.random_seed)

    sequence_length = rng.randint(50, 300)
    n_embedding_units = rng.choice(unit_choices)
    n_attention_state_units = rng.choice(unit_choices)
    n_feed_forward_units = rng.choice(unit_choices)
    n_heads = rng.randint(1, 18)
    head_split_method = rng.choice(['reshape', 'convolution', 'sample'])

    if head_split_method == 'reshape':
        attention_state_factors = factors(n_attention_state_units, *(1, 18))

        tmp_heads = 1
        for factor in attention_state_factors:
            if factor <= n_heads and factor <= 18:
                tmp_heads = factor
        n_heads = tmp_heads

    parameters = {
        'sequence_length': sequence_length,
        'dataset_method': 'slice_per_line',
        'optimizer_params': {
            'name': 'adam',
            'args': {
                'learning_rate': 1e-7,
                'beta_1': 0.99,
                'beta_2': 0.999,
                'epsilon': 1e-9,
                'amsgrad': False,
                'clipnorm': 1
            }
        },
        'config': XfmrConfig(
            dataset_method='slice_per_line',
            sequence_length=sequence_length,
            n_decoder_layers=rng.randint(1, 24),
            n_feed_forward_layers=rng.randint(1, 8),
            n_embedding_units=n_embedding_units,
            n_attention_state_units=n_attention_state_units,
            n_feed_forward_units=n_feed_forward_units,
            embedding_dropout_rate=rng.uniform(),
            attention_dropout_rate=rng.uniform(),
            residual_dropout_rate=rng.uniform(),
            decoder_dropout_rate=rng.uniform(),
            layer_norm_epsilon=1e-9,
            n_heads=n_heads,
            attention_activation_function='softmax',
            activation_function='gelu',
            head_split_method=head_split_method,
            head_sample_size=rng.randint(8, n_attention_state_units),
            normalization_method=rng.choice(['layer', 'batch', 'none']),
            output_predictions_per_layer=False,
            loss_weight_n=1,
            loss_weight_lambda=1,
            loss_weight_normalized=True
        )

    }

    model = Xfmr(
        vocabulary={f"{i}": i for i in range(vocab_size)},
        config=parameters['config']
    )
    model = model.compile_model()
    dummy_data = tf.random.uniform((1, sequence_length), maxval=vocab_size, dtype=tf.int32)

    mem_monitor = NvidiaMemoryMonitor()
    mem_monitor.start()
    for i in range(10):
        model.train_step((dummy_data, dummy_data))
    mem_monitor.stop = True
    mem_monitor.join()

    param_count = count_parameters(model)
    param_count['dtype_size'] = dtype_size
    param_count['tf_peak_memory'] = mem_monitor.tf_peak_memory
    param_count['nv_peak_memory'] = mem_monitor.nv_peak_memory

    print({k: v for k, v in param_count.items()})


def make_options():
    cli = argparse.ArgumentParser()

    cli.add_argument('--random-seed', required=False, type=int)

    return cli


if __name__ == '__main__':
    opts = make_options()
    args = opts.parse_args()

    main(args)
