# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules

# 3rd Party Modules

# Project Modules


def make_options(subparser):
    cli = subparser.add_parser(
        'transformer',
        help="Train a transformer model."
    )

    cli.add_argument(
        '--n-model-units',
        required=False,
        type=int,
        default=256
    )

    cli.add_argument(
        '--n-layers',
        required=False,
        type=int,
        default=4
    )

    cli.add_argument(
        '--n-feed-forward-units',
        required=False,
        type=int,
        default=512
    )

    cli.add_argument(
        '--n-heads',
        required=False,
        type=int,
        default=8
    )

    cli.add_argument(
        '--dropout',
        required=False,
        type=float,
        default=0.1
    )

    return cli
