# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules

# 3rd Party Modules

# Project Modules


def make_options(subparser):
    cli = subparser.add_parser(
        'xfmr',
        help='Options for training an Xfmr model.'
    )

    cli.add_argument(
        '--n-decoder-layers',
        required=True,
        type=int,
        help="The number of decoder blocks in the model."
    )

    cli.add_argument(
        '--n-feed-forward-layers',
        required=True,
        type=int,
        help=("The number of layers in the feed forward MLP model. In both the GPT and the "
              "TensorFlow tutorial there are exactly 1 layers. In this model there should be "
              "at least 1 layer, but possibly more. Because the primary layers of this MLP "
              "subnetwork can have an arbitrary number of units there is always 1 additional final "
              "layer that projects the output to the shape expected by the decoder layer.")
    )

    cli.add_argument(
        '--n-embedding-units',
        required=True,
        type=int,
        help="The size of the embedding layer (i.e., the number of units or dimension)."
    )

    cli.add_argument(
        '--n-attention-state-units',
        required=True,
        type=int,
        help="The dimensionality/number of units used in the attention layer."
    )

    cli.add_argument(
        '--n-feed-forward-units',
        required=True,
        type=int,
        help="The dimensionality/number of units of the MLP feed-forward layer."
    )

    cli.add_argument(
        '--embedding-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply to the input embedding layer."
    )

    cli.add_argument(
        '--attention-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply to the attention layers."
    )

    cli.add_argument(
        '--residual-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply to the residual layers."
    )

    cli.add_argument(
        '--decoder-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply to the decoder layers."
    )

    cli.add_argument(
        '--layer-norm-epsilon',
        required=True,
        type=float,
        help="The epsilon value to use with LayerNormalization layers."
    )

    cli.add_argument(
        '--n-heads',
        required=True,
        type=int,
        help="The number of heads to split the inputs into."
    )

    cli.add_argument(
        '--attention-activation-function',
        required=False,
        type=str,
        choices=['softmax', 'relu', 'sigmoid', 'tanh', 'identity'],
        default='softmax'
    )

    cli.add_argument(
        '--head-split-method',
        required=True,
        type=str,
        help=("The method used to split the heads. *reshape*: This is the method used in GPT2 and "
              "in the TensorFlow tutorial. *convolution*: This method applies an GPT2 style Conv1D "
              "layer to increase the dimensionality of the last dimension by a factor of n_heads so "
              "that when the heads are split the final dimension has the same number of units as "
              "the input tensor. *sample*: For each head this method samples a subset of the "
              "features (i.e., units of the final dimension). The number of samples is controlled "
              "by the --head-sample-size option.")
    )

    cli.add_argument(
        '--head-sample-size',
        required=False,
        type=int,
        help=("This determines how many features to sample when using the *sample* method of the "
              "--head-split-method option. It should be "
              "1 < head_sample_size < n_attention_state_units.")
    )

    cli.add_argument(
        '--head-sample-seed',
        required=False,
        type=int,
        help="You can optionally set a seed for some determinism when sampling features."
    )

    cli.add_argument(
        '--normalization-method',
        required=True,
        type=str,
        help=("Which normalization method to use between layers. *layer*: Use layer normalization "
              "as done by the TensorFlow tutorial and GPT2. *batch*: Use batch normalization. "
              "*none*: Do not use any normalization.")
    )

    cli.add_argument(
        '--output-predictions-per-layer',
        action='store_true',
        help="When this this is set then the model will return predictions for each intermediate "
             "decoder block layer."
    )

    cli.add_argument(
        '--decoder-update-method',
        required=True,
        type=str,
        help=("In the decoder block there are 2 significant places where the model updates "
              "its internal state. In both the TensorFlow tutorial and the GPT2 code there is an "
              "additive component to this update. It is not clear what the theory behind it is or "
              "why it is done other than it appears to work. This method allows you to try other "
              "update methods. *additive*: This is the method used by GPT2 and TensorFlow. "
              "*gated*: This is similar but uses a gating mechanism inspired by LSTM/GRUs. "
              "*simple*: This uses the outputs of the intermediate layers directly and does not add "
              "the results from previous processes into the output.")
    )

    cli.add_argument(
        '--scale-attention',
        action='store_true',
        help="When set the attention will be scaled by dk as described in the TensorFlow tutorial."
    )

    cli.add_argument(
        '--activation-function',
        required=True,
        type=str,
        help=("The activation function to use in the MLP feed forward layers. It can be any of the "
              "know activations in the ACT2FN dict in the delamo.layers module or `pgelu` for a "
              "trainable parameterized version of the gelu activation function.")
    )

    cli.add_argument(
        '--use-gpt-convolution',
        action='store_true',
        help=("In several places the GPT2 model applies a custom Conv1D layer to an input. The "
              "description of the layer describes it as similar to a Dense layer, but the weights "
              "are transposed. It's not clear to me why this is beneficial vs a standard linear "
              "layer. If this option is not set then a standard linear layer is used (for most "
              "operations) where a Conv1D layer would have been used in the GPT2 model.")
    )

    cli.add_argument(
        '--auxiliary-loss-method',
        required=True,
        type=str,
        help=("In some previous work it has been found that including predictions from "
              "intermediate layers helps improve the training speed and accuracy.  The choices "
              "are: {depth_weighted, none}. "
              "*depth_weighted*: When using this method the prediction from each intermediate "
              "layer contributes to the loss. The deeper the layer the less it contributes as a "
              "function of 1 / (layer_number * n) where n is controlled by the option "
              "--loss-weight-n and the top (output layer) always has a weight of 1. The weight "
              "of each layer (except the output layer) also decays exponentially as training "
              "progresses. The decay function is exp(-lambda * step_num). The lambda parameter is "
              "controlled by the --loss-weight-lambda option. *none*: Do not use the intermediate "
              "layers to augment the loss.")
    )

    cli.add_argument(
        '--loss-weight-n',
        required=False,
        type=float,
        default=1.0,
        help=("Only required if --auxiliary-loss-method is `depth_weighted`. This controls how "
              "quickly the weight of each deeper layer diminishes.")
    )

    cli.add_argument(
        '--loss-weight-lambda',
        required=False,
        type=float,
        default=1e-4,
        help=("Sets the decay rate for how quickly the weights for the intermediate layers "
              "decay as training progresses.")
    )

    cli.add_argument(
        '--loss-weight-normalized',
        action='store_true',
        help="If set then the loss weights for all the layers will be normalized so they sum to 1."
    )

    return cli
