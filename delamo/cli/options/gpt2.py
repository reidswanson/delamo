# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules

# 3rd Party Modules

# Project Modules


def make_options(subparser):
    cli = subparser.add_parser(
        'gpt2',
        help="Train a GPT2 model."
    )

    cli.add_argument(
        '--n-layers',
        required=True,
        type=int,
        help="The number of decoder blocks in the model."
    )

    cli.add_argument(
        '--n-heads',
        required=True,
        type=int,
        help="The number of attention heads."
    )

    cli.add_argument(
        '--n-embedding-units',
        required=True,
        type=int,
        help="The number of units in the embedding layer(s)."
    )

    cli.add_argument(
        '--n-model-units',
        required=True,
        type=int,
        help="The number of units for other layers of the model."
    )

    cli.add_argument(
        '--embedding-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply for the embedding layers."
    )

    cli.add_argument(
        '--attention-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply for the attention layers."
    )

    cli.add_argument(
        '--residual-dropout-rate',
        required=True,
        type=float,
        help="The amount of dropout to apply for the residual layers."
    )

    cli.add_argument(
        '--layer-norm-epsilon',
        required=True,
        type=float,
        help="The epsilon value used for layer normalization layers."
    )

    cli.add_argument(
        '--scale-attention',
        action='store_true',
        help="Set this to scale the attention values."
    )

    cli.add_argument(
        '--use-cache',
        action='store_true',
        help="Store various intermediate calcuations in a cache."
    )

    cli.add_argument(
        '--output-attentions',
        action='store_true',
        help="Return the attention values with the final output of them model."
    )

    cli.add_argument(
        '--auxiliary-loss-method',
        required=False,
        choices=['depth_weighted', 'none'],  # In theory this should also support `data_weighted`
        help=("Determines how intermediate hidden layers are used to augment the loss "
              "calculation during training.")
    )

    cli.add_argument(
        '--loss-weight-n',
        required=False,
        type=float,
        default=1.0,
        help=("This value determines how much initial weight each auxiliary loss value should"
              "contribute. This is only applicable when the auxiliary-loss-method is "
              "depth_weighted. It should be >= 0.5")
    )

    cli.add_argument(
        '--loss-weight-lambda',
        required=False,
        type=float,
        default=1e-4,
        help="The decay constant for use with the depth_weighted auxiliary loss method."
    )

    cli.add_argument(
        '--loss-weight-normalized',
        action='store_true',
        help="If set, then normalize the auxiliary loss weights so they sum to one."
    )

    return cli
