# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules

# 3rd Party Modules

# Project Modules


def make_options(subparser):
    cli = subparser.add_parser('tfrnn', help="Train an RNN using TensorFlow built in layers.")

    cli.add_argument(
        '--n-layers',
        required=False,
        type=int,
        default=2
    )

    cli.add_argument(
        '--n-embedding-units',
        required=False,
        type=int,
        default=128
    )

    cli.add_argument(
        '--n-units',
        required=False,
        type=int,
        default=256
    )

    cli.add_argument(
        '--cell-type',
        required=False,
        type=str,
        default='gru',
        choices=['rnn', 'gru', 'lstm']
    )

    cli.add_argument(
        '--stateful',
        required=False,
        action='store_true'
    )

    cli.add_argument(
        '--kernel-regularizer',
        required=False,
        type=str,
        choices=['l1', 'l2', 'l1_l2']
    )

    cli.add_argument(
        '--kernel-regularization',
        required=False,
        type=float
    )

    cli.add_argument(
        '--recurrent-regularizer',
        required=False,
        type=str,
        choices=['l1', 'l2', 'l1_l2']
    )

    cli.add_argument(
        '--recurrent-regularization',
        required=False,
        type=float
    )

    cli.add_argument(
        '--dropout-rate',
        required=False,
        type=float
    )

    cli.add_argument(
        '--recurrent-dropout-rate',
        required=False,
        type=float
    )

    return cli
