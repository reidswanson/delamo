# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse

# 3rd Party Modules

# Project Modules


def make_options(parser: argparse.ArgumentParser):
    opt_args = parser.add_argument_group(
        "Optuna",
        "Options for customizing Optuna parameters."
    )
    
    opt_args.add_argument(
        '--load-if-exists',
        action='store_true'
    )

    opt_args.add_argument(
        '--study-file',
        required=True,
        type=str
    )

    opt_args.add_argument(
        '--n-startup-trials',
        required=True,
        type=int
    )

    opt_args.add_argument(
        '--n-trials',
        required=True,
        type=int
    )

    opt_args.add_argument(
        '--optuna-random-seed',
        required=False,
        type=int
    )

    opt_args.add_argument(
        '--trials-file',
        required=False,
        type=str,
        help="If given the trials will also be written to a regular file using pickle."
    )

    opt_args.add_argument(
        '--plot-directory',
        required=False,
        type=str,
        help="If given several plots describing the optimization will be written to this directory."
    )

    return parser
