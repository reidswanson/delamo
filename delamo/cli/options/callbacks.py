# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################
"""
Construct command line options for setting callback parameters.
"""

# Python Modules
import argparse
import datetime
import pathlib
import os

# 3rd Party Modules
from typing import Dict, Any

import tensorflow as tf

# Project Modules


def make_options(cli: argparse.ArgumentParser):
    group = cli.add_argument_group(
        "Callbacks",
        "Options for customizing the callbacks used."
    )
    
    # Model Checkpoint
    group.add_argument(
        '--checkpoint-file',
        required=False,
        type=str,
        default='/tmp/delamo/checkpoints/model.cpt'
    )

    # Tensorboard
    group.add_argument(
        '--tensorboard-directory',
        type=str,
        required=False
    )

    group.add_argument(
        '--tensorboard-histogram-freq',
        type=int,
        required=False,
        default=0
    )

    group.add_argument(
        '--tensorboard-write-graph',
        action='store_true'
    )

    group.add_argument(
        '--tensorboard-embeddings-freq',
        type=int,
        required=False,
        default=2
    )

    group.add_argument(
        '--tensorboard-profile-batch',
        type=int,
        required=False,
        nargs='+',
        default=[0]
    )

    # Early Stopping and Reduce LR On Plateau
    group.add_argument(
        '--monitor',
        required=False,
        type=str,
        default='val_xent',
        choices=['val_xent', 'val_acc', 'val_loss']
    )

    # Early Stopping
    group.add_argument(
        '--early-stopping-min-delta',
        required=False,
        type=float,
        default=0.002,
    )

    group.add_argument(
        '--early-stopping-patience',
        required=False,
        type=int,
        default=5,
    )

    # Reduce LR On Plateau
    group.add_argument(
        '--plateau-patience',
        required=False,
        type=int,
        default=2,
    )

    group.add_argument(
        '--plateau-min-delta',
        required=False,
        type=float,
        default=0.002,
    )

    group.add_argument(
        '--plateau-factor',
        required=False,
        type=float,
        default=0.1,
    )

    return cli


def early_stopping_kwargs(args: argparse.Namespace) -> Dict[str, Any]:
    """
    Return the relevant options for the :class:`~tf.keras.callbacks.EarlyStopping` callback as a 
    dictionary suitable for using as ``kwargs``.
     
    :param args: 
    :return: 
    """
    return {
        'min_delta': args.early_stopping_min_delta,
        'patience': args.early_stopping_patience
    }


def reduce_lr_on_plateau_kwargs(args: argparse.Namespace) -> Dict[str, Any]:
    """
    Return the relevant options for the :class:`~tf.keras.callbacks.ReduceLROnPlateau` callback as 
    a dictionary suitable for using as ``kwargs``.
     
    :param args: 
    :return: 
    """
    return {
        'min_delta': args.plateau_min_delta,
        'patience': args.plateau_patience,
        'factor': args.plateau_factor
    }


def make_early_stopping_from_args(args: argparse.Namespace) -> tf.keras.callbacks.EarlyStopping:
    """
    Create an :class:`~tf.keras.callbacks.EarlyStopping` callback from a set
    of command line arguments.

    :param args:
    :return:
    """
    return tf.keras.callbacks.EarlyStopping(
        monitor=args.monitor,
        mode='max' if 'acc' in args.monitor else 'min',
        min_delta=args.early_stopping_min_delta,
        patience=args.early_stopping_patience,
        restore_best_weights=True
    )


def make_reduce_lr_on_plateau_from_args(
        args: argparse.Namespace
) -> tf.keras.callbacks.ReduceLROnPlateau:
    """
    Create a :class:`~tf.keras.callbacks.ReduceLROnPlateau` callback from a set
    of command line arguments.

    :param args:
    :return:
    """
    return tf.keras.callbacks.ReduceLROnPlateau(
        monitor=args.monitor,
        mode='max' if 'acc' in args.monitor else 'min',
        patience=args.plateau_patience,
        min_delta=args.plateau_min_delta,
        factor=args.plateau_factor
    )


def make_model_checkpoint_from_args(args: argparse.Namespace) -> tf.keras.callbacks.ModelCheckpoint:
    """
    Create a :class:`~tf.keras.callbacks.ModelCheckpoint` callback from a set
    of command line arguments.

    :param args:
    :return:
    """
    return tf.keras.callbacks.ModelCheckpoint(
        args.checkpoint_file,
        monitor=args.monitor,
        mode='max' if 'acc' in args.monitor else 'min',
        save_weights_only=True,
        save_best_only=True
    )


def make_tensorboard_dir_from_args(args: argparse.Namespace) -> str:
    return os.path.join(
        args.tensorboard_directory,
        args.command,
        datetime.datetime.now().strftime('%Y%m%d-%H:%M:%S.%f')
    )


def make_tensorboard_from_args(args: argparse.Namespace) -> tf.keras.callbacks.TensorBoard:
    """
    Create a :class:`~tf.keras.callbacks.TensorBoard` callback from a set
    of command line arguments.

    :param args:
    :return:
    """
    run_dir = make_tensorboard_dir_from_args(args)

    pathlib.Path(run_dir).mkdir(parents=True, exist_ok=True)
    profile_batch = args.tensorboard_profile_batch
    if profile_batch:
        profile_batch = profile_batch[0] if len(profile_batch) == 1 else profile_batch
    else:
        profile_batch = 0

    return tf.keras.callbacks.TensorBoard(
        log_dir=run_dir,
        histogram_freq=args.tensorboard_histogram_freq,
        write_graph=args.tensorboard_write_graph,
        embeddings_freq=args.tensorboard_embeddings_freq,
        profile_batch=profile_batch
    )
