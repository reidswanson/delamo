# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules

# 3rd Party Modules

# Project Modules


def make_options(subparser):
    cli = subparser.add_parser('dernn', help="Train an RNN using custom layers.")

    cli.add_argument(
        '--n-layers',
        required=False,
        type=int,
        default=2
    )

    cli.add_argument(
        '--n-embedding-units',
        required=False,
        type=int,
        default=128
    )

    cli.add_argument(
        '--n-units',
        required=False,
        type=int,
        default=256
    )

    cli.add_argument(
        '--cell-type',
        required=False,
        type=str,
        default='gru',
        choices=['lstm']
    )

    return cli
