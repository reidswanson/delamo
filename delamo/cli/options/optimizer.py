# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse

# 3rd Party Modules

# Project Modules
from delamo.optimizers import TransformerSchedule


def make_options(cli: argparse.ArgumentParser):
    optimizer_opts = cli.add_argument_group(
        "Optimizer",
        "Options for customizing the optimzer."
    )

    optimizer_opts.add_argument(
        '--optimizer-name',
        required=False,
        type=str,
        default='adam',
        choices=['adam', 'sgd', 'transformer']
    )

    optimizer_opts.add_argument(
        '--clipnorm',
        type=float,
        default=None,
    )

    # region Any Optimizer
    optimizer_opts.add_argument(
        '--learning-rate',
        required=False,
        type=float,
        default=0.001
    )

    optimizer_opts.add_argument(
        '--optimizer_optspnorm',
        required=False,
        type=float,
        default=None
    )
    # endregion Any Optimizer

    # region Adam
    optimizer_opts.add_argument(
        '--beta_1',
        required=False,
        type=float,
        default=0.9
    )

    optimizer_opts.add_argument(
        '--beta_2',
        required=False,
        type=float,
        default=0.999
    )

    optimizer_opts.add_argument(
        '--epsilon',
        required=False,
        type=float,
        default=1e-7
    )

    optimizer_opts.add_argument(
        '--amsgrad',
        action='store_true'
    )
    # endregion Adam

    # region SGD
    optimizer_opts.add_argument(
        '--momentum',
        required=False,
        type=float,
        default=0.0
    )

    optimizer_opts.add_argument(
        '--nesterov',
        action='store_true'
    )
    # endregion SGD

    # region Transformer
    optimizer_opts.add_argument(
        '--schedule-size',
        required=False,
        type=int,
        help=(
            "Only applicable for the transformer optimizer, but must be set when using it."
        )
    )

    optimizer_opts.add_argument(
        '--schedule-warmup',
        required=False,
        type=int,
        default=40000
    )
    # endregion Transformer

    return cli


def make_optimizer_params_from_args(args: argparse.Namespace):
    # All optimizers potentially use clipnorm
    optimizer_params = {'clipnorm': args.clipnorm}

    if args.optimizer_name == 'sgd':
        optimizer_params.update({
            'learning_rate': args.learning_rate,
            'momentum': args.momentum,
            'nesterov': args.nesterov
        })
    else:
        if args.optimizer_name == 'transformer':
            learning_rate = TransformerSchedule(args.schedule_size, args.schedul_warmup)
            optimizer_params['learning_rate'] = learning_rate
        else:
            optimizer_params['learning_rate'] = args.learning_rate

        optimizer_params.update({
            'beta_1': args.beta_1,
            'beta_2': args.beta_2,
            'epsilon': args.epsilon,
            'amsgrad': args.amsgrad,
        })

    return {
        'name': args.optimizer_name,
        'args': optimizer_params
    }