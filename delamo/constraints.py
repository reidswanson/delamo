# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules

# 3rd Party Modules
import tensorflow as tf
import tensorflow.keras.constraints as tfc

# Project Modules


class RangeConstraint(tfc.Constraint):
    def __init__(self, lower_bound: float, upper_bound: float, **kwargs):
        """
        Constrain weights such that :math:`lower_bound <= weight <= upper_bound`

        :param lower_bound:
        :param upper_bound:
        :param kwargs:
        """
        super().__init__(**kwargs)

        self.lower_bound = lower_bound
        self.upper_bound = upper_bound

    def __call__(self, w):
        lower_mask = tf.math.greater_equal(w, self.lower_bound)
        upper_mask = tf.math.less_equal(w, self.upper_bound)

        # First filter out all values less than the lower bound
        w = w * tf.cast(lower_mask, dtype=w.dtype)

        # Then set the 0 entries to the lower bound
        w = w + tf.cast(tf.logical_not(lower_mask), dtype=w.dtype) * self.lower_bound

        # Filter out all the values greater than the upper bound
        w = w * tf.cast(upper_mask, dtype=w.dtype)

        # Then set the 0 entries to the upper bound
        w = w + tf.cast(tf.logical_not(upper_mask), dtype=w.dtype) * self.upper_bound

        return w

    def get_config(self):
        return {
            'lower_bound': self.lower_bound,
            'upper_bound': self.upper_bound
        }
