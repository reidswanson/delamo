# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# This loss is derived from the transformer tutorial on the TensorFlow
# website:
# https://www.tensorflow.org/tutorials/text/transformer#loss_and_metrics

# Python Modules
import logging

# 3rd Party Modules
from typing import Union

import tensorflow as tf

# Project Modules

log = logging.getLogger(__name__)


class MaskedSparseCategoricalCrossentropy(tf.keras.losses.Loss):
    def __init__(
            self,
            mask_value: Union[int, float] = 0,
            name: str = 'MaskedSparseCategoricalCrossentropy',
            **kwargs
    ):
        """
        A simple wrapper for 
        :class:`tf.keras.losses.SparseCategoricalCrossntropy` that excludes
        masked values from the calculation.
        
        See: https://www.tensorflow.org/tutorials/text/transformer#loss_and_metrics

        :param mask_value: The value indicating whether an entry should be
               masked.
        :param name: The name of the operation.
        """
        super().__init__(name=name, **kwargs)

        self.mask_value = mask_value

    def get_config(self):
        cfg = super().get_config()
        cfg.update({
            'mask_value': self.mask_value
        })

        return cfg

    @tf.function
    def call(self, y_true: tf.Tensor, y_pred: tf.Tensor) -> float:
        """
        Compute the categorical crossentropy excluding masked values.

        :param y_true: The true values.
        :param y_pred: The predicted values.
        :return: The crossentropy.
        """
        loss_ = tf.nn.sparse_softmax_cross_entropy_with_logits(y_true, y_pred)
        mask = tf.cast(tf.math.logical_not(tf.math.equal(y_true, self.mask_value)), loss_.dtype)
        loss_ *= mask

        result = tf.math.divide_no_nan(tf.reduce_sum(loss_), tf.reduce_sum(mask))

        return result
