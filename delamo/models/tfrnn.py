# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

"""
Standard recurrent neural network models using TensorFlow / Keras primitives.
"""

# Python Modules
import argparse
import logging

from typing import Any, Dict, Optional

# 3rd Party Modules
import numpy as np
import tensorflow as tf
import tensorflow.keras.layers as tfl

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
from delamo.cli.options.optimizer import make_optimizer_params_from_args
from delamo.models.core import LanguageModel

log = logging.getLogger(__name__)


class RnnLanguageModel(LanguageModel):
    layer_classes = {
        'gru': tfl.GRU,
        'lstm': tfl.LSTM,
        'rnn': tfl.SimpleRNN
    }

    regularizers = {
        'l1': tf.keras.regularizers.l1,
        'l2': tf.keras.regularizers.l2,
        'l1_l2': tf.keras.regularizers.l1_l2
    }

    def __init__(
            self,
            batch_size: int,
            sequence_length: int,
            n_layers: int,
            n_embedding_units: int,
            n_units: int,
            vocabulary: Dict[str, int],
            dataset_method: str,
            cell_type: str = 'gru',
            stateful: bool = False,
            kernel_regularizer: Optional[str] = None,
            kernel_regularization: Optional[float] = None,
            recurrent_regularizer: Optional[str] = None,
            recurrent_regularization: Optional[float] = None,
            dropout_rate: Optional[float] = None,
            recurrent_dropout_rate: Optional[float] = None,
            n_softmax_samples: int = 5000,
            optimizer_params: Dict[str, Any] = None,
            embedding_matrix: np.ndarray = None,
            **kwargs
    ):
        """

        :param cell_type:
        :param kwargs:
        """
        super().__init__(
            sequence_length,
            vocabulary,
            dataset_method,
            n_softmax_samples,
            optimizer_params,
            **kwargs
        )

        self.batch_size = batch_size
        self.n_layers = n_layers
        self.n_embedding_units = n_embedding_units
        self.n_units = n_units
        self.cell_type = cell_type.lower()
        self.stateful = stateful
        self.kernel_regularizer = kernel_regularizer
        self.kernel_regularization = kernel_regularization or 0.0
        self.recurrent_regularizer = recurrent_regularizer
        self.recurrent_regularization = recurrent_regularization or 0.0
        self.dropout_rate = dropout_rate or 0.0
        self.recurrent_dropout_rate = recurrent_dropout_rate or 0.0
        self.embedding_matrix = embedding_matrix

        if embedding_matrix is not None:
            if n_embedding_units != embedding_matrix.shape[1]:
                log.warning(
                    f"The number of embedding units ({self.n_embedding_units}) does not match "
                    f"the dimension of the pretrained vectors: ({embedding_matrix.shape[1]}). "
                    f"Setting the number of units to match the pretrained vectors."
                )
                self.n_embedding_units = n_embedding_units = embedding_matrix.shape[1]

            log.info(f"Using pretrained word vector matrix of shape: {embedding_matrix.shape}")
            self.embedding_layer = tfl.Embedding(
                self.vocab_size,
                n_embedding_units,
                embeddings_initializer=tf.keras.initializers.Constant(embedding_matrix),
                trainable=False,
                mask_zero=True
            )
        else:
            self.embedding_layer = tfl.Embedding(
                self.vocab_size,
                n_embedding_units,
                mask_zero=True
            )
        self.first_layer = self.make_layer(self.stateful)
        self.recurrent_layers = [self.make_layer() for _ in range(n_layers - 1)]
        self.final_layer = tfl.Dense(self.vocab_size)

        self.first_layer.build((batch_size, sequence_length, n_embedding_units))
        for recurrent_layer in self.recurrent_layers:
            recurrent_layer.build((batch_size, sequence_length, n_units))

    @classmethod
    def prepare_model(cls, args: argparse.Namespace, vocab: Dict[str, int], **kwargs) -> LanguageModel:
        model = cls(
            args.batch_size,
            args.sequence_length,
            args.n_layers,
            args.n_embedding_units,
            args.n_units,
            vocab,
            args.dataset_method,
            args.cell_type,
            args.stateful,
            args.kernel_regularizer,
            args.kernel_regularization,
            args.recurrent_regularizer,
            args.recurrent_regularization,
            args.dropout_rate,
            args.recurrent_dropout_rate,
            args.n_softmax_samples,
            make_optimizer_params_from_args(args),
            **kwargs
        )

        # return cls.compile_model(model, 0)
        model.compile_model()

        return model

    def get_config(self) -> Dict[str, Any]:
        config = super().get_config()
        config.update({
            'batch_size': self.batch_size,
            'n_layers': self.n_layers,
            'n_embedding_units': self.n_embedding_units,
            'n_units': self.n_units,
            'cell_type': self.cell_type,
            'stateful': self.stateful,
            'kernel_regularizer': self.kernel_regularizer,
            'kernel_regularization': self.kernel_regularization,
            'recurrent_regularizer': self.recurrent_regularizer,
            'recurrent_regularization': self.recurrent_regularization,
            'dropout_rate': self.dropout_rate,
            'recurrent_dropout_rate': self.recurrent_dropout_rate,
            'optimizer_prams': self.optimizer_params,
            'embedding_matrix': self.embedding_matrix
        })

        return config

    def estimate_memory_usage(self, parameters: Dict[str, Any], vocab_size: int) -> int:
        """

        :param parameters:
        :param vocab_size:
        :return:
        """
        n_model_params = self.count_params()

        # The policy name should tell us how many bytes are needed for the operations
        policy_name = mixed_precision.global_policy().name

        # 2 bytes for 16 bit, 4 bytes for 32 and 8 bytes for 64
        dtype_size = 2 if '16' in policy_name else 4 if '32' in policy_name else 8

        return n_model_params * dtype_size

    # noinspection PyMethodOverriding
    def call(self, inputs: tf.Tensor, training: bool):
        if isinstance(inputs, (tuple, list)):
            x, y = inputs
        else:
            x, y = inputs, None
        x = self.embedding_layer(x)

        # There is always at least one recurrent layer, which may or may not
        # be stateful.
        x = self.first_layer(x)

        # There may optionally be additional recurrent layers, but these
        # will not be stateful.
        for recurrent_layer in self.recurrent_layers:
            x = recurrent_layer(x)

        logits = self.final_layer(x)

        if y is not None and self.n_softmax_samples > 0:
            self.add_sampled_softmax_loss(x, y, self.final_layer.weights)

        return logits

    def make_layer(self, is_stateful: bool = False):
        layer_class = self.layer_classes[self.cell_type]

        kernel_regularizer = self.make_regularizer(
            self.kernel_regularizer,
            self.kernel_regularization
        )

        recurrent_regularizer = self.make_regularizer(
            self.recurrent_regularizer,
            self.recurrent_regularization
        )

        # If we are using a stateful RNN then we can't use the CuDNN implementation
        # unless the batch_size divides evenly into the corpus, which is extremely
        # unlikely. Otherwise we have to pad the final batch. CuDNN can
        # only handle padding at the end of a tensor. However, if we have to
        # pad a batch to make it an equal size, the remaining tensors in the
        # batch will be all 0s (masked values) and CuDNN will consider that
        # a mask value at the beginning. In theory there is a fix in tensorflow
        # that should backoff to a regular RNN in those cases, but it doesn't
        # seem to work.

        # The alternative is to force the use of a standard RNN by intentionally
        # not meeting the requirements to use CuDNN. The most non invasive
        # parameter seems to be setting a very small value for the recurrent
        # dropout rate (if it is not already set).
        # https://github.com/tensorflow/tensorflow/issues/33148
        # if self.stateful is True and self.recurrent_dropout_rate == 0:
        if self.recurrent_dropout_rate == 0:
            recurrent_dropout_rate = 1e-9
        else:
            recurrent_dropout_rate = self.recurrent_dropout_rate

        layer = layer_class(
            self.n_units,
            kernel_regularizer=kernel_regularizer,
            recurrent_regularizer=recurrent_regularizer,
            dropout=self.dropout_rate,
            recurrent_dropout=recurrent_dropout_rate,
            stateful=is_stateful,
            return_sequences=True
        )

        return layer

    @classmethod
    def make_regularizer(cls, regularizer_name: Optional[str], regularizer_value: Optional[float]):
        regularizer = cls.regularizers.get(regularizer_name)

        return regularizer(regularizer_value) if regularizer else None
