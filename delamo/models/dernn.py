# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

"""
Custom RNN models mainly for pedagogical purposes.
"""

# Python Modules
import argparse

# 3rd Party Modules
from typing import Tuple, cast, List, Dict, Optional, Any

import tensorflow as tf
import tensorflow.keras.layers as tfl

# Project Modules
from delamo.cli.options.optimizer import make_optimizer_params_from_args
from delamo.models.core import LanguageModel


class LstmCell(tfl.Layer):
    def __init__(self, n_units: int, **kwargs):
        """
        This is a very basic implementation of an LSTM for pedagogical purposes
        to see how custom RNN cells are implemented.

        :param n_units: The number of hidden units.
        :param kwargs:
        """
        super().__init__(**kwargs)

        self.n_units = n_units
        self.n_output = n_units

        self.state_size = [tf.TensorShape([n_units]), tf.TensorShape([n_units])]
        self.output_size = tf.TensorShape([n_units])

        self.w_input = tfl.Dense(n_units, activation='sigmoid')
        self.w_forget = tfl.Dense(n_units, activation='sigmoid')
        self.w_carry = tfl.Dense(n_units, activation='tanh')
        self.w_output = tfl.Dense(n_units, activation='sigmoid')

    # noinspection PyMethodOverriding
    def call(
            self,
            inputs: tf.Tensor,
            states: Tuple[tf.Tensor, tf.Tensor],
            training: bool = False,
            **kwargs
    ) -> Tuple[tf.Tensor, List[tf.Tensor]]:
        h_prev, c_prev = states

        hx = tf.concat([h_prev, inputs], axis=-1)

        forget_t = self.w_forget(hx)
        input_t = self.w_input(hx)
        output_t = self.w_output(hx)
        carry_1_t = self.w_carry(hx)
        carry_2_t = forget_t * c_prev + input_t * carry_1_t
        hidden_t = output_t * tf.math.tanh(carry_2_t)

        hidden_t = cast(tf.Tensor, hidden_t)

        return hidden_t, [hidden_t, carry_2_t]


class RnnLanguageModel(LanguageModel):
    cell_types = {
        'lstm': LstmCell
    }

    def __init__(
            self,
            n_layers: int,
            n_embedding_units: int,
            n_units: int,
            sequence_length: int,
            vocabulary: Dict[str, int],
            dataset_method: str,
            cell_type: str = 'lstm',
            n_softmax_samples: int = 0,
            optimizer_params: Optional[Dict[str, Any]] = None,
            **kwargs
    ):
        super().__init__(
            sequence_length,
            vocabulary,
            dataset_method,
            n_softmax_samples,
            optimizer_params,
            **kwargs
        )

        self.n_layers = n_layers
        self.n_embedding_units = n_embedding_units
        self.n_units = n_units
        self.cell_type = cell_type

        self.embedding_layer = tfl.Embedding(self.vocab_size, n_embedding_units, mask_zero=True)
        self.recurrent_layers = [self.make_layer() for _ in range(n_layers)]
        self.final_layer = tfl.Dense(self.vocab_size)

    @classmethod
    def prepare_model(cls, args: argparse.Namespace, vocab: Dict[str, int]) -> "LanguageModel":
        model = cls(
            args.n_layers,
            args.n_embedding_units,
            args.n_units,
            args.sequence_length,
            vocab,
            args.dataset_method,
            args.cell_type,
            args.n_softmax_samples,
            make_optimizer_params_from_args(args)
        )

        # return cls.compile_model(model, 0)
        model.compile_model()

        return model

    def get_config(self) -> Dict[str, Any]:
        config = super().get_config()
        config.update({
            'n_layers': self.n_layers,
            'n_embedding_units': self.n_embedding_units,
            'n_units': self.n_units,
            'cell_type': self.cell_type,
            'vocabulary': self.vocabulary,
            'n_softmax_samples': self.n_softmax_samples,
            'optimizer_params': self.optimizer_params
        })

        return config

    # noinspection PyMethodOverriding
    def call(self, inputs: tf.Tensor, training: bool):
        x, y = inputs
        x = self.embedding_layer(x)

        for recurrent_layer in self.recurrent_layers:
            x = recurrent_layer(x)

        logits = self.final_layer(x)

        if y is not None and self.n_softmax_samples > 0:
            self.add_sampled_softmax_loss(x, y, self.final_layer.weights)

        return logits

    def make_layer(self):
        cell_type = self.cell_types[self.cell_type]

        cell = cell_type(self.n_units)
        rnn = tfl.RNN(cell, return_sequences=True)

        return rnn
