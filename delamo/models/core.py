# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import abc
import argparse
import functools
import logging

from typing import cast, Any, Dict, Optional

# 3rd Party Modules
import tensorflow as tf

# Project Modules
from delamo.losses import MaskedSparseCategoricalCrossentropy as CrossentropyLoss
from delamo.metrics import MaskedSparseCategoricalCrossentropy as CrossentropyMetric, \
    MaskedSparseCategoricalAccuracy as AccuracyMetric
from delamo.optimizers import TransformerSchedule

log = logging.getLogger(__name__)


class LanguageModel(tf.keras.models.Model, abc.ABC):
    def __init__(
            self,
            sequence_length: int,
            vocabulary: Dict[str, int],
            dataset_method: str,
            n_softmax_samples: int = 0,
            optimizer_params: Dict[str, Any] = None,
            **kwargs
    ):
        """

        :param sequence_length:
        :param vocabulary: A dictionary mapping words (strings) to unique
               integers.
        :param dataset_method:
        :param n_softmax_samples: A value > 0 indicates the model should use
               the sampled softmax loss during training instead of the full
               softmax calculation.
        :param optimizer_params:
        :param kwargs:
        """
        super().__init__()

        self.vocabulary = vocabulary
        self.id2word = {v: k for k, v in vocabulary.items()}
        self.dataset_method = dataset_method
        self.sequence_length = sequence_length
        self.vocab_size = len(vocabulary)
        self.n_softmax_samples = n_softmax_samples
        self.optimizer_params = optimizer_params or self.default_optimizer_params
        self.end_id = self.vocabulary.get('</s>', -1)

        # Create a partial function for convenience
        self.softmax_fn = functools.partial(
            tf.nn.sampled_softmax_loss,
            num_classes=self.vocab_size,
            num_sampled=self.n_softmax_samples,
        )

    @property
    def default_optimizer_params(self) -> Dict[str, Any]:
        return {
            'name': 'adam',
            'args': {
                'learning_rate': 0.001,
                'beta_1': 0.9,
                'beta_2': 0.999,
                'epsilon': 1e-7,
                'amsgrad': False,
                'clipnorm': None
            }
        }

    @classmethod
    def prepare_model(
            cls,
            args: argparse.Namespace,
            vocab: Dict[str, int],
            **kwargs
    ) -> "LanguageModel":
        raise NotImplementedError()

    def compile_model(self) -> "LanguageModel":
        optimizer = self._make_optimizer()

        # Calculating the shingled crossentropy is painfully slow.
        # I should probably calculate the regular cross entropy while training
        # and then after training is done calculate the shingled crossentropy
        # for the final evaluation on the validation and test sets.
        metrics = [
            AccuracyMetric(name='acc'),
            CrossentropyMetric(name='xent')
        ]
        loss = CrossentropyLoss()

        self.compile(
            optimizer=optimizer,
            loss=loss,
            metrics=metrics
        )

        return self

    @classmethod
    def load_from_files(cls, config_file: str, weights_file: str, compile_model: bool = False):
        import importlib
        import pickle

        with open(config_file, 'rb') as fh:
            model_config = pickle.load(fh)

        # In case there are models defined in other modules
        # use reflection to get the class type from the config.
        # This is a workaround so that I don't have to have a nasty switch
        # statement when loading a model.
        class_path = model_config['class']
        class_tokens = class_path.split('.')
        module_path = '.'.join(class_tokens[:-1])
        class_name = class_tokens[-1]
        class_module = importlib.import_module(module_path)
        model_class = getattr(class_module, class_name)

        model_config.pop('class')
        model = cast(LanguageModel, model_class(**model_config))
        model.load_weights(weights_file)

        if compile_model:
            model.compile_model()

        return model

    def generate(
            self,
            text_seed: str = '<s>',
            top_n: Optional[int] = None,
            temperature: float = 1.0,
            random_seed: Optional[int] = None
    ):
        """
        Randomly generate text from the model.

        :param text_seed: An initial textual prefix to seed the generation.
        :param top_n: If this is set, then only words who's probability
               is in the top ``n`` will be sampled. Otherwise any word in the
               vocabulary will be sampled regardless of how small its probability
               is.
        :param temperature: Use this value to bias the sampling process.
        :param random_seed: Use this to seed the random number generator to
               allow reproducible results.
        :return:
        """
        # Choose among all the
        top_n = top_n or len(self.vocabulary)
        unk_id = self.vocabulary['<unk>']
        end_id = self.vocabulary.get('</s>', None)

        text_tokens = text_seed.split()
        int_tokens = [self.vocabulary.get(t, unk_id) for t in text_tokens]
        seed_len = len(int_tokens)

        # Create a padded sequence as the (first) input to the model.
        input_seq = tf.constant(int_tokens + [0] * (self.sequence_length - seed_len))
        input_seq = tf.cast(tf.expand_dims(input_seq, 0), tf.int32)

        # The output will have at least the input seed.
        generated_output = [t for t in int_tokens]

        for curr_pred_idx in range(seed_len - 1, self.sequence_length - 1):
            x = self._prepare_input(input_seq)

            y_pred = self(x, training=False)

            if isinstance(y_pred, (tuple, list)):
                y_pred, _ = y_pred

            # Get the current prediction
            y_pred = tf.nn.softmax(y_pred[0, curr_pred_idx])

            top_idx = tf.argsort(y_pred, direction='DESCENDING')[:top_n]
            top_probs = tf.gather(y_pred, top_idx)

            # Renormalize (not sure if this is really necessary since I think
            # tf.random.categorical will do it anyway, but it can't hurt)
            top_probs = top_probs / tf.reduce_sum(top_probs)

            top_probs = tf.math.log(top_probs / temperature)
            sample = tf.random.categorical(
                tf.expand_dims(top_probs, 0),
                num_samples=1,
                dtype=tf.int32,
                seed=random_seed
            )

            token_id = top_idx[sample[0, 0]]
            generated_output.append(token_id.numpy().item())
            input_seq = tf.tensor_scatter_nd_update(
                input_seq,
                [[0, curr_pred_idx+1]],
                [token_id]
            )

            if token_id == end_id:
                break

        generated_output = [self.id2word[t] for t in generated_output]
        generated_output = ' '.join(generated_output)

        return generated_output

    def train_step(self, data):
        x, y_true = data

        x = self._prepare_input(x, y_true)
        with tf.GradientTape() as tape:
            y_pred = self(x, True)

            if self.n_softmax_samples > 0:
                loss = tf.reduce_mean(self.losses)
            else:
                loss = self.compiled_loss(y_true, y_pred)

        # For custom training steps, users can just write:
        trainable_variables = self.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, trainable_variables))

        result = {
            # Do not add the metrics for the training steps. They are kind of
            # interesting to track, but are slow to compute.
            'loss': loss
        }

        return result

    def test_step(self, data):
        x, y_true = data

        x = self._prepare_input(x, y_true)
        y_pred = self(x, False)

        if self.n_softmax_samples > 0:
            loss = tf.reduce_mean(self.losses)
        else:
            loss = self.compiled_loss(y_true, y_pred)

        self.compiled_metrics.update_state(y_true, y_pred)

        result = {m.name: m.result() for m in self.metrics}
        result['loss'] = loss

        return result

    @abc.abstractmethod
    def estimate_memory_usage(self, parameters: Dict[str, Any], vocab_size: int) -> int:
        """
        Estimate the amount of memory (in bytes) needed to process a single
        example with this model. Currently, this uses a simple heuristic.
        In the future it would be nice to use a more general and robust technique like
        the `DNNMem framework by Microsoft <https://www.microsoft.com/en-us/research/publication/estimating-gpu-memory-consumption-of-deep-learning-models/>`_.

        :param parameters: The initialization parameters for the model.
        :param vocab_size: The size of the vocabulary.
        :return: The number of bytes.
        """
        raise NotImplementedError()

    def add_sampled_softmax_loss(self, x, y, weights):
        weights, biases = weights
        weights = tf.transpose(weights)
        dim = tf.shape(x)[2]

        x = tf.reshape(x, (-1, dim))
        y = tf.reshape(y, (-1, 1))

        softmax_values = self.softmax_fn(weights, biases, y, x)

        # Softmax values has only 1 dimension of shape [batch_size * seqlen].
        # Whereas y (and the mask) is a 2 dimensional column vector.
        # To make the elementwise multiplication work we need to make the
        # shapes compatible.
        mask = tf.squeeze(tf.math.logical_not(tf.math.equal(y, 0)))
        mask = tf.cast(mask, dtype=softmax_values.dtype)

        softmax_values *= mask

        softmax_loss = tf.reduce_sum(softmax_values) / tf.reduce_sum(mask)

        self.add_loss(softmax_loss)

    def get_config(self) -> Dict[str, Any]:
        return {
            'class': f"{self.__class__.__module__}.{self.__class__.__name__}",
            'sequence_length': self.sequence_length,
            'vocabulary': self.vocabulary,
            'dataset_method': self.dataset_method,
            'n_softmax_samples': self.n_softmax_samples,
            'optimizer_params': self.optimizer_params
        }

    # region Utility Methods
    def _prepare_input(self, x: tf.Tensor, y: Optional[tf.Tensor] = None):
        return x, y

    def _make_optimizer(self) -> tf.keras.optimizers.Optimizer:
        optimizer_name = self.optimizer_params['name']
        optimizer_args = self.optimizer_params['args']

        if optimizer_name == 'adam':
            return tf.keras.optimizers.Adam(**optimizer_args)
        elif optimizer_name == 'sgd':
            return tf.keras.optimizers.SGD(**optimizer_args)
        elif optimizer_name == 'transformer':
            learning_rate = TransformerSchedule(
                optimizer_args.pop('schedule_size'),
                optimizer_args.pop('schedule_warmup')
            )
            return tf.keras.optimizers.Adam(learning_rate, **optimizer_args)
        else:
            raise ValueError(f"The optimizer '{optimizer_name}' is not supported.")
    # endregion Utility Methods
