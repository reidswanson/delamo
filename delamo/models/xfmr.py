# ##############################################################################
#  Copyright 2018 The OpenAI Team Authors and HuggingFace Inc. team.
#  Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################
"""
This is a decoder only language model that is a highly configurable hybrid
of the Transformer from the TensorFlow tutorial and the GPT-2 model from
the HuggingFace library.
"""
# Python Modules
import argparse
import logging

from typing import Dict, List, Tuple, Optional, Any

# 3rd Party Modules
import tensorflow as tf
import tensorflow.keras.layers as tfl

# Project Modules
from delamo.activations import get_activation, PGeLU
from delamo.cli.options.optimizer import make_optimizer_params_from_args
from delamo.layers import Conv1D
from delamo.models.core import LanguageModel


log = logging.getLogger(__name__)


# region Global Methods
from delamo.utils.modeling import shape_list


def make_normalization_layers(n: int, method: str, epsilon: float) -> List[tfl.Layer]:
    if method in (None, 'none'):
        return [tfl.Lambda(lambda x: x) for _ in range(n)]
    elif method == 'layer':
        return [tfl.LayerNormalization(epsilon=epsilon) for _ in range(n)]
    elif method == 'batch':
        return [tfl.BatchNormalization(epsilon=epsilon) for _ in range(n)]

    raise NotImplementedError(f"The output normalization method is not implemented: {method}")


def make_activation(activation_name: str):
    """
    Get the activation layer (or function) given the configuration setting.

    :return:
    """
    if activation_name == 'pgelu':
        return PGeLU()
    else:
        return get_activation(activation_name)
# endregion Global Methods


class XfmrConfig(dict):
    # Subclassing dict makes the class JSON serializable, which is necessary for optuna.
    # There are some downsides and pitfalls to this approach, but for such a simple class and
    # use case it is unlikely to be a problem here.

    # Setting the following class attributes allows '.' access to the dictionary entries.
    # See: https://stackoverflow.com/a/1328686
    __getattr__ = dict.__getitem__
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

    def __init__(
            self,
            dataset_method: str = 'slice',
            sequence_length: int = 200,
            n_decoder_layers: int = 5,
            n_feed_forward_layers: int = 1,
            n_embedding_units: int = 512,
            n_attention_state_units: int = 512,
            n_feed_forward_units: int = 512,
            embedding_dropout_rate: float = 0.25,
            attention_dropout_rate: float = 0.25,
            residual_dropout_rate: float = 0.25,
            decoder_dropout_rate: float = 0.25,
            layer_norm_epsilon: float = 1e-9,
            n_heads: int = 8,
            attention_activation_function: str = 'softmax',
            head_split_method: str = 'reshape',
            head_sample_size: int = 64,
            head_sample_seed: Optional[int] = None,
            normalization_method: str = 'layer',
            output_predictions_per_layer: bool = False,
            decoder_update_method: str = 'additive',
            scale_attention: bool = True,
            activation_function: str = 'gelu',
            use_gpt_convolution: bool = True,
            auxiliary_loss_method: Optional[str] = None,
            loss_weight_n: float = 1.0,
            loss_weight_lambda: float = 1e-4,
            loss_weight_normalized: bool = True,
            optimizer_params: Optional[Dict[str, Any]] = None
    ):
        super().__init__(
            dataset_method=dataset_method,
            sequence_length=sequence_length,
            n_decoder_layers=n_decoder_layers,
            n_feed_forward_layers=n_feed_forward_layers,
            n_embedding_units=n_embedding_units,
            n_attention_state_units=n_attention_state_units,
            n_feed_forward_units=n_feed_forward_units,
            embedding_dropout_rate=embedding_dropout_rate,
            attention_dropout_rate=attention_dropout_rate,
            residual_dropout_rate=residual_dropout_rate,
            layer_norm_epsilon=layer_norm_epsilon,
            n_heads=n_heads,
            attention_activation_function=attention_activation_function,
            head_split_method=head_split_method,
            head_sample_size=head_sample_size,
            head_sample_seed=head_sample_seed,
            normalization_method=normalization_method,
            output_predictions_per_layer=output_predictions_per_layer,
            decoder_dropout_rate=decoder_dropout_rate,
            decoder_update_method=decoder_update_method,
            scale_attention=scale_attention,
            activation_function=activation_function,
            use_gpt_convolution=use_gpt_convolution,
            auxiliary_loss_method=auxiliary_loss_method,
            loss_weight_n=loss_weight_n,
            loss_weight_lambda=loss_weight_lambda,
            loss_weight_normalized=loss_weight_normalized,
            optimizer_params=optimizer_params
        )


class Mlp(tfl.Layer):
    def __init__(self, config: XfmrConfig, **kwargs):
        super().__init__(**kwargs)

        self.config = config

        self.layers = self._make_layers()
        self.activations = [
            make_activation(config.activation_function)
            for _ in range(config.n_feed_forward_layers)
        ]
        self.projection = self._make_projection()
        self.dropout = tfl.Dropout(config.residual_dropout_rate)

    def call(self, x: tf.Tensor, training: bool = False):
        h = x
        for layer, activation in zip(self.layers, self.activations):
            h = layer(h)
            h = activation(h)

        h = self.projection(h)
        h = self.dropout(h, training=training)

        return h

    # region Utility Methods
    def _make_layers(self):
        n_feed_forward_units = self.config.n_feed_forward_units
        n_layers = self.config.n_feed_forward_layers

        if self.config.use_gpt_convolution:
            # The input to the first layer should have `n_embedding_units` units and
            # output a tensor with `n_feed_forward_units` as the final dimension.
            layers = [Conv1D(n_feed_forward_units, self.config.n_embedding_units)]

            # The subsequent layers are simple dense layers that map from `n_feed_forward_units`
            # to `n_feed_forward_units`
            layers += [tfl.Dense(n_feed_forward_units) for _ in range(n_layers - 1)]

            return layers
        else:
            return [tfl.Dense(n_feed_forward_units) for _ in range(n_layers)]

    def _make_projection(self):
        if self.config.use_gpt_convolution:
            return Conv1D(self.config.n_embedding_units, self.config.n_feed_forward_units)
        else:
            return tfl.Dense(self.config.n_embedding_units)
    # endregion Utility Methods


class MultiHeadAttention(tfl.Layer):
    def __init__(self, config: XfmrConfig, **kwargs):
        super().__init__(**kwargs)

        if (
                config.n_attention_state_units % config.n_heads != 0
                and config.head_split_method == 'reshape'
        ):
            raise ValueError(
                f"The number of attention state units ({config.n_attention_state_units}) must be "
                f"evenly divisible by the number of heads ({config.n_heads})"
            )

        self.config = config

        self.qkv_expansion = Conv1D(config.n_attention_state_units * 3, config.n_embedding_units)
        self.activation = self._make_activation()
        self.projection = self._make_projection()
        self.attention_dropout = tfl.Dropout(config.attention_dropout_rate)
        self.residual_dropout = tfl.Dropout(config.residual_dropout_rate)

        if config.head_split_method == 'convolution':
            if config.use_gpt_convolution:
                self.head_convolution = Conv1D(
                    config.n_heads * config.n_attention_state_units,
                    config.n_attention_state_units
                )
            else:
                self.head_convolution = tfl.Dense(config.n_heads * config.n_attention_state_units)
        elif config.head_split_method == 'sample':
            self.sample_indices = self._make_sample_indices()

    def call(self, x, training: bool = False, **kwargs):
        # Make the query, key, and value matrices by multiplying the input by a set of weights.
        # The TensorFlow method multiplies the input multiple times with different weight matrices
        # for the query, key, and values, but the end result should be essentially the same
        # thing.
        x = self.qkv_expansion(x)

        # The query, key, and values are concatenated, so we split them out here.
        query, key, value = tf.split(x, 3, axis=2)
        query, key, value = self.split_heads(query), self.split_heads(key), self.split_heads(value)

        # Apply the attention calculations
        attn, attn_weights = self.attention(query, key, value, training=training)

        # Merge the heads back to a single vector
        attn = self.merge_heads(attn)

        # Project the attention values back into the expected model dimension.
        # The TensorFlow tutorial uses a standard dense layer for this, while the GPT code
        # uses their Conv1D layer. I might be missing something, but in this case I think
        # they do the same thing. They are both just a matrix multiplication with a bias.
        output = self.projection(attn)

        # Apply some dropout
        output = self.residual_dropout(output, training=training)

        return output, attn_weights

    # region Utility Methods
    def _make_activation(self):
        if self.config.attention_activation_function == 'identity':
            return tfl.Lambda(lambda x: x)
        else:
            return tf.keras.layers.Activation(self.config.attention_activation_function)

    def _make_projection(self):
        config = self.config

        if config.use_gpt_convolution:
            if config.head_split_method == 'reshape':
                return Conv1D(config.n_embedding_units, config.n_attention_state_units)
            elif config.head_split_method == 'convolution':
                return Conv1D(
                    config.n_embedding_units,
                    config.n_heads * config.n_attention_state_units
                )
            elif config.head_split_method == 'sample':
                return Conv1D(
                    config.n_embedding_units,
                    config.n_heads * config.head_sample_size
                )
            else:
                raise NotImplementedError(f"Unknown head_split_method: {config.head_split_method}")
        else:
            return tfl.Dense(self.config.n_embedding_units)

    def attention(self, query: tf.Tensor, key: tf.Tensor, value: tf.Tensor, training: bool):
        # The key, query, and value tensors should all have the same shape:
        # (batch_size, n_heads, seq_len, embedding_size // n_heads)
        w = tf.matmul(query, key, transpose_b=True)

        # w should have the shape (batch_size, n_heads, dst_seq_len, src_seq_len).
        # Where dst_seq_len and src_seq_len should be the same and information flows
        # from source to destination.
        _, _, dst_len, src_len = shape_list(w)

        if self.config.scale_attention:
            dk = tf.cast(shape_list(key)[-1], dtype=w.dtype)
            w = w / tf.math.sqrt(dk)

        mask = self.causal_attention_mask(dst_len, src_len, dtype=w.dtype)

        # This just reshapes the mask so it is broadcastable to w
        mask = tf.reshape(mask, [1, 1, dst_len, src_len])

        # The value of the masked weights after applying the softmax should be zero (or effectively
        # zero). Softmax is defined as e^x_i / sum e^x_j. So, to have values close to zero after
        # applying the softmax function we want to have x values that are large negative values.
        # In the TensorFlow tutorial they simply multiply the boolean mask by a large negative
        # value and add this to the attention logits, w, assuming the large negative value would
        # swamp whatever value the logit previously had. The GPT code first zeros out the masked
        # logits and then subtracts a large negative value.
        w = w * mask
        if self.config.attention_activation_function in ('softmax', 'sigmoid'):
            w = w - 1e4 * (1.0 - mask)

        # Now apply the softmax (or other activation)
        w = self.activation(w)

        # Apply some dropout
        w = self.attention_dropout(w, training)

        # Multiply the weights by the value vectors
        a = tf.matmul(w, value)

        return a, w

    @staticmethod
    def causal_attention_mask(nd: int, ns: int, dtype):
        """
        1's in the lower triangle, counting from the lower right corner.
        Same as tf.matrix_band_part(tf.ones([nd, ns]), -1, ns-nd), but doesn't produce garbage on
        TPUs.

        :param nd:
        :param ns:
        :param dtype:
        :return:
        """
        i = tf.range(nd)[:, None]
        j = tf.range(ns)
        m = i >= j - ns + nd
        return tf.cast(m, dtype)

    def split_heads(self, x):
        """
        Split the input into multiple heads (by reshaping it).

        :param x:
        :return:
        """
        method = self.config.head_split_method
        n_heads = self.config.n_heads
        x_shape = shape_list(x)
        batch_size, seq_len, n_features = x_shape

        if method == 'reshape':
            # Split the features into n_buckets by simply reshaping the tensor.
            # Each head will contain a subset of the original feature set.
            # This method requires the number of heads to be an exact multiple of the number
            # of features (i.e., the number of embedding units).
            new_x_shape = x_shape[:-1] + [n_heads, n_features // n_heads]
            x = tf.reshape(x, new_x_shape)
            x = tf.transpose(x, (0, 2, 1, 3))  # (batch, head, seq_length, head_features)
        elif method == 'convolution':
            # Expand the input tensor so that each head is some function of the input
            # features.
            # This method does not require the number of heads to be an exact multiple of the
            # feature size.
            x = self.head_convolution(x)
            new_x_shape = x_shape[:-1] + [n_heads, n_features]
            x = tf.reshape(x, new_x_shape)
            x = tf.transpose(x, (0, 2, 1, 3))  # (batch, head, seq_length, head_features)
        elif method == 'sample':
            sample_size = self.config.head_sample_size
            x = tf.gather(x, self.sample_indices, axis=-1)
            x = tf.reshape(x, (batch_size, seq_len, n_heads, sample_size))
            x = tf.transpose(x, perm=(0, 2, 1, 3))
        else:
            raise NotImplementedError(f"Unknown head_split_method: {method}")

        return x

    def _make_sample_indices(self):
        import math

        n_heads = self.config.n_heads
        sample_size = self.config.head_sample_size
        n_features = self.config.n_attention_state_units
        seed = self.config.head_sample_seed

        indices = []
        for i in range(math.ceil(n_heads * sample_size / n_features)):
            indices.append(tf.random.shuffle(tf.range(n_features, dtype=tf.int32), seed=seed))

        return tf.concat(indices, axis=0)[:n_heads*sample_size]

    @staticmethod
    def merge_heads(x):
        """
        Merge the heads back into a single input (by reshaping).

        :param x:
        :return:
        """
        x = tf.transpose(x, [0, 2, 1, 3])
        x_shape = shape_list(x)
        new_x_shape = x_shape[:-2] + [x_shape[-2] * x_shape[-1]]
        return tf.reshape(x, new_x_shape)
    # endregion Utility Methods


class DecoderBlock(tfl.Layer):
    def __init__(self, config: XfmrConfig, **kwargs):
        super().__init__(**kwargs)

        self.config = config

        self.norm = make_normalization_layers(
            2,
            config.normalization_method,
            config.layer_norm_epsilon
        )
        self.dropout = tfl.Dropout(config.decoder_dropout_rate)
        self.attention = MultiHeadAttention(config)
        self.simple_activation = (
            make_activation(config.activation_function)
            if config.decoder_update_method == 'simple'
            else None
        )
        self.mlp = Mlp(config)

        if config.decoder_update_method == 'gated':
            self.zw = [
                tfl.Dense(config.n_embedding_units, activation='sigmoid')
                for _ in range(2)
            ]

    # noinspection PyMethodOverriding
    def call(self, x, model_state: tf.Tensor, training: bool = False, **kwargs):
        # x should be of shape (batch_size, sequence_length, n_embedding_units)
        # Normalize the inputs (if configured)
        # This is not done in the TF tutorial, but seems like the right thing to do.
        a = self.norm[0](x)

        # Calculate the QKV attention values (and weights)
        attn, attn_weights = self.attention(a, training=training)

        # The TF model uses dropout here but the GPT model does not
        attn = self.dropout(attn, training=training)

        # If configured, add the attention output to the input.
        # This is done both in TF and in GPT, although it is never really explained
        # why this is beneficial.
        a, model_state = self._update(model_state, x, attn, 0)

        if self.config.decoder_update_method == 'simple':
            # I we don't use an additive update it might be necessary to use an activation function
            # between the layers to get traction in training. Without an activation the model does
            # not seem to be able to learn. However, it doesn't seem able to learn with one either.
            a = self.simple_activation(a)

        # (Possibly) normalize the result
        m = self.norm[1](a)

        # Apply a feed forward network
        m = self.mlp(m, training=training)

        # Update the state again using the output from the neural network
        a, model_state = self._update(model_state, a, m, 1)

        return a, attn_weights, model_state

    # region Utility Methods
    def _update(
            self,
            state: tf.Tensor,
            x: tf.Tensor,
            h: tf.Tensor,
            zi: int
    ) -> Tuple[tf.Tensor, tf.Tensor]:
        method = self.config.decoder_update_method

        if method == 'gated':
            # There are quite a few combinations/permutations of how this could work, but none
            # of them seem to work better than the additive method (or very well at all for that
            # matter).

            # An update method inspired by LSTM/GRU updates.
            # It is a bit simpler because there is no recursive hidden state and
            # the candidate update information is taken to be the previous operations
            # in the layer (e.g., the attention calculation or the application of the
            # feed forward network.
            z = self.zw[zi](tf.concat([x, h], -1))

            # noinspection PyTypeChecker
            # u = (1.0 - z) * state + z * h
            u = (1.0 - z) * x + z * h
            # The first item is just the normal GPT/TF update that can be used
            # for further processing if necessary (e.g., passing it through the
            # MLP).
            # The second item is the gated update.

            # return x + h, u
            # return h, u
            return x, u
        elif method == 'additive':
            return x + h, state
        elif method == 'simple':
            return h, state

        raise NotImplementedError(f"Unknown decoder update method: {method}")
    # endregion Utility Methods


class Xfmr(LanguageModel):
    def __init__(self, vocabulary: Dict[str, int], config: XfmrConfig, **kwargs):
        """
        Unlike the TensorFlow transformer or the GPT2 transformer, each sub layer of the network
        can have a different intermediate dimensionality. Note the dimensionality of the
        feedforward network in the TensorFlow model can have a different dimensionality, but not
        the attention layers. The GPT2 code hints at being able to have a different dimensionality
        in the attention layers, but it does not seem implemented. This flexibility is accomplished
        by applying a :class:`~delamo.layers.Conv1D` or :class:``tfl.Dense` to map the input (of
        each layer) into whatever dimension we'd like and to work with that dimension within the
        layer. Then projecting the resulting tensor back to the dimensionality expected by the
        next layer (currently the number of embedding units, although this should be configurable
        as well).

        :param vocabulary:
        :param config:
        :param kwargs:
        """
        super().__init__(config.sequence_length, vocabulary, config.dataset_method, **kwargs)

        self.config = config

        # The embedding layer used to encode words (or other token units)
        self.token_embedding = tfl.Embedding(self.vocab_size, config.n_embedding_units)

        # The embedding layer used to encode positional information. The additive combination of
        # this layer plus the token embedding will be the input embedding to the network. Using a
        # trainable embedding layer for the positional information is similar to how GPT-2 encodes
        # it and is different than the original transformer based model described by the
        # TensorFlow tutorial.
        self.positional_embedding = tfl.Embedding(config.sequence_length, config.n_embedding_units)

        # The amount of dropout to apply to the input embedding
        self.input_dropout = tfl.Dropout(config.embedding_dropout_rate)

        # The main layers of the transformer
        self.decoder_blocks = [DecoderBlock(config) for _ in range(config.n_decoder_layers)]

        # During training we may optionally want to make and output predictions from each
        # decoder layer (not just the final layer). This was shown to help training and increase
        # the depth of the networkIn the paper "Character-Level Language Modeling with Deeper
        # Self-Attention".

        # Used to normalize the hidden states before applying the final dense
        # layer used for making predictions.
        self.output_norm = make_normalization_layers(
            config.n_decoder_layers if config.output_predictions_per_layer else 1,
            config.normalization_method,
            config.layer_norm_epsilon
        )

        # The dense layers used to generate the prediction logits
        self.prediction_layer = self._make_prediction_layers()

        # Keep track of how many steps we've trained for.
        self.step_num = 1

    @classmethod
    def prepare_model(
            cls,
            args: argparse.Namespace,
            vocab: Dict[str, int],
            **kwargs
    ) -> LanguageModel:
        config = XfmrConfig(
            dataset_method=args.dataset_method,
            sequence_length=args.sequence_length,
            n_embedding_units=args.n_embedding_units,
            embedding_dropout_rate=args.embedding_dropout_rate,
            attention_dropout_rate=args.attention_dropout_rate,
            residual_dropout_rate=args.residual_dropout_rate,
            decoder_dropout_rate=args.decoder_dropout_rate,
            layer_norm_epsilon=args.layer_norm_epsilon,
            n_decoder_layers=args.n_decoder_layers,
            n_feed_forward_layers=args.n_feed_forward_layers,
            n_feed_forward_units=args.n_feed_forward_units,
            n_heads=args.n_heads,
            attention_activation_function=args.attention_activation_function,
            head_split_method=args.head_split_method,
            head_sample_size=args.head_sample_size,
            head_sample_seed=args.head_sample_seed,
            normalization_method=args.normalization_method,
            output_predictions_per_layer=args.output_predictions_per_layer,
            decoder_update_method=args.decoder_update_method,
            scale_attention=args.scale_attention,
            activation_function=args.activation_function,
            use_gpt_convolution=args.use_gpt_convolution,
            auxiliary_loss_method=args.auxiliary_loss_method,
            loss_weight_n=args.loss_weight_n,
            loss_weight_lambda=args.loss_weight_lambda,
            loss_weight_normalized=args.loss_weight_normalized,
            optimizer_params=make_optimizer_params_from_args(args)

        )

        model = cls(vocab, config)
        model = model.compile_model()

        return model

    def get_config(self) -> Dict[str, Any]:
        config = super().get_config()
        config.update({
            'config': self.config
        })

        return config

    def call(
            self,
            token_ids,
            training: bool = False,
            **kwargs
    ):
        # The shape should be (1, sequence_length)
        position_ids = tf.range(0, self.config.sequence_length, dtype=tf.int32)[tf.newaxis, :]

        # Get the token embeddings
        token_embedding = self.token_embedding(token_ids)

        # Get the positional embeddings
        positional_embedding = self.positional_embedding(position_ids)

        # Compose the two embeddings
        # The addition should broadcast the addition of the positional embeddings to each token
        # embedding.
        input_embedding = token_embedding + positional_embedding

        # Apply the embedding dropout (if training)
        hidden_states = self.input_dropout(input_embedding, training=training)

        # Some lists to store the results
        output_attn_weights = []
        output_logits = []

        # Convenient variable
        n_layers = len(self.decoder_blocks)

        # Initialize the model state to be the full input embedding without dropout.
        # This is only needed if the decoder block is configured to use the gated update method.
        model_state = input_embedding
        for i, block in enumerate(self.decoder_blocks):
            # The hidden states need to maintain the same dimensionality as the
            # input embedding layer.
            hidden_states, attn_weights, model_state = block(
                hidden_states,
                model_state,
                training=training
            )

            if self.config.output_predictions_per_layer and i < n_layers - 1:
                # Note dropout has already been performed in the decoder block
                # but layer normalization has not.
                if self.config.decoder_update_method == 'gated':
                    # logits = self.output_norm[i](tf.concat([hidden_states, model_state], -1))
                    logits = self.output_norm[i](model_state)
                else:
                    # Apply the normalization (if one is configured)
                    logits = self.output_norm[i](hidden_states)

                # The expected shape for the input to the prediction layer should be
                # (batch_size, sequence_length, n_embedding_units)
                logits = self.prediction_layer[i](logits)

                output_logits.append(logits)

            output_attn_weights.append(attn_weights)

        # Always store the final output logits
        decoder_update_method = self.config.decoder_update_method
        # final_inputs = (
        #     tf.concat([hidden_states, model_state], -1) if decoder_update_method == 'gated' else
        #     hidden_states
        # )
        final_inputs = model_state if decoder_update_method == 'gated' else hidden_states

        logits = self.output_norm[-1](final_inputs)
        logits = self.prediction_layer[-1](logits)

        output_logits.append(logits)

        return {
            'output_logits': output_logits[-1],
            'attention_weights': output_attn_weights[-1],
            'all_logits': output_logits,
            'all_attention_weights': output_attn_weights
        }

    def train_step(self, data):
        x, y_true = data

        auxiliary_loss_method = self.config.auxiliary_loss_method

        with tf.GradientTape() as tape:
            # Note: This model does not have the functionality to use an approximate softmax
            # so the `y` values are not needed. The `lookahead_mask` is also not needed because it
            # is created on demand in the attention layer (with the name causal_mask).

            result = self(x, training=True)
            y_pred = result['output_logits']

            if auxiliary_loss_method == 'depth_weighted':
                y_pred_list = result['all_logits']
                if y_pred_list is None:
                    raise ValueError(
                        "You must set output_predictions_per_layer when using the depth_weighted "
                        "loss method"
                    )
                n_layers = self.config.n_decoder_layers

                # The contribution of each loss will be weighted based on the depth of that
                # layer and decay exponentially as training progresses. A*exp(-a*t)

                # Calculate the loss for each intermediate layer
                losses = tf.stack(
                    [self.compiled_loss(y_true, y_pred_lyr) for y_pred_lyr in y_pred_list],
                    axis=0
                )

                # The base weights represent A.
                # The base weights decrease as the layer depth increases.
                # (note the weights are in reverse order at the moment)
                base_weights = (1.0
                                / (tf.range(1, n_layers + 1, dtype=tf.float32)
                                   * self.config.loss_weight_n))

                # Compute the full exponential decay for each layer
                weights = base_weights * tf.exp(-self.config.loss_weight_lambda * self.step_num)

                # The top layer should never decay
                weights = tf.tensor_scatter_nd_update(weights, [[0]], [1.0])

                if self.config.loss_weight_normalized is True:
                    # Normalize the weights so the sum of the losses is comparable across steps and
                    # epochs
                    weights = weights / tf.reduce_sum(weights)

                # Reverse the weights (put the top output layer at position 0 and the deepest
                # hidden layer at the back).
                weights = tf.reverse(weights, axis=[0])
                loss = tf.math.reduce_sum(weights * losses)
            elif auxiliary_loss_method in (None, 'none'):
                # noinspection PyUnusedLocal
                weights = tf.constant([1.0])
                loss = self.compiled_loss(y_true, y_pred)
            else:
                raise ValueError(f"Unknown auxiliary loss method: {auxiliary_loss_method}")

        # For custom training steps, users can just write:
        trainable_variables = self.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, trainable_variables))

        # Update the step number
        self.step_num += 1

        # Do not add the metrics for the training steps. They are kind of
        # interesting to track, but are slow to compute.
        result = {'loss': loss}

        # Unless you are running eagerly this will only print out the weights for the first
        # step, so it's not particularly useful and can be misleading if not aware of the
        # constraints.
        # for i, w in enumerate(tf.unstack(weights)):
        #     result[f'loss_weight_{i:02d}'] = w

        return result

    def test_step(self, data):
        x, y_true = data
        output = self(x, training=False)
        y_pred = output['output_logits']
        loss = self.compiled_loss(y_true, y_pred)
        self.compiled_metrics.update_state(y_true, y_pred)

        result = {m.name: m.result() for m in self.metrics}
        result['loss'] = loss

        return result

    def estimate_memory_usage(self, parameters: Dict[str, Any], vocab_size: int) -> int:
        # seq_len = parameters['sequence_length']
        # n_model_units = parameters['n_model_units']
        # n_mlp_units = parameters['n_embedding_units']

        raise NotImplementedError()

    # region Utility Methods
    def _make_prediction_layers(self):
        n = self.config.n_decoder_layers if self.config.output_predictions_per_layer else 1

        return [tfl.Dense(self.vocab_size, activation='linear') for _ in range(n)]
    # endregion Utility Methods
