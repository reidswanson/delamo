# ##############################################################################
#  Copyright 2020 Google Developers, Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Most of this code is lightly modified and restructured from the
# transformer tutorial on the TensorFlow website.
# See: https://www.tensorflow.org/tutorials/text/transformer

"""
Transformer based models.
"""

# Python Modules
import argparse
import logging
import math

from typing import Dict, Tuple, Optional, Any

# 3rd Party Modules
import numpy as np
import tensorflow as tf
import tensorflow.keras.layers as tfl

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
from delamo.cli.options.optimizer import make_optimizer_params_from_args
from delamo.models.core import LanguageModel
from delamo.utils.math import factors

log = logging.getLogger(__name__)


class PositionEncoding(tfl.Layer):
    def __init__(self, position: int, n_model_dim: int, dtype=tf.float32):
        """
        A layer for adding the positional information to an input tensor.

        :param position:
        :param n_model_dim:
        :param dtype:
        """
        super().__init__()

        self.position = position
        self.n_model_dim = n_model_dim

        angle_rads = self._get_angles(
            tf.expand_dims(tf.range(position, dtype=dtype), axis=1),
            tf.expand_dims(tf.range(n_model_dim, dtype=dtype), axis=0),
            n_model_dim,
            dtype
        )

        even = tf.reshape(tf.range(0, tf.shape(angle_rads)[1], 2), (-1, 1))
        odd = tf.reshape(tf.range(1, tf.shape(angle_rads)[1], 2), (-1, 1))

        # Transposing makes the gather/scattering easier
        angle_rads = tf.transpose(angle_rads)
        even_rads = tf.math.sin(tf.gather_nd(angle_rads, even))
        odd_rads = tf.math.cos(tf.gather_nd(angle_rads, odd))

        angle_rads = tf.tensor_scatter_nd_update(angle_rads, even, even_rads)
        angle_rads = tf.tensor_scatter_nd_update(angle_rads, odd, odd_rads)
        angle_rads = tf.transpose(angle_rads)

        pos_encoding = tf.expand_dims(angle_rads, axis=0)

        self.pos_encoding = tf.cast(pos_encoding, dtype=dtype)

    def get_config(self):
        config = super().get_config()
        config.update({
            'position': self.position,
            'n_model_dim': self.n_model_dim
        })

        return config

    def __call__(self, x, *args, **kwargs):
        seq_len = tf.shape(x)[1]

        return x + self.pos_encoding[:, seq_len, :]

    @classmethod
    def _get_angles(cls, pos, i, n_model_dim, dtype):
        exponent = (2.0 * (i//2.0)) / tf.cast(n_model_dim, dtype)
        angle_rates = 1.0 / tf.pow(10000.0, exponent)

        return pos * angle_rates


class MultiHeadAttention(tfl.Layer):
    def __init__(self, n_model_dim: int, n_heads: int):
        """
        See: https://www.tensorflow.org/tutorials/text/transformer#multi-head_attention

        :param n_model_dim: Number of dimensions for the core model.
               The value must be an exact multiple of the number of heads.
        :param n_heads: Number of heads
        """
        super().__init__()

        # Validate the inputs
        if n_model_dim % n_heads != 0:
            raise ValueError(
                f"The model dimension ({n_model_dim}) must be an exact "
                f"multiple of the number of heads ({n_heads})."
            )

        self.n_model_dim = n_model_dim
        self.n_heads = n_heads
        self.depth = n_model_dim // n_heads

        # Layers to store the query, key, and value weights
        self.wq = tfl.Dense(n_model_dim)
        self.wk = tfl.Dense(n_model_dim)
        self.wv = tfl.Dense(n_model_dim)

        # Output layer
        self.dense = tfl.Dense(n_model_dim)

    def get_config(self):
        config = super().get_config()
        config.update({
            'n_model_dim': self.n_model_dim,
            'n_heads': self.n_heads
        })

        return config

    # noinspection PyMethodOverriding
    def call(self, v, k, q, mask, **kwargs):
        batch_size = tf.shape(q)[0]

        q = self.wq(q)  # (batch_size, seq_len, n_model_dim)
        k = self.wk(k)  # (batch_size, seq_len, n_model_dim)
        v = self.wv(v)  # (batch_size, seq_len, n_model_dim)

        q = self.split_heads(q, batch_size)  # (batch_size, num_heads, seq_len_q, depth)
        k = self.split_heads(k, batch_size)  # (batch_size, num_heads, seq_len_k, depth)
        v = self.split_heads(v, batch_size)  # (batch_size, num_heads, seq_len_v, depth)

        # scaled_attention.shape == (batch_size, num_heads, seq_len_q, depth)
        # attention_weights.shape == (batch_size, num_heads, seq_len_q, seq_len_k)
        scaled_attention, attention_weights = self.sdp_attention(q, k, v, mask)

        # (batch_size, seq_len_q, num_heads, depth)
        scaled_attention = tf.transpose(scaled_attention, perm=[0, 2, 1, 3])

        # (batch_size, seq_len_q, n_model_dim)
        concat_attention = tf.reshape(
            scaled_attention,
            (batch_size, -1, self.n_model_dim)
        )

        # (batch_size, seq_len_q, n_model_dim)
        output = self.dense(concat_attention)

        return output, attention_weights

    def split_heads(self, x, batch_size):
        """
        Split the last dimension into (num_heads, depth).
        Transpose the result such that the shape is (batch_size, num_heads, seq_len, depth)
        """
        x = tf.reshape(x, (batch_size, -1, self.n_heads, self.depth))

        # Reorder the dimensions
        return tf.transpose(x, perm=[0, 2, 1, 3])

    @classmethod
    def sdp_attention(cls, q, k, v, mask):
        """
        Calculate the (scaled dot product) attention weights.
        q, k, v must have matching leading dimensions.
        k, v must have matching penultimate dimension, i.e.: seq_len_k = seq_len_v.
        The mask has different shapes depending on its type(padding or look ahead)
        but it must be broadcastable for addition.

        See: https://www.tensorflow.org/tutorials/text/transformer#scaled_dot_product_attention

        :param q: query shape == (..., seq_len_q, depth)
        :param k: key shape == (..., seq_len_k, depth)
        :param v: value shape == (..., seq_len_v, depth_v)
        :param mask: Float tensor with shape broadcastable
               to (..., seq_len_q, seq_len_k). Defaults to None.
        :return: output, attention_weights
        """
        # (..., seq_len_q, seq_len_k)
        matmul_qk = tf.matmul(q, k, transpose_b=True)

        # scale matmul_qk
        dk = tf.cast(tf.shape(k)[-1], k.dtype)
        scaled_attention_logits = matmul_qk / tf.math.sqrt(dk)

        # add the mask to the scaled tensor.
        if mask is not None:
            scaled_attention_logits += (mask * -1e9)

        # softmax is normalized on the last axis (seq_len_k) so that the scores
        # add up to 1. # (..., seq_len_q, seq_len_k)
        attention_weights = tf.nn.softmax(scaled_attention_logits, axis=-1)

        # (..., seq_len_q, depth_v)
        output = tf.matmul(attention_weights, v)

        return output, attention_weights


class DecoderLayer(tfl.Layer):
    def __init__(
            self,
            n_model_dim: int,
            n_heads: int,
            n_ff_dim: int,
            dropout_rate: float = 0.1
    ):
        """
        See: https://www.tensorflow.org/tutorials/text/transformer#decoder_layer

        :param n_model_dim: Number of dimensions for the core model
        :param n_heads: Number of heads
        :param dropout_rate: The dropout rate
        """
        super().__init__()

        self.n_model_dim = n_model_dim
        self.n_heads = n_heads
        self.n_ff_dim = n_ff_dim
        self.dropout_rate = dropout_rate

        # Attention
        self.mha = [MultiHeadAttention(n_model_dim, n_heads) for _ in range(1)]

        # Feed Forward
        self.ffn = self._make_feed_forward_layers(n_model_dim, n_ff_dim)

        # Normalization
        self.layer_norm = [tfl.LayerNormalization(epsilon=1e-6) for _ in range(2)]

        # Dropout
        self.dropout = [tfl.Dropout(dropout_rate) for _ in range(2)]

    def get_config(self):
        config = super().get_config()
        config.update({
            'n_model_dim': self.n_model_dim,
            'n_heads': self.n_heads,
            'n_ff_dim': self.n_ff_dim,
            'dropout_rate': self.dropout_rate,
        })

        return config

    # noinspection PyMethodOverriding
    def call(self, x, training, look_ahead_mask):
        # enc_output.shape == (batch_size, input_seq_len, n_model_dim)

        # (batch_size, target_seq_len, n_model_dim)
        attn, attn_weights_block = self.mha[0](x, x, x, look_ahead_mask)
        attn = self.dropout[0](attn, training=training)
        out = self.layer_norm[0](attn + x)

        # There isn't an encoder so we don't need the other layers used
        # in the transformer tutorial.

        # (batch_size, target_seq_len, n_model_dim)
        ffn_output = self.ffn(out)
        ffn_output = self.dropout[1](ffn_output, training=training)
        result = self.layer_norm[1](ffn_output + out)  # (batch_size, target_seq_len, n_model_dim)

        return result, attn_weights_block

    @classmethod
    def _make_feed_forward_layers(cls, n_model_dim, n_ff_dim):
        return tf.keras.Sequential([
            # (batch_size, seq_len, dff)
            tfl.Dense(n_ff_dim, activation='relu'),

            # (batch_size, seq_len, n_model_dim)
            tfl.Dense(n_model_dim)
        ])


class Decoder(tfl.Layer):
    def __init__(
            self,
            n_layers: int,
            n_model_dim: int,
            n_heads: int,
            n_ff_dim: int,
            vocab_size: int,
            max_position_encoding: int,
            dropout_rate: float = 0.1
    ):
        """
        See: https://www.tensorflow.org/tutorials/text/transformer#decoder

        :param n_layers: The number of layers.
        :param n_model_dim: The number of units in the base "model" layers.
               This must be an exact multiple of ``n_heads``.
        :param n_heads: The number of heads.
        :param n_ff_dim: The number of units in the feed forward layers.
        :param vocab_size: The number of words in the vocabulary including
               the start, end, and padding tokens (typically 0).
        :param max_position_encoding: The maximum length of a sequence.
        :param dropout_rate: The dropout rate.
        """
        super().__init__()

        self.n_layers = n_layers
        self.n_model_dim = n_model_dim
        self.n_heads = n_heads
        self.n_ff_dim = n_ff_dim
        self.vocab_size = vocab_size
        self.max_position_encoding = max_position_encoding
        self.dropout_rate = dropout_rate

        self.embedding = tfl.Embedding(vocab_size, n_model_dim)
        self.pos_encoding = PositionEncoding(
            max_position_encoding,
            n_model_dim,
            self.embedding.dtype
        )

        self.decoder_layers = [
            DecoderLayer(n_model_dim, n_heads, n_ff_dim, dropout_rate)
            for _ in range(n_layers)
        ]
        self.dropout = tfl.Dropout(dropout_rate)

    def get_config(self):
        config = super().get_config()
        config.update({
            'n_layers': self.n_layers,
            'n_model_dim': self.n_model_dim,
            'n_heads': self.n_heads,
            'n_ff_dim': self.n_ff_dim,
            'vocab_size': self.vocab_size,
            'max_position_encoding': self.max_position_encoding,
            'dropout_rate': self.dropout_rate,
        })

        return config

    # noinspection PyMethodOverriding
    def call(
            self,
            x: tf.Tensor,
            training: bool,
            lookahead_mask: tf.Tensor,
            **kwargs
    ):
        attention_weights = {}

        # (batch_size, target_seq_len, n_model_dim)
        x = self.embedding(x)
        x *= tf.math.sqrt(tf.cast(self.n_model_dim, x.dtype))
        x = self.pos_encoding(x)

        x = self.dropout(x, training=training)

        for i in range(self.n_layers):
            x, block_1 = self.decoder_layers[i](x, training, lookahead_mask)
            attention_weights[f'decoder_layer{i+1}_block_1'] = block_1

        return x, attention_weights

    @classmethod
    def make_positional_encoding(cls, position: int, n_model_dim):
        """
        Create a tensor that can add positional information to the input
        tensor.

        See: https://www.tensorflow.org/tutorials/text/transformer#positional_encoding

        :param position:
        :param n_model_dim:
        :return:
        """
        angle_rads = cls._get_angles(
            np.arange(position)[:, np.newaxis],
            np.arange(n_model_dim)[np.newaxis, :],
            n_model_dim
        )

        # apply sin to even indices in the array; 2i
        angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])

        log.debug(f"angle_rads:\n{angle_rads}")
        # apply cos to odd indices in the array; 2i+1
        angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])

        # log.debug(f"odd_rads:\n{angle_rads[:, 1::2]}")

        pos_encoding = angle_rads[np.newaxis, ...]

        return tf.cast(pos_encoding, dtype=tf.float32)

    @classmethod
    def _get_angles(cls, pos, i, n_model_dim):
        exponent = (2 * (i//2)) / np.float32(n_model_dim)
        angle_rates = 1 / np.power(10000, exponent)
        return pos * angle_rates


class DecoderOnlyLanguageModel(LanguageModel):
    def __init__(
            self,
            n_layers: int,
            n_model_dim: int,
            n_heads: int,
            n_ff_dim: int,
            sequence_length: int,
            vocabulary: Dict[str, int],
            dataset_method: str,
            dropout: float,
            n_softmax_samples: int = 5000,
            optimizer_params: Optional[Dict[str, Any]] = None,
            **kwargs
    ):
        """
        A model using (only) the decoding layers of a Transformer
        that can be used to implement language models.

        :param n_layers: The number of :class:`DecoderLayer`s.
        :param n_model_dim: The number of units in the base model.
        :param n_heads: The number of multi-attention heads.
        :param n_ff_dim: The number of units in the feed forward network.
        :param vocabulary: A dictionary mapping strings (words) to their
               integer values.
        :para seq_len: The length of a sequence.
        :param kwargs: Not used.
        """
        super().__init__(
            sequence_length,
            vocabulary,
            dataset_method,
            n_softmax_samples,
            optimizer_params,
            **kwargs
        )

        # Ensure the number of heads is a multiple of the model_dim
        if n_model_dim % n_heads != 0:
            old_n_heads = n_heads
            min_difference = math.inf
            for factor in factors(n_model_dim):
                difference = abs(old_n_heads - factor)
                if difference < min_difference:
                    min_difference = difference
                    n_heads = factor

            log.warning(
                f"The model dimension ({n_model_dim}) is not an exact multiple of the number of "
                f"heads ({old_n_heads}). Using {n_heads} n_heads instead."
            )

        self.n_layers = n_layers
        self.n_model_dim = n_model_dim
        self.n_heads = n_heads
        self.n_ff_dim = n_ff_dim
        self.vocabulary = vocabulary
        self.pos_encoding = self.vocab_size
        self.dropout = dropout

        self.decoder = Decoder(
            n_layers,
            n_model_dim,
            n_heads,
            n_ff_dim,
            self.vocab_size,
            self.pos_encoding,
            self.dropout
        )

        self.final_layer = tfl.Dense(self.vocab_size)

    @classmethod
    def prepare_model(cls, args: argparse.Namespace, py_vocab: Dict[str, int]) -> LanguageModel:
        """

        :param args:
        :param py_vocab:
        :return:
        """
        model = cls(
            args.n_layers,
            args.n_model_units,
            args.n_heads,
            args.n_feed_forward_units,
            args.sequence_length,
            py_vocab,
            args.dataset_method,
            args.dropout,
            args.n_softmax_samples,
            make_optimizer_params_from_args(args)
        )

        model.compile_model()

        return model

    def get_config(self):
        config = super().get_config()
        config.update({
            'n_layers': self.n_layers,
            'n_model_dim': self.n_model_dim,
            'n_heads': self.n_heads,
            'n_ff_dim': self.n_ff_dim,
            'dropout': self.dropout
        })

        return config

    # noinspection PyMethodOverriding
    def call(self, inputs: Tuple[tf.Tensor, tf.Tensor], training: bool):
        """

        :param inputs: The input data and lookahead mask as a tuple.
        :param training: ``True`` if we are training.
        :return: A tuple containing the final output and the attention
                 weights.
        """
        (x, y), lookahead_mask = inputs
        output, attention_weights = self.decoder(x, training, lookahead_mask)

        final_output = self.final_layer(output)

        if y is not None and self.n_softmax_samples > 0:
            self.add_sampled_softmax_loss(output, y, self.final_layer.weights)

        return final_output, attention_weights

    def train_step(self, data):
        x, y_true = data

        x = self._prepare_input(x, y_true)

        with tf.GradientTape() as tape:
            y_pred, _ = self(x, True)

            if self.n_softmax_samples > 0:
                loss = tf.reduce_mean(self.losses)
            else:
                loss = self.compiled_loss(y_true, y_pred)

        # For custom training steps, users can just write:
        trainable_variables = self.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, trainable_variables))

        # Do not add the metrics for the training steps. They are kind of
        # interesting to track, but are slow to compute.
        result = {'loss': loss}

        return result

    def test_step(self, data):
        x, y_true = data

        x = self._prepare_input(x, y_true)
        y_pred, _ = self(x, False)

        if self.n_softmax_samples > 0:
            loss = sum(self.losses)
        else:
            loss = self.compiled_loss(y_true, y_pred)

        self.compiled_metrics.update_state(y_true, y_pred)

        result = {m.name: m.result() for m in self.metrics}
        result['loss'] = loss

        return result

    @classmethod
    def make_padding_mask(cls, seq: tf.Tensor):
        """
        Mask all the pad tokens in the batch of sequence. It ensures that the
        model does not treat padding as the input. The mask indicates where pad
        value 0 is present: it outputs a 1 at those locations, and a 0 otherwise.

        :param seq:
        :return:
        """
        seq = tf.cast(tf.math.equal(seq, 0), tf.float32)

        # add extra dimensions to add the padding
        # to the attention logits.
        # (batch_size, 1, 1, seq_len)
        return seq[:, tf.newaxis, tf.newaxis, :]

    @classmethod
    def make_lookahead_mask(cls, size: int):
        """
       Create a mask that filters elements that have not been seen yet.
       Namely if there are ``size`` inputs (e.g., sequence length) then
       when examining the first element (corresponding to the first row of the
       mask), the first column will be 0 and all other columns in the row will
       be 1. When examining the second element (the second row), then the first
       two columns will be 0 and all others 1.

       :param size: The size of the mask (i.e., the sequence length)
       :return: The mask of shape (``size``, ``size``).
       """
        # https://www.tensorflow.org/api_docs/python/tf/linalg/band_part
        # This will create an upper triangular matrix of shape (size, size)
        # that is filled with 1s.
        mask = 1 - tf.linalg.band_part(tf.ones((size, size)), -1, 0)

        return mask  # (seq_len, seq_len)

    def estimate_memory_usage(self, parameters: Dict[str, Any], vocab_size: int) -> int:
        seq_len = parameters['sequence_length']
        n_model_dim = parameters['n_model_dim']
        n_ff_dim = parameters['n_ff_dim']

        # The policy name should tell us how many bytes are needed for the operations
        policy_name = mixed_precision.global_policy().name

        # 2 bytes for 16 bit, 4 bytes for 32 and 8 bytes for 64
        dtype_size = 2 if '16' in policy_name else 4 if '32' in policy_name else 8

        # It is not clear how to get a good estimate of the amount of memory
        # TensorFlow will use for the model. I've tried adding up the matrix
        # sizes for each of the layers, but that does not lead to a very
        # accurate estimate.
        # According to the original paper (Attention is All You Need) the
        # complexity per layer is O(seq_len * seq_len * model_dim). So this
        # should be a rough starting point, although it is not clear what the
        # constant factor is.
        # This blog post also provides some detail.
        # https://medium.com/analytics-vidhya/transformer-vs-rnn-and-cnn-18eeefa3602b

        # When profiling, it appears that the calculations in the final output
        # layer are the dominant factor in them memory usage, but this may
        # only be true for certain hyper-parameters (e.g., small model_dim
        # and/or sequence_length).
        final_params = seq_len * vocab_size

        # Although the paper suggests it's the sequence_length and
        # model dimension that is important, if the feed forward dimension
        # is much larger than the model dimension the estimate is too low.
        decoder_params = (seq_len * seq_len) * max(n_model_dim, n_ff_dim)

        # When the vocabulary is big, I suspect the final_params dominates,
        # and when the vocabulary is small the decoder_params dominate.
        # I have not investigated thoroughly, but it appears the memory usage
        # estimate is pretty good when the vocabulary is small (around 10k), but
        # starts overestimated the memory usage as the vocabulary size grows.
        mem_usage = max(decoder_params, final_params) * dtype_size

        log.debug(f"Estimated memory for final layer: {final_params * dtype_size}")
        log.debug(f"Estimated memory for the decoder: {decoder_params * dtype_size}")
        log.debug(f"Estimated memory: {mem_usage}")

        return mem_usage

    def _prepare_input(self, x: tf.Tensor, y: Optional[tf.Tensor] = None):
        return (x, y), self.make_lookahead_mask(tf.shape(x)[1])
