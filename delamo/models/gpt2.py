# ##############################################################################
#  Copyright 2018 The OpenAI Team Authors and HuggingFace Inc. team.
#  Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import argparse
import logging

from typing import Dict, Any, Optional

# 3rd Party Modules
import tensorflow as tf
import tensorflow.keras.layers as tfl

# Project Modules
from delamo.activations import get_activation
from delamo.cli.options.optimizer import make_optimizer_params_from_args
from delamo.layers import Conv1D
from delamo.models.core import LanguageModel
from delamo.utils.modeling import shape_list

log = logging.getLogger(__name__)


class Gpt2Config(object):
    def __init__(
            self,
            dataset_method: str = 'slice',
            sequence_length: int = 200,
            n_layers: int = 8,
            n_heads: int = 8,
            n_embedding_units: int = 512,
            n_model_units: int = 2048,
            embedding_dropout_rate: float = 0.3,
            attention_dropout_rate: float = 0.3,
            residual_dropout_rate: float = 0.3,
            layer_norm_epsilon: float = 1e-9,
            scale: bool = False,
            use_cache: bool = False,
            output_attentions: bool = False,
            auxiliary_loss_method: str = None,
            loss_weight_n: float = 1.0,
            loss_weight_lambda: float = 1e-4,
            loss_weight_normalized: bool = True,
            optimizer_params: Optional[Dict[str, Any]] = None
    ):
        super().__init__()

        self.dataset_method = dataset_method
        self.sequence_length = sequence_length
        self.n_layers = n_layers
        self.n_heads = n_heads
        self.n_embedding_units = n_embedding_units
        self.n_model_units = n_model_units
        self.embedding_dropout_rate = embedding_dropout_rate
        self.attention_dropout_rate = attention_dropout_rate
        self.residual_dropout_rate = residual_dropout_rate
        self.layer_norm_epsilon = layer_norm_epsilon
        self.scale = scale
        self.use_cache = use_cache
        self.output_attentions = output_attentions
        self.auxiliary_loss_method = auxiliary_loss_method
        self.loss_weight_n = loss_weight_n
        self.loss_weight_lambda = loss_weight_lambda
        self.loss_weight_normalized = loss_weight_normalized
        self.optimizer_params = optimizer_params


class Attention(tfl.Layer):
    def __init__(self, config: Gpt2Config, **kwargs):
        super().__init__(**kwargs)

        if config.n_embedding_units % config.n_heads != 0:
            raise ValueError(
                f"The number of embedding units must be evenly divisible by the number of heads"
            )

        self.n_embedding_units = config.n_embedding_units
        self.n_state = config.n_embedding_units
        self.n_heads = config.n_heads
        self.split_size = self.n_state
        self.attention_dropout_rate = config.attention_dropout_rate
        self.residual_dropout_rate = config.residual_dropout_rate
        self.scale = config.scale
        self.output_attentions = config.output_attentions

        self.c_attention = Conv1D(self.n_state * 3, self.n_embedding_units)
        self.c_projection = Conv1D(self.n_state, self.n_embedding_units)
        self.attention_dropout = tfl.Dropout(self.attention_dropout_rate)
        self.residual_dropout = tfl.Dropout(self.residual_dropout_rate)

        self.pruned_heads = set()

    def split_heads(self, x):
        x_shape = shape_list(x)
        new_x_shape = x_shape[:-1] + [self.n_heads, x_shape[-1] // self.n_heads]
        x = tf.reshape(x, new_x_shape)
        return tf.transpose(x, (0, 2, 1, 3))  # (batch, head, seq_length, head_features)

    @classmethod
    def merge_heads(cls, x):
        x = tf.transpose(x, [0, 2, 1, 3])
        x_shape = shape_list(x)
        new_x_shape = x_shape[:-2] + [x_shape[-2] * x_shape[-1]]
        return tf.reshape(x, new_x_shape)

    # noinspection PyMethodOverriding
    def call(
            self,
            x,
            layer_past,
            attention_mask,
            head_mask,
            use_cache,
            output_attentions,
            training: bool = False,
    ):
        x = self.c_attention(x)
        query, key, value = tf.split(x, 3, axis=2)
        query, key, value = self.split_heads(query), self.split_heads(key), self.split_heads(value)

        if layer_past is not None:
            past_key, past_value = tf.unstack(layer_past, axis=0)
            key = tf.concat([past_key, key], axis=-2)
            value = tf.concat([past_value, value], axis=-2)

        # To cope with Keras serialization
        if use_cache is True:
            present = tf.stack([key, value], axis=0)
        else:
            present = (None,)

        attention_outputs = self._attention(
            query,
            key,
            value,
            attention_mask,
            head_mask,
            output_attentions,
            training=training
        )

        a = attention_outputs[0]

        a = self.merge_heads(a)
        a = self.c_projection(a)
        a = self.residual_dropout(a, training=training)

        outputs = [a, present] + attention_outputs[1:]

        return outputs

    # region Utility Methods
    @staticmethod
    def _causal_attention_mask(nd: int, ns: int, dtype):
        """
        1's in the lower triangle, counting from the lower right corner.
        Same as tf.matrix_band_part(tf.ones([nd, ns]), -1, ns-nd), but doesn't produce garbage on
        TPUs.

        :param nd:
        :param ns:
        :param dtype:
        :return:
        """
        i = tf.range(nd)[:, None]
        j = tf.range(ns)
        m = i >= j - ns + nd
        return tf.cast(m, dtype)

    def _attention(
            self,
            q: tf.Tensor,
            k: tf.Tensor,
            v: tf.Tensor,
            attention_mask: tf.Tensor,
            head_mask: tf.Tensor,
            output_attentions: bool,
            training: bool = False
    ):
        # q, k, v have shape [batch, n_heads, sequence_length, features]
        w = tf.matmul(q, k, transpose_b=True)
        if self.scale:
            dk = tf.cast(shape_list(k)[-1], tf.float32)  # scale attention_scores
            w = w / tf.math.sqrt(dk)

        # w has shape [batch, n_heads, dst_sequence_length, src_sequence_length],
        # where information flows from src to dst.
        _, _, nd, ns = shape_list(w)

        b = self._causal_attention_mask(nd, ns, dtype=w.dtype)
        b = tf.reshape(b, [1, 1, nd, ns])
        w = w * b - 1e4 * (1 - b)

        if attention_mask is not None:
            # Apply the attention mask
            # noinspection PyTypeChecker
            w = w + attention_mask

        w = tf.nn.softmax(w, axis=-1)
        w = self.attention_dropout(w, training=training)

        # Mask heads if we want to
        if head_mask is not None:
            w = w * head_mask

        outputs = [tf.matmul(w, v)]
        if output_attentions:
            outputs.append(w)

        return outputs

    # endregion Utility Methods


class Mlp(tf.keras.layers.Layer):
    def __init__(self, config: Gpt2Config, **kwargs):
        super().__init__(**kwargs)
        nx = config.n_embedding_units
        self.c_fc = Conv1D(config.n_model_units, nx, name="c_fc")
        self.c_proj = Conv1D(nx, config.n_model_units, name="c_proj")
        self.act = get_activation("gelu")
        self.dropout = tf.keras.layers.Dropout(config.residual_dropout_rate)

    def call(self, x, training=False):
        h = self.act(self.c_fc(x))
        h2 = self.c_proj(h)
        h2 = self.dropout(h2, training=training)
        return h2


class Block(tfl.Layer):
    def __init__(self, config: Gpt2Config, **kwargs):
        super().__init__(**kwargs)

        self.n_embedding_units = config.n_embedding_units
        self.n_model_units = config.n_model_units
        self.n_heads = config.n_heads
        self.layer_norm_epsilon = config.layer_norm_epsilon

        self.layer_norm = [
            tfl.LayerNormalization(epsilon=self.layer_norm_epsilon)
            for _ in range(2)
        ]
        self.attention = Attention(config)
        self.mlp = Mlp(config)

    # noinspection PyMethodOverriding
    def call(
            self,
            x,
            layer_past,
            attention_mask,
            head_mask,
            use_cache,
            output_attentions,
            training: bool = False
    ):
        a = self.layer_norm[0](x)

        attn_output = self.attention(
            a,
            layer_past,
            attention_mask,
            head_mask,
            use_cache,
            output_attentions,
            training=training
        )  # (a, present, (attentions))
        a = attn_output[0]
        x = x + a

        m = self.layer_norm[1](x)
        m = self.mlp(m, training=training)
        x = x + m

        outputs = [x] + attn_output[1:]

        return outputs  # x, present, (attentions)


class Gpt2(LanguageModel):
    def __init__(self, vocab: Dict[str, int], config: Gpt2Config, **kwargs):
        super().__init__(
            config.sequence_length,
            vocab,
            config.dataset_method,
            **kwargs
        )

        self.config = config
        self.n_embedding_units = config.n_embedding_units
        self.n_layers = config.n_layers

        # The huggingface library computes the input and positional embeddings slightly differently
        self.input_embedding = tfl.Embedding(self.vocab_size, self.n_embedding_units)
        self.positional_embedding = tfl.Embedding(self.sequence_length, self.n_embedding_units)

        self.input_dropout = tfl.Dropout(config.embedding_dropout_rate)
        self.final_layer_norm = tfl.LayerNormalization(epsilon=config.layer_norm_epsilon)
        self.decoder_blocks = [Block(config) for _ in range(config.n_layers)]

        self.final_layer = tfl.Dense(self.vocab_size)

    @classmethod
    def prepare_model(
            cls,
            args: argparse.Namespace,
            vocab: Dict[str, int],
            **kwargs
    ) -> "LanguageModel":
        config = Gpt2Config(
            dataset_method=args.dataset_method,
            sequence_length=args.sequence_length,
            n_layers=args.n_layers,
            n_heads=args.n_heads,
            n_embedding_units=args.n_embedding_units,
            n_model_units=args.n_model_units,
            embedding_dropout_rate=args.embedding_dropout_rate,
            attention_dropout_rate=args.attention_dropout_rate,
            residual_dropout_rate=args.residual_dropout_rate,
            layer_norm_epsilon=args.layer_norm_epsilon,
            scale=args.scale_attention,
            use_cache=args.use_cache,
            output_attentions=args.output_attentions,
            optimizer_params=make_optimizer_params_from_args(args)
        )

        model = cls(vocab, config)
        model.compile_model()

        return model

    def call(
            self,
            inputs,
            past=None,
            attention_mask=None,
            token_type_ids=None,
            position_ids=None,
            head_mask=None,
            inputs_embeds=None,
            use_cache=None,
            output_attentions=None,
            output_hidden_states=None,
            return_dict=None,
            training=False,
    ):
        # There are other options in the huggingface library, but for now it's
        # a requirement that the inputs are the token ids.
        input_ids = inputs

        # Given the simplification the shape should be (batch_size, sequence_length)
        input_shape = shape_list(input_ids)

        # Given the simplification the target shape and the input shape should
        # actually be the same thing.
        input_ids = tf.reshape(input_ids, [-1, input_shape[-1]])

        if past is None:
            past_length = 0
            past = [None] * len(self.decoder_blocks)
        else:
            past_length = shape_list(past[0][0])[-2]

        # The position ids can be passed in as a parameter in the huggingface library,
        # but for now, they are always defined here.
        position_ids = tf.range(past_length, input_shape[-1] + past_length, dtype=tf.int32)
        position_ids = position_ids[tf.newaxis, :]

        if attention_mask is not None:
            # We create a 3D attention mask from a 2D tensor mask.
            # Sizes are [batch_size, 1, 1, to_seq_length]
            # So we can broadcast to [batch_size, num_heads, from_seq_length, to_seq_length]
            # this attention mask is more simple than the triangular masking of causal attention
            # used in OpenAI GPT, we just need to prepare the broadcast dimension here.
            attention_mask = attention_mask[:, tf.newaxis, tf.newaxis, :]

            # Since attention_mask is 1.0 for positions we want to attend and 0.0 for
            # masked positions, this operation will create a tensor which is 0.0 for
            # positions we want to attend and -10000.0 for masked positions.
            # Since we are adding it to the raw scores before the softmax, this is
            # effectively the same as removing these entirely.
            attention_mask = tf.cast(attention_mask, tf.float32)
            attention_mask = (1.0 - attention_mask) * -10000.0
        else:
            attention_mask = None

        # Prepare head mask if needed
        # 1.0 in head_mask indicate we keep the head
        # attention_probs has shape bsz x n_heads x N x N
        # input head_mask has shape [num_heads] or [num_hidden_layers x num_heads]
        # and head_mask is converted to shape [n_layers x bsz x num_heads x seq_length x seq_length]
        if head_mask is not None:
            raise NotImplementedError
        else:
            head_mask = [None] * self.n_layers

        # The new shape after the following operation should be (batch_size, sequence_length)
        position_ids = tf.reshape(position_ids, [-1, shape_list(position_ids)[-1]])

        # Get the input embeddings
        input_embedding = self.input_embedding(input_ids)

        # Huggingface also has an optional token type embedding, but I don't use it here.

        # Get the positional embeddings
        positional_embedding = self.positional_embedding(position_ids)

        # Compute the compositional embedding
        hidden_states = input_embedding + positional_embedding

        hidden_states = self.input_dropout(hidden_states, training=training)

        # The input_shape should be (batch_size, sequence_length),
        # so the output shape should be (batch_size, sequence_length, n_embedding_units)
        output_shape = input_shape + [shape_list(hidden_states)[-1]]

        presents = () if use_cache else None
        all_attentions = () if output_attentions else None
        all_hidden_states = () if output_hidden_states else None

        for i, (block, layer_past) in enumerate(zip(self.decoder_blocks, past)):
            if output_hidden_states:
                all_hidden_states = all_hidden_states + (tf.reshape(hidden_states, output_shape),)

            outputs = block(
                hidden_states,      # The composite embeddings
                layer_past,         # I think this is only used during inference/generation
                attention_mask,     # I'm not sure how this is defined (so I'm not using it)
                head_mask[i],       # Should always be None (not implemented here or in huggingface)
                use_cache,          # Not sure how or when this should be used
                output_attentions,  # Flag used to return attentions
                training=training
            )

            hidden_states, present = outputs[:2]

            if use_cache:
                presents = presents + (present,)

            if output_attentions:
                all_attentions = all_attentions + (outputs[2],)

        hidden_states = self.final_layer_norm(hidden_states)

        hidden_states = tf.reshape(hidden_states, output_shape)

        # Add last hidden state
        if output_hidden_states:
            all_hidden_states = all_hidden_states + (hidden_states,)

        if output_attentions:
            # let the number of heads free (-1) so we can extract attention even after head pruning
            attention_output_shape = input_shape[:-1] + [-1] + shape_list(all_attentions[0])[-2:]
            all_attentions = tuple(tf.reshape(t, attention_output_shape) for t in all_attentions)

        logits = self.final_layer(hidden_states)

        if not return_dict:
            return tuple(
                v for
                v in [logits, hidden_states, presents, all_hidden_states, all_attentions]
                if v is not None
            )

        return {
            'logits': logits,
            'last_hidden_state': hidden_states,
            'past_key_values': presents,
            'hidden_states': all_hidden_states,
            'attentions': all_attentions
        }

    def train_step(self, data):
        x, y_true = data

        auxiliary_loss_method = self.config.auxiliary_loss_method

        with tf.GradientTape() as tape:
            # Note: This model does not have the functionality to use an approximate softmax
            # so the `y` values are not needed. The `lookahead_mask` is also not needed because it
            # is created on demand in the attention layer (with the name causal_mask).

            result = self(x, output_hidden_states=True, return_dict=True, training=True)
            y_pred = result['logits']
            y_pred_list = result['hidden_states']  # TODO this is not correct

            if auxiliary_loss_method == 'data_weighted':
                # Character-Level Language Modeling with Deeper Self-Attention
                # This paper uses a weighting scheme that sums the losses of
                # each intermediate layer, but the ith layer stops contributing
                # to the loss after i / (2n) amount of the data has been seen (
                # where n is the total number of hidden layers).
                # This seems like an odd way to weight the layers. It is also
                # ambiguous exactly what this means. Is this the fraction of data
                # per epoch, or over the whole training process? It is also
                # difficult / impossible to implement without knowing the size
                # of the corpus in advance (which is not always possible).
                raise NotImplementedError("The number of data points is unknown.")
            elif auxiliary_loss_method == 'depth_weighted':
                if y_pred_list is None:
                    raise ValueError(
                        "You must return the hidden states when using the depth_weighted loss "
                        "method"
                    )
                n_layers = self.config.n_layers

                # The contribution of each loss will be weighted based on the depth of that
                # layer and decay exponentially as training progresses. A*exp(-a*t)

                # Calculate the loss for each intermediate layer
                losses = tf.constant(
                    [self.compiled_loss(y_true, y_pred_lyr) for y_pred_lyr in y_pred_list],
                    dtype=y_pred.dtype
                )

                # The base weights represent A.
                # The base weights decrease as the layer depth increases.
                # (note the weights are in reverse order at the moment)
                base_weights = 1.0 / (tf.range(1, n_layers + 1) * self.config.loss_weight_n)

                # Compute the full exponential decay for each layer
                weights = base_weights * tf.exp(-self.config.loss_weight_lambda * self.step_num)

                # The top layer should never decay
                weights = tf.tensor_scatter_nd_update(weights, [[0]], [1.0])

                if self.config.loss_weight_normalized is True:
                    # Normalize the weights so the sum of the losses is comparable across steps and
                    # epochs
                    weights = weights / tf.reduce_sum(weights)

                # Reverse the weights (put the top output layer at position 0 and the deepest
                # hidden layer at the back).
                weights = tf.reverse(weights, axis=[0])

                loss = weights * losses
            elif auxiliary_loss_method in (None, 'none'):
                loss = self.compiled_loss(y_true, y_pred)
            else:
                raise ValueError(f"Unknown auxiliary loss method: {auxiliary_loss_method}")

        # For custom training steps, users can just write:
        trainable_variables = self.trainable_variables
        gradients = tape.gradient(loss, trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, trainable_variables))

        # Do not add the metrics for the training steps. They are kind of
        # interesting to track, but are slow to compute.
        result = {'loss': loss}

        return result

    def test_step(self, data):
        x, y_true = data
        output = self(x, return_dict=True, training=False)
        y_pred = output['logits']
        loss = self.compiled_loss(y_true, y_pred)
        self.compiled_metrics.update_state(y_true, y_pred)

        result = {m.name: m.result() for m in self.metrics}
        result['loss'] = loss

        return result

    def estimate_memory_usage(self, parameters: Dict[str, Any], vocab_size: int) -> int:
        # seq_len = parameters['sequence_length']
        # n_model_units = parameters['n_model_units']
        # n_mlp_units = parameters['n_embedding_units']

        raise NotImplementedError()
