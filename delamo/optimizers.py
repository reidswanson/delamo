# ##############################################################################
#  Copyright 2020 Google Developers, Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# This learning rate schedule is derived from the transformer tutorial on the
# TensorFlow website:
# https://www.tensorflow.org/tutorials/text/transformer#optimizer

# Python Modules

# 3rd Party Modules
import tensorflow as tf

# Project Modules


class TransformerSchedule(tf.keras.optimizers.schedules.LearningRateSchedule):
    def __init__(self, n_model_dim: int, warmup_steps: int = 4000):
        super().__init__()

        self.n_model_dim = n_model_dim
        self.warmup_steps = warmup_steps

    def __call__(self, step):
        arg1 = tf.math.rsqrt(step)
        arg2 = step * (self.warmup_steps ** -1.5)

        return tf.math.rsqrt(tf.cast(self.n_model_dim, tf.float32)) * tf.math.minimum(arg1, arg2)

    def get_config(self):
        return {
            'n_model_dim': self.n_model_dim,
            'warmup_steps': self.warmup_steps
        }
