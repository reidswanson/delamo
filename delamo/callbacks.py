# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import logging

from typing import List, Optional

# 3rd Party Modules
import tensorflow as tf

# Project Modules
from delamo.text.generation import TextGenerator

log = logging.getLogger(__name__)


class ResetStatesCallback(tf.keras.callbacks.Callback):
    def __init__(self, period: int):
        """
        Reset the states of an RNN. The states are always reset at the beginning
        and end of every epoch. It will also reset the states after every
        ``period`` number of batches.
        """
        super().__init__()

        self.period = period

    def on_batch_end(self, batch, logs=None):
        if batch > 0 and batch % self.period == 0:
            self.model.first_layer.reset_states()

    def on_epoch_begin(self, epoch, logs=None):
        if epoch > 1:
            self.model.first_layer.reset_states()

    def on_epoch_end(self, epoch, logs=None):
        self.model.first_layer.reset_states()


class TextGenerationCallback(tf.keras.callbacks.Callback):
    def __init__(
            self,
            period: int = 1,
            n_samples: int = 1,
            input_seed: List[str] = None,
            top_n: Optional[int] = 40,
            temperature: float = 1.0,
            random_seed: Optional[int] = None
    ):
        """
        Generate text sequences from the model as it is training.

        :param period: Text will be generated every ``period`` number of
               epochs.
        :param n_samples: How many texts to generate.
        :param input_seed: The text used to seed the generation process.
        :param top_n: Only sample the words with the ``top_n`` highest
               probabilities.
        :param temperature: The temperature used to bias the probabilities.
        :param random_seed: Use this to seed the random number generator
               for reproducible results.
        """
        super().__init__()

        self.period = period
        self.n_samples = n_samples
        self.input_seed = ['<s>'] if input_seed is None else input_seed
        self.top_n = top_n
        self.temperature = temperature
        self.random_seed = random_seed

    def on_epoch_end(self, epoch, logs=None):
        if epoch % self.period != 0:
            return

        texts = [
            self.generate(self.input_seed, self.top_n, self.temperature, self.random_seed)
            for _ in range(self.n_samples)
        ]

        for i, text in enumerate(texts):
            print()
            log.info(f"generated_text [{i}]: {text}")
