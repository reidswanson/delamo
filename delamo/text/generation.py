# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import abc
import logging

from typing import Dict, List, Callable, Union, Tuple

# 3rd Party Modules
import tensorflow as tf

from depq import DEPQ

# Project Modules

log = logging.getLogger(__name__)


class Strategy(abc.ABC):
    def __init__(self, max_len: int, end_id: int, model: tf.keras.models.Model = None):
        """
        A base class for generating novel sequences from a language model.

        :param max_len: The maximum length of the generate text. Note, this
               implementation of the transformer requires fixed length
               sequences during training. ``max_len`` must be the same value
               as the fixed length during training.
        :param end_id: The integer value representing the end of a sequence.
        :param model: The keras model. This must be set before any instances
               of this class can be used.
        """

        self._max_len = max_len
        self._end_id = end_id
        self._model = model

    def __call__(self, *args, **kwargs):
        if self._model is None:
            raise RuntimeError("The model must be set before generating.")

        return self.call(*args, **kwargs)

    @property
    def model(self) -> tf.keras.Model:
        """
        :return: The keras model used for generating sequences.
        """
        return self._model

    @model.setter
    def model(self, model: tf.keras.Model):
        """
        Set the model.

        :param model: The model to be used for generating sequences.
        :return:
        """
        self._model = model

    @abc.abstractmethod
    def call(self, input_seed: List[int],  *args, **kwargs) -> List[int]:
        """
        Generate a single sequence from the model.

        :param input_seed: The initial sequence to seed the generation.
        :param args: Not used.
        :param kwargs: Not used.
        :return: A list of integers representing the generated tokens.
        """
        raise NotImplementedError()

    def preprocess(self, input_seed: List[int]) -> Tuple[int, tf.Tensor, List[int]]:
        """
        Setup some datastructures used for generation.

        **For internal use**

        :param input_seed: The initial sequence to seed the generation.
        :return:
        """
        seed_len = len(input_seed)

        # Create an array for generating the text.
        # It should start with the input seed and needs to be a fixed
        # length. (I think)
        input_seq = input_seed + [0] * (self._max_len - seed_len)

        # Convert it to a tensor and expand the dimensions so it is
        # compatible with the model.
        input_seq = tf.cast(tf.expand_dims(input_seq, 0), tf.int32)

        generated_output = [t for t in input_seed]

        return seed_len, input_seq, generated_output

    def make_lookahead_mask(self, current_pred_idx: int) -> tf.Tensor:
        """
        Create a mask that excludes tokens we haven't seen or generated yet.

        :param current_pred_idx: The index of the token we are currently
               processing.
        :return: The mask
        """
        return 1 - tf.linalg.band_part(tf.ones((1, self._max_len)), 0, current_pred_idx)


class GreedyStrategy(Strategy):
    def __init__(self, max_len: int, end_id: int, model: tf.keras.models.Model = None):
        """
        Generate instances using a greedy strategy. Namely, always select
        the most likely event at the given time step.

        :param max_len: The maximum length of the generate text. Note, this
               implementation of the transformer requires fixed length
               sequences during training. ``max_len`` must be the same value
               as the fixed length during training.
        :param end_id: The integer value representing the end of a sequence.
        :param model: The keras model. This must be set before any instances
               of this class can be used.
        """
        super().__init__(max_len, end_id, model)

    def call(self, input_seed: List[int], *args, **kwargs):
        model = self._model
        end_id = self._end_id

        seed_len, input_seq, generated_output = self.preprocess(input_seed)

        for current_pred_idx in range(seed_len - 1, self._max_len - 1):
            lookahead_mask = self.make_lookahead_mask(current_pred_idx)
            x = input_seq, lookahead_mask
            y_pred, _ = model(x, training=False)

            # Get the current prediction
            y_pred = y_pred[0, current_pred_idx]

            token_id = tf.argmax(y_pred)
            generated_output.append(token_id.numpy().item())
            input_seq = tf.tensor_scatter_nd_update(
                input_seq,
                [[0, current_pred_idx+1]],
                [token_id]
            )

            if token_id == end_id:
                break

        return generated_output


class SampleStrategy(Strategy):
    def __init__(
            self,
            max_len: int,
            end_id: int,
            top_k: Union[int, Callable[[int], int]] = 0,
            temperature: Union[float, Callable[[int], float]] = 1.0,
            model: tf.keras.models.Model = None
    ):
        """
        Generate a new sequence by sampling from the predicted distribution
        at each time step.

        :param max_len: The maximum length of the generate text. Note, this
               implementation of the transformer requires fixed length
               sequences during training. ``max_len`` must be the same value
               as the fixed length during training.
        :param end_id: The integer value representing the end of a sequence.
        :param top_k: This can be an integer or a callable function taking
               1 integer argument representing the current length of the
               generated text.

               If it is an integer then the possible items are sorted by
               likelihood and only the ``top_k`` are sampled from. If ``top_k``
               is less than or equal to 0, the entire population is
               sampled.

               If it is a function, then the return value represents ``top_k``
               and the items are again sorted and sampled as before.
        :param temperature: This can also be a number of callable function
               taking 1 integer argument and returning a float
               (greater than 0).

               The return value of the function (or the passed in value)
               represents the sampling temperature. High values of temperature
               result in a higher degree of randomness, whereas low values
               of temperature bias towards more probable events (with a greedy
               search at the limit). A value of 1.0 samples from the predicted
               distribution as is.
        :param model: The keras model. This must be set before any instances
               of this class can be used.
        """
        super().__init__(max_len, end_id, model)

        # Intuitively it seems like the temperature should be high(er)
        # early when there is a lot of uncertainty concerning what the
        # semantics are. Then as the sequence length increases, and more
        # context is given, the temperature should drop. A similar
        # argument could be made about the size of k.
        if isinstance(top_k, int):
            self._top_k = lambda n: min(max_len, max_len if top_k <= 0 else top_k)
        elif callable(top_k):
            self._top_k = top_k
        else:
            raise ValueError(f"top_k must either be an integer or a callable.")

        if isinstance(temperature, (float, int)) and temperature > 0:
            self._temperature = lambda n: float(temperature)
        elif callable(temperature):
            self._temperature = temperature
        else:
            raise ValueError(
                f"The temperature must either be a number greater than 0 or a callable."
            )

    def call(self, input_seed: List[int], *args, **kwargs):
        model = self._model
        end_id = self._end_id
        top_k = self._top_k
        temperature = self._temperature

        seed_len, input_seq, generated_output = self.preprocess(input_seed)

        for current_pred_idx in range(seed_len - 1, self._max_len - 1):
            lookahead_mask = self.make_lookahead_mask(current_pred_idx)
            x = input_seq, lookahead_mask
            y_pred, _ = model(x, training=False)

            # Get the current prediction
            y_pred = y_pred[0, current_pred_idx]

            top_idx = tf.argsort(y_pred, direction='DESCENDING')[:top_k(current_pred_idx)]
            top_probs = tf.gather(y_pred, top_idx)

            # Renormalize (not sure if this is really necessary since I think
            # tf.random.categorical will do it anyway, but it can't hurt)
            top_probs = top_probs / tf.reduce_sum(top_probs)

            top_probs = tf.math.log(top_probs / temperature(current_pred_idx))
            sample = tf.random.categorical(
                tf.expand_dims(top_probs, 0),
                num_samples=1,
                dtype=tf.int32
            )

            token_id = top_idx[sample[0, 0]]
            generated_output.append(token_id.numpy().item())
            input_seq = tf.tensor_scatter_nd_update(
                input_seq,
                [[0, current_pred_idx+1]],
                [token_id]
            )

            if token_id == end_id:
                break

        return generated_output


class SampleBeamStrategy(Strategy):
    def __init__(
            self,
            max_len: int,
            end_id: int,
            n_samples: int = 10,
            beam_width: int = 100,
            top_k: int = 0,
            temperature: float = 1.0,
            model: tf.keras.models.Model = None
    ):
        """
        This is slow and does not work well.

        :param max_len:
        :param end_id:
        :param n_samples:
        :param beam_width:
        :param top_k:
        :param temperature:
        :param model:
        """
        super().__init__(max_len, end_id, model)

        if n_samples <= 0:
            raise ValueError("The number of samples must be greater than 0.")

        if beam_width <= 0:
            raise ValueError("The beam width must be greater than 0.")

        if temperature <= 0:
            raise ValueError(f"The temperature cannot be less than or equal to 0.")

        top_k = max_len if top_k <= 0 else top_k

        self._n_samples = n_samples
        self._beam_width = beam_width
        self._top_k = top_k
        self._temperature = temperature

    def call(self, input_seed: List[int], *args, **kwargs):
        model = self._model
        end_id = self._end_id
        max_len = self._max_len - 1
        n_samples = self._n_samples
        top_k = self._top_k
        temperature = self._temperature

        pq = DEPQ(maxlen=self._beam_width)
        pq.insert([t for t in input_seed], 0)

        result = []

        while not pq.is_empty() and len(result) < self._beam_width:
            current_input, priority = pq.popfirst()
            if current_input[-1] == end_id or len(current_input) >= max_len:
                log.debug(f"Adding output to results: {priority:9.3f} {current_input}")
                result.append((current_input, priority))
                removed = pq.elim(current_input) + pq.elim(current_input[:-1])
                log.debug(f"removed: {len(removed)} items")

                continue

            current_pred_idx = len(current_input) - 1
            _, input_seq, _ = self.preprocess(current_input)
            lookahead_mask = self.make_lookahead_mask(current_pred_idx)
            x = input_seq, lookahead_mask
            y_pred, _ = model(x, training=False)

            # Get the current prediction
            y_pred = y_pred[0, current_pred_idx]

            top_idx = tf.argsort(y_pred, direction='DESCENDING')[:top_k]
            top_probs = tf.gather(y_pred, top_idx)

            top_probs = tf.math.log(top_probs / temperature)
            samples = tf.random.categorical(
                tf.expand_dims(top_probs, 0),
                num_samples=n_samples,
                dtype=tf.int32
            )

            for sample in samples[0]:
                next_input = [t for t in current_input] + [top_idx[sample].numpy().item()]
                priority = priority + top_probs[sample].numpy().item()

                # log.debug(f"Inserting stub: {priority:9.3f} {next_input}")
                pq.insert(next_input, priority)

        # Sample from the results
        outputs = [o for o, _ in result]
        log_p = [lp / len(o) for o, lp in result]

        samples = tf.random.categorical(
            tf.expand_dims(log_p, 0),
            num_samples=1,
            dtype=tf.int32
        )

        return outputs[samples[0, 0]]


class TextGenerator(object):
    def __init__(self, strategy: Strategy, token_2_id: Dict[str, int], id_2_token: Dict[int, str]):
        """

        :param strategy:
        :param token_2_id:
        :param id_2_token:
        """
        self._strategy = strategy
        self._token_2_id = token_2_id
        self._id_2_token = id_2_token

    @property
    def strategy(self) -> Strategy:
        return self._strategy

    def generate(self, input_seed: List[str]):
        if tf.rank(input_seed) != 1:
            raise ValueError(f"The input seed must be rank 1")

        input_seed = [self._token_2_id[t] for t in input_seed]
        generated_tokens = self._strategy(input_seed)

        return ' '.join([self._id_2_token[i] for i in generated_tokens])
