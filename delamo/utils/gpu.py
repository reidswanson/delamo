# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import math

# 3rd Party Modules
import nvidia_smi
import tensorflow as tf

# Project Modules


nvidia_smi.nvmlInit()


def get_max_available_memory(overhead: float = 0.1) -> int:
    """
    Get the maximum amount of memory available across all visible GPU devices.

    :overhead: How much memory (as a fraction of the total from 0.0 to 1.0) that should be
               considered overhead and not considered available.
    :return: The amount of memory in bytes.
    """
    max_memory = 0
    for i, _ in enumerate(tf.config.get_visible_devices('GPU')):
        h = nvidia_smi.nvmlDeviceGetHandleByIndex(i)
        info = nvidia_smi.nvmlDeviceGetMemoryInfo(h)
        available_memory = int(math.floor((1.0 - overhead) * info.total))
        if available_memory > max_memory:
            max_memory = available_memory

    return max_memory


def disable_gpus():
    tf.config.set_visible_devices([], 'GPU')
