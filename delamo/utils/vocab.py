# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import gzip

from typing import Dict

# 3rd Party Modules
import tensorflow as tf

# Project Modules


def py_vocab_from_file(
        filename: str,
        unk: str = '<unk>',
        start: str = '<s>',
        end: str = '</s>',
        newline: str = '<br>'
) -> Dict[str, int]:
    unique_words = set()
    in_open = gzip.open if filename.endswith('.gz') else open

    with in_open(filename, 'rt') as fh:
        for line in fh:
            if line:
                tokens = line.split()
                unique_words.update(tokens)

    word2id = {'': 0}
    word2id.update({k: v for v, k in enumerate(unique_words, 1)})
    if unk not in word2id:
        word2id[unk] = len(word2id)

    if start not in word2id:
        word2id[start] = len(word2id)

    if end not in word2id:
        word2id[end] = len(word2id)

    if newline not in word2id:
        word2id[newline] = len(word2id)

    return word2id


def tf_vocab_from_dict(
        word2id: Dict[str, int],
        unk: str = '<unk>'
) -> tf.lookup.StaticHashTable:
    keys = tf.constant(list(word2id.keys()))
    values = tf.constant(list(word2id.values()))

    word_initializer = tf.lookup.KeyValueTensorInitializer(keys, values)
    word2id = tf.lookup.StaticHashTable(word_initializer, word2id[unk])

    return word2id


def tf_vocab_from_variable(
        words: tf.Variable,
        unk: str = '<unk>'
) -> tf.lookup.StaticHashTable:
    word2id = {
        k.numpy().decode('utf-8'): v
        for v, k in enumerate(tf.convert_to_tensor(words, dtype=tf.string))
    }

    return tf_vocab_from_dict(word2id, unk)
