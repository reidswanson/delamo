# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
from functools import reduce
from typing import List, Optional

# 3rd Party Modules

# Project Modules


def factors(n: int, low: int = 1, high: Optional[int] = None) -> List[int]:
    """
    Return the factors of ``n`` that are in the range of [``low``, ``high``].
    If ``low`` and ``high`` are not specified then all the factors will be returned.

    :param n: The number to factor.
    :param low: The smallest factor to return (inclusive).
    :param high: The largest factor to return (inclusive).
    :return: The factors.
    """
    high = high or n

    f = set(reduce(list.__add__, [[i, n//i] for i in range(1, int(n**0.5) + 1) if n % i == 0]))
    return list(sorted([i for i in f if low <= i <= high]))
