# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# Python Modules
import collections
import logging

from typing import List, Dict

# 3rd Party Modules
import numpy as np
import tensorflow as tf

from tensorflow.keras.mixed_precision import experimental as mixed_precision

# Project Modules
from tensorflow.python.framework.convert_to_constants import convert_variables_to_constants_v2

log = logging.getLogger(__name__)


def shape_list(x: tf.Tensor) -> List[int]:
    """
    Deal with dynamic shape in tensorflow cleanly.

    Args:
        x (:obj:`tf.Tensor`): The tensor we want the shape of.

    Returns:
        :obj:`List[int]`: The shape of the tensor as a list.
    """
    static = x.shape.as_list()
    dynamic = tf.shape(x)

    return [dynamic[i] if s is None else s for i, s in enumerate(static)]


def get_initializer(initializer_range: float = 0.02) -> tf.initializers.TruncatedNormal:
    """
    Creates a :obj:`tf.initializers.TruncatedNormal` with the given range.

    Args:
        initializer_range (`float`, defaults to 0.02): Standard deviation of the initializer range.

    Returns:
        :obj:`tf.initializers.TruncatedNormal`: The truncated normal initializer.
    """
    return tf.keras.initializers.TruncatedNormal(stddev=initializer_range)


def count_parameters(model: tf.keras.Model) -> Dict[str, int]:
    """
    Count the number of parameters in the model.

    See `This StackOverflow post <https://stackoverflow.com/a/46216013>_`

    :param model:
    :return:
    """
    import tensorflow.keras.layers as tfl

    stack = collections.deque(model.layers)
    params = collections.defaultdict(int)
    # Create graph nodes.
    while len(stack) > 0:
        layer = stack.pop()

        # Append a wrapped layer's label to node's label, if it exists.
        layer_type = layer.__class__.__name__
        layer_attributes = layer.__dict__

        nested_layers = [lyr for lyr in layer_attributes.values() if isinstance(lyr, tfl.Layer)]
        nested_params = sum(lyr.count_params() for lyr in nested_layers)
        layer_params = layer.count_params() - nested_params

        params[layer_type] += layer_params

        if nested_layers:
            for nested_layer in nested_layers:
                stack.append(nested_layer)

    return params
