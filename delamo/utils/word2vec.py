# ##############################################################################
#  Copyright 2020 Reid Swanson
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
# ##############################################################################

# See the following links about the derivation of this implementation
# https://stackoverflow.com/a/27329142/4971706
# https://github.com/boppreh/word2vec_bin_parser/blob/master/word2vec_bin_parser.py

# Python Modules
import collections
import gzip
import struct

from typing import Optional, Set, Dict

# 3rd Party Modules
import numpy as np

# Project Modules


class WordVectors(collections.UserDict):
    def __init__(self, vecdim: int):
        """
        Stores word/vector mappings as a dictionary like object whose keys are the words and
        whose values are the corresponding vectors (as a numpy array)

        :vecdim: The number of dimensions of the vectors.
        """
        super().__init__()

        self.vecdim = vecdim

    @classmethod
    def load(cls, filename: str, vocabulary: Optional[Set[str]] = None):
        """
        Load a set of word vectors from a file.

        :param filename: Load an existing collection from a file in the Google
               word2vec format. The first line stores the number of words ``n_words`` and
               vector size ``size`` (as a space separated string). The remainder of the
               file are space separated pairs of words and vectors. For each
               word in ``n_words`` a sequence of bytes is read until a space is
               encountered. This sequence (excluding the space) is decoded as a utf-8
               string and is the current word. Next ``size`` 4-byte floats are read and
               used as the vector for the current word.
        :param vocabulary: (Optional) Only load words that are in the vocabulary.
        :return: The ``WordVectors``.
        """
        fopen = gzip.open if filename.endswith('.gz') else open

        with fopen(filename, 'rb') as fh:
            # The first line of the file stores the number of words and the
            # vector dimension (as a string).
            n_words, vecdim = [int(s) for s in fh.readline().split()]

            w2v = WordVectors(vecdim)

            for i in range(n_words):
                # Get the word
                word = cls._read_word(fh)

                # Get the vector
                vector = np.array(struct.unpack('f' * vecdim, fh.read(vecdim * 4)))

                if not vocabulary or word in vocabulary:
                    # Set the entry
                    w2v.data[word] = vector

        return w2v

    def save(self, filename: str):
        """
        Save the word vectors to a file in the Google format.
        :param filename: The file where the vectors will be saved. If the filename
               ends with ``.gz`` it will be compressed using gzip otherwise it
               will not be compressed.
        """
        fopen = gzip.open if filename.endswith('.gz') else open

        with fopen(filename, 'wb') as fh:
            fh.write(f'{len(self.data)} {self.vecdim}\n'.encode('utf-8'))

            for word, vector in self.data.items():
                fh.write(f'{word} '.encode('utf-8'))
                fh.write(struct.pack('=' + 'f' * self.vecdim, *vector.tolist()))

    def get_embedding_matrix(self, vocab: Dict[str, int]) -> np.ndarray:
        """
        Convert the word vectors to a numpy array that can be used to initialize
        a TensorFlow/Keras class:`tf.keras.layers.Embedding` layer.

        :param vocab: A python dictionary that maps words (strings) to their integer index
               (in the embedding layer).
        :return: A 2d numpy array.
        """
        this_keys, that_keys = set(self.data.keys()), set(vocab.keys())
        if this_keys != that_keys:
            missing = (
                this_keys - that_keys if len(this_keys) > len(that_keys) else
                that_keys - this_keys
            )

            raise ValueError(
                f"The vocab must have the same words as the word vectors. {missing}"
            )

        matrix = np.zeros((len(vocab), self.vecdim))

        for word, idx in vocab.items():
            matrix[idx] = self.data[word]

        return matrix

    @classmethod
    def _read_word(cls, fh) -> str:
        # A word is a sequence of characters terminated by a space
        buffer = []
        while True:
            c = fh.read(1)
            if c == b' ':
                return b''.join(buffer).decode('utf-8')
            else:
                buffer.append(c)
