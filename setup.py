# Python Modules
from setuptools import find_packages, setup

# 3rd Party Modules

# Project Modules


if __name__ == '__main__':
    setup(name='delamo',
          version='0.4.0',
          description='A self attention decoder only based language model',
          install_requires=[
              'numpy',
              'tensorflow>=2.3',
          ],
          extras_require={
              'plotting': [
                  'matplotlib'
              ],
              'beam_search': [
                  'depq'
              ],
              'parameter-search': [
                  'nvidia-ml-py3',
                  'oputna',
                  'pandas',
                  'plotly',
                  'psutil',
                  'scikit-learn'
              ],
              'estimate-memory-usage': [
                  'plotnine'
              ]
          },
          author='Reid Swanson',
          maintainer='Reid Swanson',
          author_email='reid@reidswanson.com',
          maintainer_email='reid@reidswanson.com',
          zip_safe=False,
          packages=find_packages(),
          include_package_data=True,
          license='Apache-2.0',
          url='https://bitbucket.org/reidswanson/delamo',
          classifiers=['Development Status :: 4 - Beta',
                       'Intended Audience :: Science/Research',
                       'License :: OSI Approved :: Apache Software License',
                       'Natural Language :: English',
                       'Operating System :: POSIX :: Linux',
                       'Programming Language :: Python :: 3.7']
    )